#!/bin/bash
#------------------------------------------------------------------------------
#Create the indexes files used by astrometry.net
#it requires gnu "parallel" : https://www.gnu.org/software/bash/manual/html_node/GNU-Parallel.html
#debian: sudo apt install parallel
#fedora: sudo dnf install parallel
#------------------------------------------------------------------------------
INPUT_DIR=split_heal_pix/
INPUT_FILE_PREFIX=gaia_ed3_heal_pix-
OUTPUT_DIR=output
#-----------------------------------------------------------------------------
#set -xv  #DEBUG ON
#-----------------------------------------------------------------------------
#to set scale, see: http://astrometry.net/doc/readme.html#getting-index-files 

#healpix NSide = 2
MIN_HEAL_PIX=0
MAX_HEAL_PIX=47

#from 6 arcmin (MIN_SCALE=9) to 2 degrees (MAX_SCALE=8)
MIN_SCALE=0   
MAX_SCALE=8

MARGIN_DEGREES=0.01
JITTER_ARCOSEC=0.1

HEAL_PIX_N_SIDE=2

SORTING_FIELD=FLUX #if you are using a magnitude field use '-J' instead '-f' in the command line below
DEDUPLICATION_RADIUS_ARCOSEC=1
MAX_REUSES=16
#------------------------------------------------------------------------------
startTime=$(date +'%s')
echo "========================================================================="
echo $(date +"%Y-%m-%d:%Hh:%Mm:%Ss") 'Starting script 3'
echo "========================================================================="
#------------------------------------------------------------------------------
dropcaches_system_ctl 
rm -fr $OUTPUT_DIR
mkdir $OUTPUT_DIR

DATE=$(date '+%y%m%d')
echo "SCRIPT 3. CURRENT PATH        : '$PWD'"
echo "SCRIPT 3. Building GAIA indexes on directory:'$OUTPUT_DIR'"

for ((HEAL_PIX=MIN_HEAL_PIX; HEAL_PIX<=MAX_HEAL_PIX; HEAL_PIX++)); do
  echo "============>heal pix:$HEAL_PIX starts <============"
  for ((SCALE=$MIN_SCALE; SCALE<=$MAX_SCALE; SCALE++)); do
     
    echo "------>heal pix:$HEAL_PIX scale:$SCALE <-------"
    
    FILE_NAME_AND_EXTENSION="${INPUT_FITS##*/}"
    ONLY_FILENAME="${FILE_NAME_AND_EXTENSION%.*}"
  
    HEALPIX_FORMATTED=$(printf "%02d" $HEAL_PIX)
    SCALE_FORMATTED=$(printf "%02d" $SCALE)
         
    ONAME=$OUTPUT_DIR/"index"-$HEALPIX_FORMATTED-$SCALE_FORMATTED$ODIR.fits   
    
    INPUT_FITS=$INPUT_DIR/$INPUT_FILE_PREFIX$HEALPIX_FORMATTED.fits
    
    ID=$HEALPIX_FORMATTED$SCALE_FORMATTED
    
    sem -j +0 ./build-astrometry-index \
    -I $ID \
    -i $INPUT_FITS \
    -o $ONAME \
    -H $HEAL_PIX \
    -P $SCALE \
    -s $HEAL_PIX_N_SIDE \
    -S $SORTING_FIELD \
    -f \
    -j $JITTER_ARCOSEC \
    -r $DEDUPLICATION_RADIUS_ARCOSEC \
    -L $MAX_REUSES \
    -m $MARGIN_DEGREES \
    -M    
            
  done
  sem --wait
  echo "============>heal pix:$HEAL_PIX ends <============"
done  

#-------------------------------------------------------------------------------
echo "========================================================================="
echo $(date +"%Y-%m-%d:%Hh:%Mm:%Ss") 'End of script 3' 
echo "========================================================================="
echo "Elapsed time: $(($(date +'%s') - $startTime))s"
echo "-------------------------------------------------------------------------"


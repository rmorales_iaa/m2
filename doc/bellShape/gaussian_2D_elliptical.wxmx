PK     g�R�B�H         mimetypetext/x-wxmathmlPK     g�RiQ#4  4  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using a text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     g�RJ�o�'(  '(     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 21.05.2-DevelopmentSnapshot   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="160" activecell="4">

<cell type="code">
<input>
<editor type="input">
<line>f(x,y,A,B,x_0,y_0,s_x,s_y):=A*exp(-((x-x_0)^2/(2*s_x^2)+(y-y_0)^2/(2*s_y^2)))+B;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o1)	">(%o1) </lbl><fn><r><fnm>f</fnm></r><r><p><v>x</v><fnm>,</fnm><v>y</v><fnm>,</fnm><v>A</v><fnm>,</fnm><v>B</v><fnm>,</fnm><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><fnm>,</fnm><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><fnm>,</fnm><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><fnm>,</fnm><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></p></r></fn><fnm>:=</fnm><v>A</v><h>·</h><fn><r><fnm>exp</fnm></r><r><p><v>−</v><r><p><f><r><e><r><r><p><v>x</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><r><p><v>y</v><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f></p></r></p></r></fn><v>+</v><v>B</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff(f(x,y,A,B,x_0,y_0,s_x,s_y),A,1);</line>
<line>diff(f(x,y,A,B,x_0,y_0,s_x,s_y),B,1);</line>
<line>diff(f(x,y,A,B,x_0,y_0,s_x,s_y),x_0,1);</line>
<line>diff(f(x,y,A,B,x_0,y_0,s_x,s_y),y_0,1);</line>
<line>diff(f(x,y,A,B,x_0,y_0,s_x,s_y),s_x,1);</line>
<line>diff(f(x,y,A,B,x_0,y_0,s_x,s_y),s_y,1);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o2)	">(%o2) </lbl><e><r><s>%e</s></r><r><v>−</v><f><r><e><r><r><p><v>y</v><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>−</v><f><r><e><r><r><p><v>x</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f></r></e><lbl altCopy="(%o3)	">(%o3) </lbl><n>1</n><lbl altCopy="(%o4)	">(%o4) </lbl><f><r><v>A</v><h>·</h><r><p><v>x</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i></p></r><h>·</h><e><r><s>%e</s></r><r><v>−</v><f><r><e><r><r><p><v>y</v><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>−</v><f><r><e><r><r><p><v>x</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><lbl altCopy="(%o5)	">(%o5) </lbl><f><r><v>A</v><h>·</h><r><p><v>y</v><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i></p></r><h>·</h><e><r><s>%e</s></r><r><v>−</v><f><r><e><r><r><p><v>y</v><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>−</v><f><r><e><r><r><p><v>x</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><lbl altCopy="(%o6)	">(%o6) </lbl><f><r><v>A</v><h>·</h><e><r><r><p><v>x</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e><h>·</h><e><r><s>%e</s></r><r><v>−</v><f><r><e><r><r><p><v>y</v><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>−</v><f><r><e><r><r><p><v>x</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>3</n></r></e></r></f><lbl altCopy="(%o7)	">(%o7) </lbl><f><r><v>A</v><h>·</h><e><r><r><p><v>y</v><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e><h>·</h><e><r><s>%e</s></r><r><v>−</v><f><r><e><r><r><p><v>y</v><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>−</v><f><r><e><r><r><p><v>x</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>3</n></r></e></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>integrate(integrate(f(x,y,A,B,x_0,y_0,s_x,s_y), x,mx,Mx),y,my,My);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o8)	">(%o8) </lbl><f><r><v>−</v><r><p><s>π</s><h>·</h><v>A</v><h>·</h><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>erf</fnm></r><r><p><f><r><q><n>2</n></q><h>·</h><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>−</v><q><n>2</n></q><h>·</h><v>Mx</v></r><r><n>2</n><h>·</h><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r></f></p></r></fn><v>−</v><s>π</s><h>·</h><v>A</v><h>·</h><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>erf</fnm></r><r><p><f><r><q><n>2</n></q><h>·</h><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>−</v><q><n>2</n></q><h>·</h><v>mx</v></r><r><n>2</n><h>·</h><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r></f></p></r></fn></p></r><h>·</h><fn><r><fnm>erf</fnm></r><r><p><f><r><q><n>2</n></q><h>·</h><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>−</v><q><n>2</n></q><h>·</h><v>my</v></r><r><n>2</n><h>·</h><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r></f></p></r></fn><v>+</v><r><p><s>π</s><h>·</h><v>A</v><h>·</h><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>erf</fnm></r><r><p><f><r><q><n>2</n></q><h>·</h><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>−</v><q><n>2</n></q><h>·</h><v>Mx</v></r><r><n>2</n><h>·</h><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r></f></p></r></fn><v>−</v><s>π</s><h>·</h><v>A</v><h>·</h><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>erf</fnm></r><r><p><f><r><q><n>2</n></q><h>·</h><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>−</v><q><n>2</n></q><h>·</h><v>mx</v></r><r><n>2</n><h>·</h><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r></f></p></r></fn></p></r><h>·</h><fn><r><fnm>erf</fnm></r><r><p><f><r><q><n>2</n></q><h>·</h><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>−</v><q><n>2</n></q><h>·</h><v>My</v></r><r><n>2</n><h>·</h><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r></f></p></r></fn><v>−</v><r><p><n>2</n><h>·</h><v>B</v><h>·</h><v>Mx</v><v>−</v><n>2</n><h>·</h><v>B</v><h>·</h><v>mx</v></p></r><h>·</h><v>my</v><v>−</v><n>2</n><h>·</h><v>B</v><h>·</h><v>My</v><h>·</h><v>mx</v><v>+</v><n>2</n><h>·</h><v>B</v><h>·</h><v>Mx</v><h>·</h><v>My</v></r><r><n>2</n></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>x: 3;</line>
<line>y: 7;</line>
<line>A:1021;</line>
<line>B:2231;</line>
<line>s_x:2.45;</line>
<line>s_y:3.5;</line>
<line>x_0:14;</line>
<line>y_0:23;</line>
<line>T:%pi/32;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o9)	">(%o9) </lbl><n>3</n><lbl altCopy="(%o10)	">(%o10) </lbl><n>7</n><lbl altCopy="(%o11)	">(%o11) </lbl><n>1021</n><lbl altCopy="(%o12)	">(%o12) </lbl><n>2231</n><lbl altCopy="(%o13)	">(%o13) </lbl><n>2.45</n><lbl altCopy="(%o14)	">(%o14) </lbl><n>3.5</n><lbl altCopy="(%o15)	">(%o15) </lbl><n>14</n><lbl altCopy="(%o16)	">(%o16) </lbl><n>23</n><lbl altCopy="(%o17)	">(%o17) </lbl><f><r><s>π</s></r><r><n>32</n></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(f(x,y,A,B,x_0,y_0,s_x,s_y));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o18)	">(%o18) </lbl><n>2231.000001241022</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(%e^(-(y-y_0)^2/(2*s_y^2)-(x-x_0)^2/(2*s_x^2)));</line>
<line>float((A*(x-x_0)*%e^(-(y-y_0)^2/(2*s_y^2)-(x-x_0)^2/(2*s_x^2)))/s_x^2);</line>
<line>float((A*(y-y_0)*%e^(-(y-y_0)^2/(2*s_y^2)-(x-x_0)^2/(2*s_x^2)))/s_y^2);</line>
<line>float((A*(x-x_0)^2*%e^(-(y-y_0)^2/(2*s_y^2)-(x-x_0)^2/(2*s_x^2)))/s_x^3);</line>
<line>float((A*(y-y_0)^2*%e^(-(y-y_0)^2/(2*s_y^2)-(x-x_0)^2/(2*s_x^2)))/s_y^3);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o19)	">(%o19) </lbl><n>1.215496397313897</n><h>·</h><e><r><n>10</n></r><r><n>−9</n></r></e><lbl altCopy="(%o20)	">(%o20) </lbl><v>−</v><n>2.27425906509494</n><h>·</h><e><r><n>10</n></r><r><n>−6</n></r></e><lbl altCopy="(%o21)	">(%o21) </lbl><v>−</v><n>1.620926460940394</n><h>·</h><e><r><n>10</n></r><r><n>−6</n></r></e><lbl altCopy="(%o22)	">(%o22) </lbl><n>1.02109590677732</n><h>·</h><e><r><n>10</n></r><r><n>−5</n></r></e><lbl altCopy="(%o23)	">(%o23) </lbl><n>7.409949535727516</n><h>·</h><e><r><n>10</n></r><r><n>−6</n></r></e>
</mth></output>
</cell>

</wxMaximaDocument>PK      g�R�B�H                       mimetypePK      g�RiQ#4  4  
             5   format.txtPK      g�RJ�o�'(  '(               �  content.xmlPK      �   �.    
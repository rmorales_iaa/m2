PK     ,�R�B�H         mimetypetext/x-wxmathmlPK     ,�RiQ#4  4  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using a text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     ,�R���M  �M     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 21.05.2-DevelopmentSnapshot   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="110" activecell="2">

<cell type="code">
<input>
<editor type="input">
<line>a : cos(T)^2/(2*sx^2) + sin(T)^2/(2*sy^2);</line>
<line>b : -sin(2*T)/(4*sx^2) + sin(2*T)/(4*sy^2);</line>
<line>c : sin(T)^2/(2*sx^2) + cos(T)^2/(2*sy^2);</line>
<line>f(x,y,A,B,sx,sy,x0,y0,T) := B + (A*exp(-(a*(x-x0)^2 + 2*b*(x-x0)*(y-y0) + c*(y-y0)^2)));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o1)	">(%o1) </lbl><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f><lbl altCopy="(%o2)	">(%o2) </lbl><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f><lbl altCopy="(%o3)	">(%o3) </lbl><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f><lbl altCopy="(%o4)	">(%o4) </lbl><fn><r><fnm>f</fnm></r><r><p><v>x</v><fnm>,</fnm><v>y</v><fnm>,</fnm><v>A</v><fnm>,</fnm><v>B</v><fnm>,</fnm><v>sx</v><fnm>,</fnm><v>sy</v><fnm>,</fnm><v>x0</v><fnm>,</fnm><v>y0</v><fnm>,</fnm><v>T</v></p></r></fn><fnm>:=</fnm><v>B</v><v>+</v><v>A</v><h>·</h><fn><r><fnm>exp</fnm></r><r><p><v>−</v><r><p><v>a</v><h>·</h><e><r><r><p><v>x</v><v>−</v><v>x0</v></p></r></r><r><n>2</n></r></e><v>+</v><n>2</n><h>·</h><v>b</v><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r><v>+</v><v>c</v><h>·</h><e><r><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><n>2</n></r></e></p></r></p></r></fn>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff(f(x,y,A,B,sx,sy,x0,y0,T),A,1);</line>
<line>diff(f(x,y,A,B,sx,sy,x0,y0,T),B,1);</line>
<line>diff(f(x,y,A,B,sx,sy,x0,y0,T),sx,1);</line>
<line>diff(f(x,y,A,B,sx,sy,x0,y0,T),sy,1);</line>
<line>diff(f(x,y,A,B,sx,sy,x0,y0,T),x0,1);</line>
<line>diff(f(x,y,A,B,sx,sy,x0,y0,T),y0,1);</line>
<line>diff(f(x,y,A,B,sx,sy,x0,y0,T),T,1);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o5)	">(%o5) </lbl><e><r><s>%e</s></r><r><v>−</v><n>2</n><h>·</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r><v>−</v><r><p><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><n>2</n></r></e><v>−</v><r><p><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>x</v><v>−</v><v>x0</v></p></r></r><r><n>2</n></r></e></r></e><lbl altCopy="(%o6)	">(%o6) </lbl><n>1</n><lbl altCopy="(%o7)	">(%o7) </lbl><v>A</v><h>·</h><r><p><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><e><r><v>sx</v></r><r><n>3</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e><h>·</h><e><r><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><n>2</n></r></e></r><r><e><r><v>sx</v></r><r><n>3</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e><h>·</h><e><r><r><p><v>x</v><v>−</v><v>x0</v></p></r></r><r><n>2</n></r></e></r><r><e><r><v>sx</v></r><r><n>3</n></r></e></r></f></p></r><h>·</h><e><r><s>%e</s></r><r><v>−</v><n>2</n><h>·</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r><v>−</v><r><p><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><n>2</n></r></e><v>−</v><r><p><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>x</v><v>−</v><v>x0</v></p></r></r><r><n>2</n></r></e></r></e><lbl altCopy="(%o8)	">(%o8) </lbl><v>A</v><h>·</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><e><r><v>sy</v></r><r><n>3</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e><h>·</h><e><r><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><n>2</n></r></e></r><r><e><r><v>sy</v></r><r><n>3</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e><h>·</h><e><r><r><p><v>x</v><v>−</v><v>x0</v></p></r></r><r><n>2</n></r></e></r><r><e><r><v>sy</v></r><r><n>3</n></r></e></r></f></p></r><h>·</h><e><r><s>%e</s></r><r><v>−</v><n>2</n><h>·</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r><v>−</v><r><p><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><n>2</n></r></e><v>−</v><r><p><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>x</v><v>−</v><v>x0</v></p></r></r><r><n>2</n></r></e></r></e><lbl altCopy="(%o9)	">(%o9) </lbl><v>A</v><h>·</h><r><p><n>2</n><h>·</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r><v>+</v><n>2</n><h>·</h><r><p><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r></p></r><h>·</h><e><r><s>%e</s></r><r><v>−</v><n>2</n><h>·</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r><v>−</v><r><p><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><n>2</n></r></e><v>−</v><r><p><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>x</v><v>−</v><v>x0</v></p></r></r><r><n>2</n></r></e></r></e><lbl altCopy="(%o10)	">(%o10) </lbl><v>A</v><h>·</h><r><p><n>2</n><h>·</h><r><p><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r><v>+</v><n>2</n><h>·</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r></p></r><h>·</h><e><r><s>%e</s></r><r><v>−</v><n>2</n><h>·</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r><v>−</v><r><p><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><n>2</n></r></e><v>−</v><r><p><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>x</v><v>−</v><v>x0</v></p></r></r><r><n>2</n></r></e></r></e><lbl altCopy="(%o11)	">(%o11) </lbl><v>A</v><h>·</h><r><p><v>−</v><n>2</n><h>·</h><r><p><f><r><fn><r><fnm>cos</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>cos</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r><v>−</v><r><p><f><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><e><r><v>sx</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><e><r><v>sy</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><n>2</n></r></e><v>−</v><r><p><f><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>x</v><v>−</v><v>x0</v></p></r></r><r><n>2</n></r></e></p></r><h>·</h><e><r><s>%e</s></r><r><v>−</v><n>2</n><h>·</h><r><p><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>−</v><f><r><fn><r><fnm>sin</fnm></r><r><p><n>2</n><h>·</h><v>T</v></p></r></fn></r><r><n>4</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><r><p><v>x</v><v>−</v><v>x0</v></p></r><h>·</h><r><p><v>y</v><v>−</v><v>y0</v></p></r><v>−</v><r><p><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>y</v><v>−</v><v>y0</v></p></r></r><r><n>2</n></r></e><v>−</v><r><p><f><r><e><r><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sy</v></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn></r><r><n>2</n></r></e></r><r><n>2</n><h>·</h><e><r><v>sx</v></r><r><n>2</n></r></e></r></f></p></r><h>·</h><e><r><r><p><v>x</v><v>−</v><v>x0</v></p></r></r><r><n>2</n></r></e></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line> </line>
</editor>
</input>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>x: 3;</line>
<line>y: 7;</line>
<line>A:1021;</line>
<line>B:2231;</line>
<line>sx:2.45;</line>
<line>sy:3.5;</line>
<line>x0:14;</line>
<line>y0:23;</line>
<line>T:%pi/6;</line>
<line>a : cos(T)^2/(2*sx^2) + sin(T)^2/(2*sy^2);</line>
<line>b : -sin(2*T)/(4*sx^2) + sin(2*T)/(4*sy^2);</line>
<line>c : sin(T)^2/(2*sx^2) + cos(T)^2/(2*sy^2);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o12)	">(%o12) </lbl><n>3</n><lbl altCopy="(%o13)	">(%o13) </lbl><n>7</n><lbl altCopy="(%o14)	">(%o14) </lbl><n>1021</n><lbl altCopy="(%o15)	">(%o15) </lbl><n>2231</n><lbl altCopy="(%o16)	">(%o16) </lbl><n>2.45</n><lbl altCopy="(%o17)	">(%o17) </lbl><n>3.5</n><lbl altCopy="(%o18)	">(%o18) </lbl><n>14</n><lbl altCopy="(%o19)	">(%o19) </lbl><n>23</n><lbl altCopy="(%o20)	">(%o20) </lbl><f><r><s>π</s></r><r><n>6</n></r></f><lbl altCopy="(%o21)	">(%o21) </lbl><n>0.07267805081216158</n><lbl altCopy="(%o22)	">(%o22) </lbl><v>−</v><n>0.01062057476051644</n><h>·</h><q><n>3</n></q><lbl altCopy="(%o23)	">(%o23) </lbl><n>0.05143690129112869</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(f(x,y,A,B,sx,sy,x0,y0,T) ) ;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o24)	">(%o24) </lbl><n>2231.000191968161</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(%e^(-2*(sin(2*T)/(4*sy^2)-sin(2*T)/(4*sx^2))*(x-x0)*(y-y0)-(cos(T)^2/(2*sy^2)+sin(T)^2/(2*sx^2))*(y-y0)^2-(sin(T)^2/(2*sy^2)+cos(T)^2/(2*sx^2))*(x-x0)^2));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o25)	">(%o25) </lbl><n>1.880197461649847</n><h>·</h><e><r><n>10</n></r><r><n>−7</n></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(A*(-(sin(2*T)*(x-x0)*(y-y0))/sx^3+(sin(T)^2*(y-y0)^2)/sx^3+(cos(T)^2*(x-x0)^2)/sx^3)*%e^(-2*(sin(2*T)/(4*sy^2)-sin(2*T)/(4*sx^2))*(x-x0)*(y-y0)-(cos(T)^2/(2*sy^2)+sin(T)^2/(2*sx^2))*(y-y0)^2-(sin(T)^2/(2*sy^2)+cos(T)^2/(2*sx^2))*(x-x0)^2));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o26)	">(%o26) </lbl><n>3.040878443905965</n><h>·</h><e><r><n>10</n></r><r><n>−5</n></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(A*((sin(2*T)*(x-x0)*(y-y0))/sy^3+(cos(T)^2*(y-y0)^2)/sy^3+(sin(T)^2*(x-x0)^2)/sy^3)*%e^(-2*(sin(2*T)/(4*sy^2)-sin(2*T)/(4*sx^2))*(x-x0)*(y-y0)-(cos(T)^2/(2*sy^2)+sin(T)^2/(2*sx^2))*(y-y0)^2-(sin(T)^2/(2*sy^2)+cos(T)^2/(2*sx^2))*(x-x0)^2));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o27)	">(%o27) </lbl><n>0.001677546384828654</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(A*(2*(sin(2*T)/(4*sy^2)-sin(2*T)/(4*sx^2))*(y-y0)+2*(sin(T)^2/(2*sy^2)+cos(T)^2/(2*sx^2))*(x-x0))*%e^(-2*(sin(2*T)/(4*sy^2)-sin(2*T)/(4*sx^2))*(x-x0)*(y-y0)-(cos(T)^2/(2*sy^2)+sin(T)^2/(2*sx^2))*(y-y0)^2-(sin(T)^2/(2*sy^2)+cos(T)^2/(2*sx^2))*(x-x0)^2));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o28)	">(%o28) </lbl><v>−</v><n>1.939387360744614</n><h>·</h><e><r><n>10</n></r><r><n>−4</n></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(A*(2*(cos(T)^2/(2*sy^2)+sin(T)^2/(2*sx^2))*(y-y0)+2*(sin(2*T)/(4*sy^2)-sin(2*T)/(4*sx^2))*(x-x0))*%e^(-2*(sin(2*T)/(4*sy^2)-sin(2*T)/(4*sx^2))*(x-x0)*(y-y0)-(cos(T)^2/(2*sy^2)+sin(T)^2/(2*sx^2))*(y-y0)^2-(sin(T)^2/(2*sy^2)+cos(T)^2/(2*sx^2))*(x-x0)^2));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o29)	">(%o29) </lbl><v>−</v><n>2.382867357473069</n><h>·</h><e><r><n>10</n></r><r><n>−4</n></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(A*(-2*(cos(2*T)/(2*sy^2)-cos(2*T)/(2*sx^2))*(x-x0)*(y-y0)-((cos(T)*sin(T))/sx^2-(cos(T)*sin(T))/sy^2)*(y-y0)^2-((cos(T)*sin(T))/sy^2-(cos(T)*sin(T))/sx^2)*(x-x0)^2)*%e^(-2*(sin(2*T)/(4*sy^2)-sin(2*T)/(4*sx^2))*(x-x0)*(y-y0)-(cos(T)^2/(2*sy^2)+sin(T)^2/(2*sx^2))*(y-y0)^2-(sin(T)^2/(2*sy^2)+cos(T)^2/(2*sx^2))*(x-x0)^2));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o30)	">(%o30) </lbl><n>4.818656839710071</n><h>·</h><e><r><n>10</n></r><r><n>−4</n></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
<line></line>
<line></line>
<line></line>
<line></line>
</editor>
</input>
</cell>

</wxMaximaDocument>PK      ,�R�B�H                       mimetypePK      ,�RiQ#4  4  
             5   format.txtPK      ,�R���M  �M               �  content.xmlPK      �   �T    
PK     ���R�B�H         mimetypetext/x-wxmathmlPK     ���RiQ#4  4  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using a text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     ���Rc�i��;  �;     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 21.05.2-DevelopmentSnapshot   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="130" activecell="8">

<cell type="code">
<input>
<editor type="input">
<line>M(x,y,A,G,x_0,y_0,B,s_x,s_y,T):= G + ( A / (1+(((x*cos(T)+y*sin(T))-x_0)^2 /s_x^2)+(((-x*sin(T)+y*cos(T))-y_0)^2 /s_y^2))^B);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o1)	">(%o1) </lbl><fn><r><fnm>M</fnm></r><r><p><v>x</v><fnm>,</fnm><v>y</v><fnm>,</fnm><v>A</v><fnm>,</fnm><v>G</v><fnm>,</fnm><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><fnm>,</fnm><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><fnm>,</fnm><v>B</v><fnm>,</fnm><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><fnm>,</fnm><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><fnm>,</fnm><v>T</v></p></r></fn><fnm>:=</fnm><v>G</v><v>+</v><f><r><v>A</v></r><r><e><r><r><p><n>1</n><v>+</v><f><r><e><r><r><p><v>x</v><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><v>+</v><v>y</v><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><r><p><r><p><v>−</v><v>x</v></p></r><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><v>+</v><v>y</v><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f></p></r></r><r><v>B</v></r></e></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff(M(x,y,A,G,x_0,y_0,B,s_x,s_y,T),A,1);</line>
<line>diff(M(x,y,A,G,x_0,y_0,B,s_x,s_y,T),G,1);</line>
<line>diff(M(x,y,A,G,x_0,y_0,B,s_x,s_y,T),x_0,1);</line>
<line>diff(M(x,y,A,G,x_0,y_0,B,s_x,s_y,T),y_0,1);</line>
<line>diff(M(x,y,A,G,x_0,y_0,B,s_x,s_y,T),s_x,1);</line>
<line>diff(M(x,y,A,G,x_0,y_0,B,s_x,s_y,T),s_y,1);</line>
<line>diff(M(x,y,A,G,x_0,y_0,B,s_x,s_y,T),B,1);</line>
<line>diff(M(x,y,A,G,x_0,y_0,B,s_x,s_y,T),T,1);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o2)	">(%o2) </lbl><f><r><n>1</n></r><r><e><r><r><p><f><r><e><r><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><n>1</n></p></r></r><r><v>B</v></r></e></r></f><lbl altCopy="(%o3)	">(%o3) </lbl><n>1</n><lbl altCopy="(%o4)	">(%o4) </lbl><f><r><n>2</n><h>·</h><v>A</v><h>·</h><v>B</v><h>·</h><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r><h>·</h><e><r><r><p><f><r><e><r><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><n>1</n></p></r></r><r><v>−</v><v>B</v><v>−</v><n>1</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><lbl altCopy="(%o5)	">(%o5) </lbl><f><r><n>2</n><h>·</h><v>A</v><h>·</h><v>B</v><h>·</h><e><r><r><p><f><r><e><r><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><n>1</n></p></r></r><r><v>−</v><v>B</v><v>−</v><n>1</n></r></e><h>·</h><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><lbl altCopy="(%o6)	">(%o6) </lbl><f><r><n>2</n><h>·</h><v>A</v><h>·</h><v>B</v><h>·</h><e><r><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e><h>·</h><e><r><r><p><f><r><e><r><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><n>1</n></p></r></r><r><v>−</v><v>B</v><v>−</v><n>1</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>3</n></r></e></r></f><lbl altCopy="(%o7)	">(%o7) </lbl><f><r><n>2</n><h>·</h><v>A</v><h>·</h><v>B</v><h>·</h><e><r><r><p><f><r><e><r><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><n>1</n></p></r></r><r><v>−</v><v>B</v><v>−</v><n>1</n></r></e><h>·</h><e><r><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>3</n></r></e></r></f><lbl altCopy="(%o8)	">(%o8) </lbl><v>−</v><f><r><v>A</v><h>·</h><fn><r><fnm>log</fnm></r><r><p><f><r><e><r><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><n>1</n></p></r></fn></r><r><e><r><r><p><f><r><e><r><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><n>1</n></p></r></r><r><v>B</v></r></e></r></f><lbl altCopy="(%o9)	">(%o9) </lbl><v>−</v><v>A</v><h>·</h><v>B</v><h>·</h><e><r><r><p><f><r><e><r><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><e><r><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><n>2</n></r></e></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><n>1</n></p></r></r><r><v>−</v><v>B</v><v>−</v><n>1</n></r></e><h>·</h><r><p><f><r><n>2</n><h>·</h><r><p><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r><h>·</h><r><p><v>−</v><i altCopy="y_0"><r><v>y</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><e><r><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></r><r><n>2</n></r></e></r></f><v>+</v><f><r><n>2</n><h>·</h><r><p><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r><h>·</h><r><p><fn><r><fnm>sin</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>y</v><v>−</v><i altCopy="x_0"><r><v>x</v></r><r><v>0</v></r></i><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>T</v></p></r></fn><h>·</h><v>x</v></p></r></r><r><e><r><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></r><r><n>2</n></r></e></r></f></p></r>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>x: 3;</line>
<line>y: 7;</line>
<line>A:1021;</line>
<line>G:2231;</line>
<line>x_0:14;</line>
<line>y_0:23;</line>
<line>B: 3.14;</line>
<line>s_x:2.45;</line>
<line>s_y:3.5;</line>
<line>T:%pi/32;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o10)	">(%o10) </lbl><n>3</n><lbl altCopy="(%o11)	">(%o11) </lbl><n>7</n><lbl altCopy="(%o12)	">(%o12) </lbl><n>1021</n><lbl altCopy="(%o13)	">(%o13) </lbl><n>2231</n><lbl altCopy="(%o14)	">(%o14) </lbl><n>14</n><lbl altCopy="(%o15)	">(%o15) </lbl><n>23</n><lbl altCopy="(%o16)	">(%o16) </lbl><n>3.14</n><lbl altCopy="(%o17)	">(%o17) </lbl><n>2.45</n><lbl altCopy="(%o18)	">(%o18) </lbl><n>3.5</n><lbl altCopy="(%o19)	">(%o19) </lbl><f><r><s>π</s></r><r><n>32</n></r></f>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(M(x,y,A,G,x_0,y_0,B,s_x,s_y,T));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o20)	">(%o20) </lbl><n>2231.009129641967</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(1/((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1)^B);</line>
<line>float((2*A*B*(sin(T)*y-x_0+cos(T)*x)*((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1)^(-B-1))/s_x^2);</line>
<line>float((2*A*B*((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1)^(-B-1)*(-y_0+cos(T)*y-sin(T)*x))/s_y^2);</line>
<line>float((2*A*B*(sin(T)*y-x_0+cos(T)*x)^2*((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1)^(-B-1))/s_x^3);</line>
<line>float((2*A*B*((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1)^(-B-1)*(-y_0+cos(T)*y-sin(T)*x)^2)/s_y^3);</line>
<line>float(-(A*log((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1))/((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1)^B);</line>
<line>float(-A*B*((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1)^(-B-1)*((2*(-sin(T)*y-cos(T)*x)*(-y_0+cos(T)*y-sin(T)*x))/s_y^2+(2*(cos(T)*y-sin(T)*x)*(sin(T)*y-x_0+cos(T)*x))/s_x^2));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o21)	">(%o21) </lbl><n>8.941862847607242</n><h>·</h><e><r><n>10</n></r><r><n>−6</n></r></e><lbl altCopy="(%o22)	">(%o22) </lbl><v>−</v><n>0.002433804420662086</n><lbl altCopy="(%o23)	">(%o23) </lbl><v>−</v><n>0.001885290976353062</n><lbl altCopy="(%o24)	">(%o24) </lbl><n>0.0102600510537859</n><lbl altCopy="(%o25)	">(%o25) </lbl><n>0.008795021557627949</n><lbl altCopy="(%o26)	">(%o26) </lbl><v>−</v><n>0.03379934942021254</n><lbl altCopy="(%o27)	">(%o27) </lbl><n>0.009316757095409845</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
<line>float(A*log((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o28)	">(%o28) </lbl><n>3779.900228424654</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float(((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1)^B);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o29)	">(%o29) </lbl><n>111833.5202678254</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>float((A*log((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1))/((-y_0+cos(T)*y-sin(T)*x)^2/s_y^2+(sin(T)*y-x_0+cos(T)*x)^2/s_x^2+1)^B);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o30)	">(%o30) </lbl><n>0.03379934942021254</n>
</mth></output>
</cell>

</wxMaximaDocument>PK      ���R�B�H                       mimetypePK      ���RiQ#4  4  
             5   format.txtPK      ���Rc�i��;  �;               �  content.xmlPK      �   �B    
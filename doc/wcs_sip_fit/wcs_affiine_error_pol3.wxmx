PK     (^#V�B�H         mimetypetext/x-wxmathmlPK     (^#ViQ#4  4  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using a text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     (^#V��TX         content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 22.09.0   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="110" activecell="1">

<cell type="code">
<input>
<editor type="input">
<line>/* &quot;The SIP convention for representing distorsion in FITS image headers&quot; (Shupe et al. 2005) */</line>
<line></line>
<line>e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33):=</line>
<line>a_00*x^0*y^0 +</line>
<line>a_01*x^0*y^1 +</line>
<line>a_02*x^0*y^2 +</line>
<line>a_03*x^0*y^3 +</line>
<line></line>
<line></line>
<line>a_10*x^1*y^0 +</line>
<line>a_11*x^1*y^1 +</line>
<line>a_12*x^1*y^2 +</line>
<line>a_13*x^1*y^3 +</line>
<line></line>
<line></line>
<line>a_20*x^2*y^0 +</line>
<line>a_21*x^2*y^1 +</line>
<line>a_22*x^2*y^2 +</line>
<line>a_23*x^2*y^3 +</line>
<line></line>
<line></line>
<line>a_30*x^3*y^0 +</line>
<line>a_31*x^3*y^1 +</line>
<line>a_32*x^3*y^2 +</line>
<line>a_33*x^3*y^3 </line>
<line></line>
<line>;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o1)	">(%o1) </lbl><fn><r><fnm>e</fnm></r><r><p><v>x</v><fnm>,</fnm><v>y</v><fnm>,</fnm><i altCopy="a_00"><r><v>a</v></r><r><v>00</v></r></i><fnm>,</fnm><i altCopy="a_01"><r><v>a</v></r><r><v>01</v></r></i><fnm>,</fnm><i altCopy="a_02"><r><v>a</v></r><r><v>02</v></r></i><fnm>,</fnm><i altCopy="a_03"><r><v>a</v></r><r><v>03</v></r></i><fnm>,</fnm><i altCopy="a_10"><r><v>a</v></r><r><v>10</v></r></i><fnm>,</fnm><i altCopy="a_11"><r><v>a</v></r><r><v>11</v></r></i><fnm>,</fnm><i altCopy="a_12"><r><v>a</v></r><r><v>12</v></r></i><fnm>,</fnm><i altCopy="a_13"><r><v>a</v></r><r><v>13</v></r></i><fnm>,</fnm><i altCopy="a_20"><r><v>a</v></r><r><v>20</v></r></i><fnm>,</fnm><i altCopy="a_21"><r><v>a</v></r><r><v>21</v></r></i><fnm>,</fnm><i altCopy="a_22"><r><v>a</v></r><r><v>22</v></r></i><fnm>,</fnm><i altCopy="a_23"><r><v>a</v></r><r><v>23</v></r></i><fnm>,</fnm><i altCopy="a_30"><r><v>a</v></r><r><v>30</v></r></i><fnm>,</fnm><i altCopy="a_31"><r><v>a</v></r><r><v>31</v></r></i><fnm>,</fnm><i altCopy="a_32"><r><v>a</v></r><r><v>32</v></r></i><fnm>,</fnm><i altCopy="a_33"><r><v>a</v></r><r><v>33</v></r></i></p></r></fn><fnm>:=</fnm><i altCopy="a_00"><r><v>a</v></r><r><v>00</v></r></i><h>·</h><e><r><v>x</v></r><r><n>0</n></r></e><h>·</h><e><r><v>y</v></r><r><n>0</n></r></e><v>+</v><i altCopy="a_01"><r><v>a</v></r><r><v>01</v></r></i><h>·</h><e><r><v>x</v></r><r><n>0</n></r></e><h>·</h><e><r><v>y</v></r><r><n>1</n></r></e><v>+</v><i altCopy="a_02"><r><v>a</v></r><r><v>02</v></r></i><h>·</h><e><r><v>x</v></r><r><n>0</n></r></e><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><v>+</v><i altCopy="a_03"><r><v>a</v></r><r><v>03</v></r></i><h>·</h><e><r><v>x</v></r><r><n>0</n></r></e><h>·</h><e><r><v>y</v></r><r><n>3</n></r></e><v>+</v><i altCopy="a_10"><r><v>a</v></r><r><v>10</v></r></i><h>·</h><e><r><v>x</v></r><r><n>1</n></r></e><h>·</h><e><r><v>y</v></r><r><n>0</n></r></e><v>+</v><i altCopy="a_11"><r><v>a</v></r><r><v>11</v></r></i><h>·</h><e><r><v>x</v></r><r><n>1</n></r></e><h>·</h><e><r><v>y</v></r><r><n>1</n></r></e><v>+</v><i altCopy="a_12"><r><v>a</v></r><r><v>12</v></r></i><h>·</h><e><r><v>x</v></r><r><n>1</n></r></e><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><v>+</v><i altCopy="a_13"><r><v>a</v></r><r><v>13</v></r></i><h>·</h><e><r><v>x</v></r><r><n>1</n></r></e><h>·</h><e><r><v>y</v></r><r><n>3</n></r></e><v>+</v><i altCopy="a_20"><r><v>a</v></r><r><v>20</v></r></i><h>·</h><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><e><r><v>y</v></r><r><n>0</n></r></e><v>+</v><i altCopy="a_21"><r><v>a</v></r><r><v>21</v></r></i><h>·</h><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><e><r><v>y</v></r><r><n>1</n></r></e><v>+</v><i altCopy="a_22"><r><v>a</v></r><r><v>22</v></r></i><h>·</h><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><v>+</v><i altCopy="a_23"><r><v>a</v></r><r><v>23</v></r></i><h>·</h><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><e><r><v>y</v></r><r><n>3</n></r></e><v>+</v><i altCopy="a_30"><r><v>a</v></r><r><v>30</v></r></i><h>·</h><e><r><v>x</v></r><r><n>3</n></r></e><h>·</h><e><r><v>y</v></r><r><n>0</n></r></e><v>+</v><i altCopy="a_31"><r><v>a</v></r><r><v>31</v></r></i><h>·</h><e><r><v>x</v></r><r><n>3</n></r></e><h>·</h><e><r><v>y</v></r><r><n>1</n></r></e><v>+</v><i altCopy="a_32"><r><v>a</v></r><r><v>32</v></r></i><h>·</h><e><r><v>x</v></r><r><n>3</n></r></e><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><v>+</v><i altCopy="a_33"><r><v>a</v></r><r><v>33</v></r></i><h>·</h><e><r><v>x</v></r><r><n>3</n></r></e><h>·</h><e><r><v>y</v></r><r><n>3</n></r></e>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>≣;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o2)	">(%o2) </lbl><v>≣</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_00,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_01,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_02,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_03,1);</line>
<line></line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_10,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_11,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_12,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_13,1);</line>
<line></line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_20,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_21,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_22,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_23,1);</line>
<line></line>
<line></line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_30,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_31,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_32,1);</line>
<line>diff(e(x,y,a_00,a_01,a_02,a_03,a_10,a_11,a_12,a_13,a_20,a_21,a_22,a_23,a_30,a_31,a_32,a_33),a_33,1);</line>
<line></line>
<line></line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o3)	">(%o3) </lbl><n>1</n><lbl altCopy="(%o4)	">(%o4) </lbl><v>y</v><lbl altCopy="(%o5)	">(%o5) </lbl><e><r><v>y</v></r><r><n>2</n></r></e><lbl altCopy="(%o6)	">(%o6) </lbl><e><r><v>y</v></r><r><n>3</n></r></e><lbl altCopy="(%o7)	">(%o7) </lbl><v>x</v><lbl altCopy="(%o8)	">(%o8) </lbl><v>x</v><h>·</h><v>y</v><lbl altCopy="(%o9)	">(%o9) </lbl><v>x</v><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><lbl altCopy="(%o10)	">(%o10) </lbl><v>x</v><h>·</h><e><r><v>y</v></r><r><n>3</n></r></e><lbl altCopy="(%o11)	">(%o11) </lbl><e><r><v>x</v></r><r><n>2</n></r></e><lbl altCopy="(%o12)	">(%o12) </lbl><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><v>y</v><lbl altCopy="(%o13)	">(%o13) </lbl><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><lbl altCopy="(%o14)	">(%o14) </lbl><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><e><r><v>y</v></r><r><n>3</n></r></e><lbl altCopy="(%o15)	">(%o15) </lbl><e><r><v>x</v></r><r><n>3</n></r></e><lbl altCopy="(%o16)	">(%o16) </lbl><e><r><v>x</v></r><r><n>3</n></r></e><h>·</h><v>y</v><lbl altCopy="(%o17)	">(%o17) </lbl><e><r><v>x</v></r><r><n>3</n></r></e><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><lbl altCopy="(%o18)	">(%o18) </lbl><e><r><v>x</v></r><r><n>3</n></r></e><h>·</h><e><r><v>y</v></r><r><n>3</n></r></e>
</mth></output>
</cell>

</wxMaximaDocument>PK       (^#V�B�H                       mimetypePK       (^#ViQ#4  4  
             5   format.txtPK       (^#V��TX                   �  content.xmlPK      �   �&    
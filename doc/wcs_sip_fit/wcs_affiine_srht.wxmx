PK     E�%V�B�H         mimetypetext/x-wxmathmlPK     E�%ViQ#4  4  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using a text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     E�%V ��N�/  �/     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 22.09.0   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="140">

<cell type="code">
<input>
<editor type="input">
<line>/* https://www.mathworks.com/discovery/affine-transformation.html</line>
<line>https://math.stackexchange.com/questions/612006/decomposing-an-affine-transformationbbbvv */</line>
<line></line>
<line>T: matrix([1,0,0],[0,1,0],[t_x,t_y,0]);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o1)	">(%o1) </lbl><tb roundedParens="true"><mtr><mtd><n>1</n></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>1</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><i altCopy="t_x"><r><v>t</v></r><r><v>x</v></r></i></mtd><mtd><i altCopy="t_y"><r><v>t</v></r><r><v>y</v></r></i></mtd><mtd><n>0</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>S: matrix([s_x,0,0],[0,s_y,0],[0,0,1]);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o2)	">(%o2) </lbl><tb roundedParens="true"><mtr><mtd><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i></mtd><mtd><n>0</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>H: matrix([1,h_y,0],[h_x,1,0],[0,0,1]);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o3)	">(%o3) </lbl><tb roundedParens="true"><mtr><mtd><n>1</n></mtd><mtd><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i></mtd><mtd><n>1</n></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>R: matrix([cos(θ),sin(θ),0],[-sin(θ),cos(θ),0],[0,0,1]);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o4)	">(%o4) </lbl><tb roundedParens="true"><mtr><mtd><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></mtd><mtd><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></mtd><mtd><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><n>0</n></mtd><mtd><n>0</n></mtd><mtd><n>1</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>  C : (S.R.H.T); </line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o5)	">(%o5) </lbl><tb roundedParens="true"><mtr><mtd><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><r><p><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></p></r></mtd><mtd><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><r><p><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><v>+</v><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></p></r></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><r><p><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></p></r></mtd><mtd><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><r><p><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></p></r></mtd><mtd><n>0</n></mtd></mtr><mtr><mtd><i altCopy="t_x"><r><v>t</v></r><r><v>x</v></r></i></mtd><mtd><i altCopy="t_y"><r><v>t</v></r><r><v>y</v></r></i></mtd><mtd><n>0</n></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>P: matrix([x,y,z]);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o6)	">(%o6) </lbl><tb roundedParens="true"><mtr><mtd><v>x</v></mtd><mtd><v>y</v></mtd><mtd><v>z</v></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>K = (C.P);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o7)	">(%o7) </lbl><v>K</v><v>=</v><tb roundedParens="true"><mtr><mtd><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><v>x</v><h>·</h><r><p><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></p></r><v>+</v><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><v>y</v><h>·</h><r><p><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><v>+</v><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></p></r></mtd></mtr><mtr><mtd><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><v>y</v><h>·</h><r><p><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></p></r><v>+</v><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><v>x</v><h>·</h><r><p><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></p></r></mtd></mtr><mtr><mtd><i altCopy="t_y"><r><v>t</v></r><r><v>y</v></r></i><h>·</h><v>y</v><v>+</v><i altCopy="t_x"><r><v>t</v></r><r><v>x</v></r></i><h>·</h><v>x</v></mtd></mtr></tb>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>X(x,y,s_x,h_x,h_y,θ) :=s_x*x*(h_x*sin(θ)+cos(θ))+s_x*y*(sin(θ)+h_y*cos(θ));</line>
<line></line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o8)	">(%o8) </lbl><fn><r><fnm>X</fnm></r><r><p><v>x</v><fnm>,</fnm><v>y</v><fnm>,</fnm><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><fnm>,</fnm><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><fnm>,</fnm><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><fnm>,</fnm><v>θ</v></p></r></fn><fnm>:=</fnm><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><v>x</v><h>·</h><r><p><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></p></r><v>+</v><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><v>y</v><h>·</h><r><p><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><v>+</v><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></p></r>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff(X(x,y,s_x,h_x,h_y,θ) ,s_x,1);</line>
<line>diff(X(x,y,s_x,h_x,h_y,θ) ,h_x,1);</line>
<line>diff(X(x,y,s_x,h_x,h_y,θ) ,h_y,1);</line>
<line>diff(X(x,y,s_x,h_x,h_y,θ) ,θ,1);</line>
<line></line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o9)	">(%o9) </lbl><v>x</v><h>·</h><r><p><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><v>+</v><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></p></r><v>+</v><v>y</v><h>·</h><r><p><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><v>+</v><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></p></r><lbl altCopy="(%o10)	">(%o10) </lbl><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><v>x</v><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><lbl altCopy="(%o11)	">(%o11) </lbl><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><v>y</v><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><lbl altCopy="(%o12)	">(%o12) </lbl><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><v>y</v><h>·</h><r><p><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></p></r><v>+</v><i altCopy="s_x"><r><v>s</v></r><r><v>x</v></r></i><h>·</h><v>x</v><h>·</h><r><p><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></p></r>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>Y(x,y,s_y,h_x,h_y,θ)  :=s_y*y*(cos(θ)-h_y*sin(θ))+s_y*x*(h_x*cos(θ)-sin(θ));</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o13)	">(%o13) </lbl><fn><r><fnm>Y</fnm></r><r><p><v>x</v><fnm>,</fnm><v>y</v><fnm>,</fnm><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><fnm>,</fnm><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><fnm>,</fnm><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><fnm>,</fnm><v>θ</v></p></r></fn><fnm>:=</fnm><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><v>y</v><h>·</h><r><p><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></p></r><v>+</v><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><v>x</v><h>·</h><r><p><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></p></r>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff(Y(x,y,s_y,h_x,h_y,θ),s_y,1);</line>
<line>diff(Y(x,y,s_y,h_x,h_y,θ),h_x,1);</line>
<line>diff(Y(x,y,s_y,h_x,h_y,θ),h_y,1);</line>
<line>diff(Y(x,y,s_y,h_x,h_y,θ),θ,1);</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o14)	">(%o14) </lbl><v>y</v><h>·</h><r><p><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></p></r><v>+</v><v>x</v><h>·</h><r><p><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn></p></r><lbl altCopy="(%o15)	">(%o15) </lbl><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><v>x</v><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn><lbl altCopy="(%o16)	">(%o16) </lbl><v>−</v><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><v>y</v><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><lbl altCopy="(%o17)	">(%o17) </lbl><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><v>x</v><h>·</h><r><p><v>−</v><i altCopy="h_x"><r><v>h</v></r><r><v>x</v></r></i><h>·</h><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></p></r><v>+</v><i altCopy="s_y"><r><v>s</v></r><r><v>y</v></r></i><h>·</h><v>y</v><h>·</h><r><p><v>−</v><fn><r><fnm>sin</fnm></r><r><p><v>θ</v></p></r></fn><v>−</v><i altCopy="h_y"><r><v>h</v></r><r><v>y</v></r></i><h>·</h><fn><r><fnm>cos</fnm></r><r><p><v>θ</v></p></r></fn></p></r>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>Z(x,y,t_x,t_y) :=t_y*y+t_x*x;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o18)	">(%o18) </lbl><fn><r><fnm>Z</fnm></r><r><p><v>x</v><fnm>,</fnm><v>y</v><fnm>,</fnm><i altCopy="t_x"><r><v>t</v></r><r><v>x</v></r></i><fnm>,</fnm><i altCopy="t_y"><r><v>t</v></r><r><v>y</v></r></i></p></r></fn><fnm>:=</fnm><i altCopy="t_y"><r><v>t</v></r><r><v>y</v></r></i><h>·</h><v>y</v><v>+</v><i altCopy="t_x"><r><v>t</v></r><r><v>x</v></r></i><h>·</h><v>x</v>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff(Z(x,y,t_x,t_y),t_x,1);</line>
<line>diff(Z(x,y,t_x,t_y),t_y,1);</line>
<line></line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o19)	">(%o19) </lbl><v>x</v><lbl altCopy="(%o20)	">(%o20) </lbl><v>y</v>
</mth></output>
</cell>

</wxMaximaDocument>PK       E�%V�B�H                       mimetypePK       E�%ViQ#4  4  
             5   format.txtPK       E�%V ��N�/  �/               �  content.xmlPK      �   U6    
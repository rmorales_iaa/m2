PK     5��X�B�H         mimetypetext/x-wxmathmlPK     5��XiQ#4  4  
   format.txt

This file contains a wxMaxima session in the .wxmx format.
.wxmx files are .xml-based files contained in a .zip container like .odt
or .docx files. After changing their name to end in .zip the .xml and
eventual bitmap files inside them can be extracted using any .zip file
viewer.
The reason why part of a .wxmx file still might still seem to make sense in a
ordinary text viewer is that the text portion of .wxmx by default
isn't compressed: The text is typically small and compressing it would
mean that changing a single character would (with a high probability) change
big parts of the  whole contents of the compressed .zip archive.
Even if version control tools like git and svn that remember all changes
that were ever made to a file can handle binary files compression would
make the changed part of the file bigger and therefore seriously reduce
the efficiency of version control

wxMaxima can be downloaded from https://github.com/wxMaxima-developers/wxmaxima.
It also is part of the windows installer for maxima
(https://wxmaxima-developers.github.io/wxmaxima/).

If a .wxmx file is broken but the content.xml portion of the file can still be
viewed using a text editor just save the xml's text as "content.xml"
and try to open it using a recent version of wxMaxima.
If it is valid XML (the XML header is intact, all opened tags are closed again,
the text is saved with the text encoding "UTF8 without BOM" and the few
special characters XML requires this for are properly escaped)
chances are high that wxMaxima will be able to recover all code and text
from the XML file.

PK     5��XLE �F  F     content.xml<?xml version="1.0" encoding="UTF-8"?>

<!--   Created using wxMaxima 24.02.1   -->
<!--https://wxMaxima-developers.github.io/wxmaxima/-->

<wxMaximaDocument version="1.5" zoom="90">

<cell type="code">
<input>
<editor type="input">
<line>f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14) :=a_0 * x^4 + a_1 * x^3*y + a_2 * x^2 * y^2+ a_3 * x * y^3+ a_4 * y^4+ a_5 * x^3 + a_6 * x^2 * y+ a_7 * x*y^2+ a_8 * y^3+ a_9 * x^2+ a_10 * x * y+ a_11 * y^2+ a_12 * x+ a_13 * y+ a_14;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o1)&#009;">(%o1) </lbl><fn><r><fnm>f_xy</fnm></r><r><p><i altCopy="a_0"><r><v>a</v></r><r><v>0</v></r></i><fnm>,</fnm><i altCopy="a_1"><r><v>a</v></r><r><v>1</v></r></i><fnm>,</fnm><i altCopy="a_2"><r><v>a</v></r><r><v>2</v></r></i><fnm>,</fnm><i altCopy="a_3"><r><v>a</v></r><r><v>3</v></r></i><fnm>,</fnm><i altCopy="a_4"><r><v>a</v></r><r><v>4</v></r></i><fnm>,</fnm><i altCopy="a_5"><r><v>a</v></r><r><v>5</v></r></i><fnm>,</fnm><i altCopy="a_6"><r><v>a</v></r><r><v>6</v></r></i><fnm>,</fnm><i altCopy="a_7"><r><v>a</v></r><r><v>7</v></r></i><fnm>,</fnm><i altCopy="a_8"><r><v>a</v></r><r><v>8</v></r></i><fnm>,</fnm><i altCopy="a_9"><r><v>a</v></r><r><v>9</v></r></i><fnm>,</fnm><i altCopy="a_10"><r><v>a</v></r><r><v>10</v></r></i><fnm>,</fnm><i altCopy="a_11"><r><v>a</v></r><r><v>11</v></r></i><fnm>,</fnm><i altCopy="a_12"><r><v>a</v></r><r><v>12</v></r></i><fnm>,</fnm><i altCopy="a_13"><r><v>a</v></r><r><v>13</v></r></i><fnm>,</fnm><i altCopy="a_14"><r><v>a</v></r><r><v>14</v></r></i></p></r></fn><fnm>:=</fnm><i altCopy="a_0"><r><v>a</v></r><r><v>0</v></r></i><h>·</h><e><r><v>x</v></r><r><n>4</n></r></e><t type="Operator">+</t><i altCopy="a_1"><r><v>a</v></r><r><v>1</v></r></i><h>·</h><e><r><v>x</v></r><r><n>3</n></r></e><h>·</h><v>y</v><t type="Operator">+</t><i altCopy="a_2"><r><v>a</v></r><r><v>2</v></r></i><h>·</h><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><t type="Operator">+</t><i altCopy="a_3"><r><v>a</v></r><r><v>3</v></r></i><h>·</h><v>x</v><h>·</h><e><r><v>y</v></r><r><n>3</n></r></e><t type="Operator">+</t><i altCopy="a_4"><r><v>a</v></r><r><v>4</v></r></i><h>·</h><e><r><v>y</v></r><r><n>4</n></r></e><t type="Operator">+</t><i altCopy="a_5"><r><v>a</v></r><r><v>5</v></r></i><h>·</h><e><r><v>x</v></r><r><n>3</n></r></e><t type="Operator">+</t><i altCopy="a_6"><r><v>a</v></r><r><v>6</v></r></i><h>·</h><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><v>y</v><t type="Operator">+</t><i altCopy="a_7"><r><v>a</v></r><r><v>7</v></r></i><h>·</h><v>x</v><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><t type="Operator">+</t><i altCopy="a_8"><r><v>a</v></r><r><v>8</v></r></i><h>·</h><e><r><v>y</v></r><r><n>3</n></r></e><t type="Operator">+</t><i altCopy="a_9"><r><v>a</v></r><r><v>9</v></r></i><h>·</h><e><r><v>x</v></r><r><n>2</n></r></e><t type="Operator">+</t><i altCopy="a_10"><r><v>a</v></r><r><v>10</v></r></i><h>·</h><v>x</v><h>·</h><v>y</v><t type="Operator">+</t><i altCopy="a_11"><r><v>a</v></r><r><v>11</v></r></i><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><t type="Operator">+</t><i altCopy="a_12"><r><v>a</v></r><r><v>12</v></r></i><h>·</h><v>x</v><t type="Operator">+</t><i altCopy="a_13"><r><v>a</v></r><r><v>13</v></r></i><h>·</h><v>y</v><t type="Operator">+</t><i altCopy="a_14"><r><v>a</v></r><r><v>14</v></r></i>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line>diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_0);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_1);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_2);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_3);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_4);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_5);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_6);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_7);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_8);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_9);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_10);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_11);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_12);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_13);&#010;diff(f_xy(a_0,a_1,a_2,a_3,a_4,a_5,a_6,a_7,a_8,a_9,a_10,a_11,a_12,a_13,a_14),a_14);&#010;&#010;</line>
</editor>
</input>
<output>
<mth><lbl altCopy="(%o2)&#009;">(%o2) </lbl><e><r><v>x</v></r><r><n>4</n></r></e><lbl altCopy="(%o3)&#009;">(%o3) </lbl><e><r><v>x</v></r><r><n>3</n></r></e><h>·</h><v>y</v><lbl altCopy="(%o4)&#009;">(%o4) </lbl><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><lbl altCopy="(%o5)&#009;">(%o5) </lbl><v>x</v><h>·</h><e><r><v>y</v></r><r><n>3</n></r></e><lbl altCopy="(%o6)&#009;">(%o6) </lbl><e><r><v>y</v></r><r><n>4</n></r></e><lbl altCopy="(%o7)&#009;">(%o7) </lbl><e><r><v>x</v></r><r><n>3</n></r></e><lbl altCopy="(%o8)&#009;">(%o8) </lbl><e><r><v>x</v></r><r><n>2</n></r></e><h>·</h><v>y</v><lbl altCopy="(%o9)&#009;">(%o9) </lbl><v>x</v><h>·</h><e><r><v>y</v></r><r><n>2</n></r></e><lbl altCopy="(%o10)&#009;">(%o10) </lbl><e><r><v>y</v></r><r><n>3</n></r></e><lbl altCopy="(%o11)&#009;">(%o11) </lbl><e><r><v>x</v></r><r><n>2</n></r></e><lbl altCopy="(%o12)&#009;">(%o12) </lbl><v>x</v><h>·</h><v>y</v><lbl altCopy="(%o13)&#009;">(%o13) </lbl><e><r><v>y</v></r><r><n>2</n></r></e><lbl altCopy="(%o14)&#009;">(%o14) </lbl><v>x</v><lbl altCopy="(%o15)&#009;">(%o15) </lbl><v>y</v><lbl altCopy="(%o16)&#009;">(%o16) </lbl><n>1</n>
</mth></output>
</cell>

<cell type="code">
<input>
<editor type="input">
<line></line>
</editor>
</input>
<output>
<mth><t breakline="true">Message from maxima&apos;s stderr stream: </t>
</mth></output>
</cell>

</wxMaximaDocument>PK       5��X�B�H                       mimetypePK       5��XiQ#4  4  
             5   format.txtPK       5��XLE �F  F               �  content.xmlPK      �        
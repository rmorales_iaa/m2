#!/bin/bash
#------------------------------------
echo "First fix wcs win m2.fixWCS_solving"
#------------------------------------
YEAR=$1

OK=/mnt/uxmal_groups/image_storage/om_cal_oct_2024/om_cal_$YEAR/wcs
I=/mnt/uxmal_groups/image_storage/om_cal_oct_2024/om_cal_$YEAR/wcs/not_solved/
O=/mnt/uxmal_groups/image_storage/om_cal_oct_2024/om_cal_$YEAR/wcs/try_2

cd /home/rafa/proyecto/m2/deploy/input/astrometry.net/
./wcs_classic_parallel_solve_fits_no_recursive.bash $I $O

rm $I/*.fits
mv $O/*.fits  $OK
mv $O/not_solved/*.fits $I
rm -fr $O
#------------------------------------

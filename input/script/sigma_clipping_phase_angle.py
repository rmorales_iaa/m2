import pandas as pd
import numpy as np

iteration = 100
sigma_scale = 2.5
use_central_value_as_median = True

def sigma_clipping_double(data):
    old_stat = None

    for _ in range(iteration):
        if len(data) == 0:
            return data

        # Convert 'est_reduced_magnitude' to numeric
        data['est_reduced_magnitude'] = pd.to_numeric(data['est_reduced_magnitude'], errors='coerce')

        # Drop rows with NaN values in 'est_reduced_magnitude'
        data = data.dropna(subset=['est_reduced_magnitude'])

        new_stat = np.median(data['est_reduced_magnitude']), np.std(data['est_reduced_magnitude'])
        median, std_dev = new_stat
        central_value = median if use_central_value_as_median else std_dev
        a = central_value - (sigma_scale * std_dev)
        b = central_value + (sigma_scale * std_dev)

        data = data[(data['est_reduced_magnitude'] >= a) & (data['est_reduced_magnitude'] <= b)]

        if old_stat is not None and len(data) == len(old_stat):
            return data  # same amount of items

        old_stat = data.copy()

    return data

# Read input TSV file
input_file_path = 'light_curve.csv'
df = pd.read_csv(input_file_path, delimiter='\t')

# Apply sigma clipping
result_df = sigma_clipping_double(df)

# Write result to TSV file
output_file_path = 'light_curve_sigma_clipped.tsv'
result_df.to_csv(output_file_path, sep='\t', index=False)

print(f"Result saved to: {output_file_path}")

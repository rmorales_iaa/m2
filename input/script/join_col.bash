#!/bin/bash

#set -x
DIR="/home/rafa/Downloads/"
OUTPUT_FILE="/home/rafa/Downloads/deleteme/joined.csv"
OUTPUT_FILE_GROUP="/home/rafa/Downloads/deleteme/joined_group.csv"

# Remove the output file if it already exists
if [ -f "$output_file" ]; then
    rm "$output_file"
fi

for file in "$DIR"/*.csv; do
    tail -n +2 "$file" | cut -f2 "$file" >> "$OUTPUT_FILE"
done

cat $OUTPUT_FILE  | sort -n | uniq -c | sort -rn > $OUTPUT_FILE_GROUP

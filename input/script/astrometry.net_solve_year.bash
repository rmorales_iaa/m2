#!/bin/bash


cd /home/rafa/proyecto/m2/input/astrometry.net/

for YEAR in {2024..2024}
do
  echo "Processing year $YEAR..."

  dropcaches_system_ctl
  
  I=/mnt/uxmal_groups/image_storage/om_cal_oct_2024/om_cal_$YEAR/science/
  O=/mnt/uxmal_groups/image_storage/om_cal_oct_2024/om_cal_$YEAR/wcs/

  ./wcs_classic_parallel_solve_fits.bash $I $O

done

echo "All years processed."


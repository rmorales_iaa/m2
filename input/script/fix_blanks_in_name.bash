DIR=/mnt/uxmal_groups/image_storage/dss16_disk3/observaciones/2023

find $DIR -depth -type d -name "* *" -exec bash -c 'mv "$1" "${1// /_}"' _ {} \;
find $DIR -depth -type f -name "* *" -exec bash -c 'mv "$1" "${1// /_}"' _ {} \;

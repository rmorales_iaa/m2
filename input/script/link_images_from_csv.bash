#!/bin/bash

# Directorio donde se encuentran los archivos de imagen
d1="/mnt/uxmal_groups/spark/tmp/mpo_20020000_varuna/"

# Directorio donde se crearán los enlaces simbólicos
d2="/fast_disk/tmp/varuna/mpo_20020000_varuna_2019_detected/"

# Archivo CSV que contiene los nombres de las imágenes
csv_file="/home/rafa/Downloads/work_with_users/nico/varuna/varuna_2019_detected"

# Crear el directorio d2 si no existe
rm -fr "$d2"
mkdir -p "$d2"

# Iterar sobre cada línea del archivo CSV
while IFS=, read -r imagen
do
    # Verificar si el archivo de imagen existe en d1
    if [ -f "$d1/$imagen.fits" ]; then
        # Crear un enlace simbólico en d2
        #ln -s "$d1/$imagen.fits" "$d2/$imagen.fits"
        cp "$d1/$imagen.fits" "$d2/$imagen.fits"
        echo "Enlace creado para $imagen"
    else
        echo "El archivo $imagen no se encuentra en $d1"
    fi
done < "$csv_file"

P=/mnt/uxmal_groups/image_storage/om_cal_oct_2024/om_cal_2016
a="$P/wcs"
b="$P/science"
c="$P/not_solved"

# Find files in b that are not in a and copy them to c
for file in "$b"/*; do
    filename=$(basename "$file")
    if [[ ! -e "$a/$filename" ]]; then
        cp "$file" "$c/"
        echo "Copied $filename to $c"
    fi
done

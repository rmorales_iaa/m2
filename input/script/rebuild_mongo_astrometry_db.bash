#!/bin/bash
#-----------------------------------------------
#rebuild the database where the solved images are: ROOT_DIR
ROOT_DIR=/mnt/uxmal_groups/image_storage/om_cal/
#-----------------------------------------------
#set -x	  #enable debug
#-----------------------------------------------
for YEAR in {2000..2050}
do
  dir=$ROOT_DIR/om_cal_$YEAR/wcs/
  echo "Parsing directory: '$dir'"
  if [ -d $dir ]
   then
     java -jar m2.jar --astrometry-catalog $dir 
  fi
done
#-----------------------------------------------
java -jar m2.jar --astrometry-catalog /mnt/uxmal_groups/image_storage/om_cal/external/ --recursive
#-----------------------------------------------

#------------------------------------------
#------------------------------------------
NAME=didymos
VR=0.458
#------------------------------------------
IN=/fast_disk/tmp/didimos/didymos_base/
OUT=/fast_disk/tmp/didimos/didymos_FITTED
#------------------------------------------
rm -fr $OUT
cp -L -r $IN $OUT
set -x  #deug on

START_TIME=$(date +'%s')

java -jar m2.jar --fit-wcs $OUT --precise-wcs-sip 

java -jar m2.jar --photometry-absolute $NAME --dir $OUT --debug-storage --astrometry --drop
java -jar m2.jar --orbit-residual $NAME --debug-storage

echo "Elapsed time: $(($(date +'%s') - $START_TIME))s"
#------------------------------------------

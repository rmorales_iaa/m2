#!/bin/bash

set -x
#-------------------------------------------------
FILE_1="/home/rafa/Downloads/light_curve_new.csv"
FILE_2="/home/rafa/Downloads/light_curve_web.csv"
COLUMN_1="image_name"
COLUMN_2="estimated_magnitude"
#---------------------------------------------
FILE_1_ONE_COL="file1_columnX.txt"
FILE_2_ONE_COL="file2_columnX.txt"
#--------------------------------------------

# Function to extract the specified columns from a CSV file
extract_column() {
  file=$1
  column_1=$2
  column_2=$3
  output_file=$4

  awk -v column_val_1="$column_1" -v column_val_2="$column_2" '{
    if (NR==1) {
        val_1=-1; val_2=-1;
        for(i=1; i<=NF; i++) {
            if ($i == column_val_1) {
                val_1=i;
            }
            if ($i == column_val_2) {
                val_2=i;
            }
        }
    }
    if(val_1 != -1 && val_2 != -1) {
        print $val_1, $val_2;
    }
}' "$file" | sort > "$output_file"
}

# Extract specified columns from both files
extract_column "$FILE_1" "$COLUMN_1" "$COLUMN_2" "$FILE_1_ONE_COL"
extract_column "$FILE_2" "$COLUMN_1" "$COLUMN_2" "$FILE_2_ONE_COL"

# Find the differences between the two columns
diff "$FILE_1_ONE_COL" "$FILE_2_ONE_COL"


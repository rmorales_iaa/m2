#!/bin/bash

# Definir los directorios
d1="/home/rafa/Downloads/deleteme/2023_cal/science/"
d2="/home/rafa/Downloads/deleteme/2023_cal/wcs/"
d3="/home/rafa/Downloads/deleteme/2023_cal/wcs_new"

# Crear el directorio d3 si no existe
mkdir -p "$d3"

# Comparar archivos y copiar los que no están en d2 a d3
for archivo in "$d1"/*; do
    nombre_archivo=$(basename "$archivo")
    if [ ! -e "$d2/$nombre_archivo" ]; then
        cp "$archivo" "$d3/"
    fi
done


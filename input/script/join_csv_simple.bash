#!/bin/bash

directory="/home/rafa/proyecto/m2/output/results/mpo_blind_search/0_source_detection/image_content/"
output_file="/home/rafa/Downloads/joined_file.csv"


# Remove the output file if it already exists
if [ -f "$output_file" ]; then
    rm "$output_file"
fi


# Get the list of CSV files in the directory
csv_files=$(find "$directory" -name "*.csv")

# Flag to indicate if the header has been written
header_written=false

# Iterate over each CSV file and append its content to the output file
for file in $csv_files; do
    # Skip the header if it has already been written
    if [ "$header_written" = false ]; then
        head -n 1 "$file" >> "$output_file"
        header_written=true
    fi

    # Append the content of the CSV file (excluding the header) to the output file
    tail -n +2 "$file" >> "$output_file"
done

echo "CSV files combined into $output_file"
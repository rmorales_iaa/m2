#!/bin/bash

input_dir="/home/rafa/Downloads/deleteme/small_set_4/"
output_dir="/home/rafa/Downloads/deleteme/stacked/"
output_gif="${output_dir}/animated.gif"

rm -fr "$output_dir"
mkdir -p "$output_dir"

# Convert FITS files to BMP
for fits_file in "$input_dir"/*.fits; do
    base_name=$(basename "$fits_file" .fits)
    output_file="${output_dir}/${base_name}.bmp"
    echo "converting from FITS to bmp: $base_name"
    convert "$fits_file" "$output_file"
done

# Create an animated GIF from BMP files
echo "creating gif file '$output_gif'"
convert -delay 20 -loop 0 "${output_dir}/*.bmp" "$output_gif"

#endo of script

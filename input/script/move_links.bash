#!/bin/bash

# Source directory
source_directory="/home/rafa/proyecto/m2/output/deleteme_remain/"

# Destination directory
destination_directory="/home/rafa/proyecto/m2/output/deleteme/"

MAX=25000

rm -fr $destination_directory
mkdir $destination_directory

# Count of copied files
copied_files=0

# Iterate over files in the source directory
for file in "$source_directory"/*; do
    
    cp -RP "$file" "$destination_directory"
    rm "$file"
    
    # Increment the copied files count
    ((copied_files++))

    
    if [ "$copied_files" -eq $MAX ]; then
        break
    fi
done

echo "Copied $copied_files files to $destination_directory"


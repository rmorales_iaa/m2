#!/bin/bash

# Navigate to the directory containing the files
cd $1

# Loop through all files in the directory
for file in *; do
    # Check if the file name contains spaces
    if [[ "$file" == *" "* ]]; then
        # Remove spaces and rename the file
        newfile=$(echo "$file" |  tr ' ' '_')
        mv "$file" "$newfile"
        echo "Renamed: $file -> $newfile"
    fi
done


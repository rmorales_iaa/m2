# Loop from YEAR 2002 to 2024
for YEAR in {2016..2020}; do
    P="/mnt/uxmal_groups/image_storage/om_cal_oct_2024/om_cal_$YEAR"
    a="$P/wcs"
    b="$P/science"
    #c="$P/not_solved"
    c="/home/rafa/Downloads/deleteme/om/$YEAR"

    # Ensure target directory exists
    rm -fr $c
    mkdir "$c"

    # Find files in b that are not in a (searching recursively) and copy them to c
    for file in "$b"/*; do
        filename=$(basename "$file")
        
        # Check if the file exists anywhere in directory 'a' recursively
        if ! find "$a" -type f -name "$filename" -print -quit | grep -q .; then
            cp "$file" "$c/"
            echo "Copied $filename to $c for YEAR=$YEAR"
        fi
    done

     # Check if directory 'c' is empty after copying files
    if [ -z "$(ls -A "$c")" ]; then
        echo "Directory $c is empty for YEAR=$YEAR. No files were copied."
        # Optionally, you can remove the empty directory:
        rmdir "$c"
    fi

done



rm -fr  astrometry.net
git clone  https://github.com/dstndstn/astrometry.net.git
cd astrometry.net
./config
make  -j$(nproc)
make  -j$(nproc) py
make  -j$(nproc) extra
sudo make install
cd ..
solve-field --version

sudo sed -i '/^#inparallel/c\inparallel'  /usr/local/astrometry/bin/../etc/astrometry.cfg
sudo sed -i '/^add_path/c\add_path /mnt/uxmal_groups/common_data/astrometry.net/indexes/astrometry.net/index-5200/'  /usr/local/astrometry/bin/../etc/astrometry.cfg


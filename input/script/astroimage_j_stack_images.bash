#!/bin/bash

#Built-in Macro Functions
#https://imagej.net/ij/developer/macro/functions.html

# Check if a directory was provided as an argument
if [ -z "$1" ]; then
  echo "Please provide a directory containing the FITS images."
  exit 1
fi

# Directory that contains the images
DIRECTORY="$1"

# Check if the directory exists
if [ ! -d "$DIRECTORY" ]; then
  echo "The provided directory does not exist."
  exit 1
fi

# Path to the AstroImageJ executable
AIJ_PATH="/home/rafa/atajo/astroimagej"

# Create a temporary file for the macro script
MACRO_FILE=$(mktemp)

# Generate the macro script for ImageJ
cat <<EOT > $MACRO_FILE

dir = "$DIRECTORY";
list = getFileList(dir);
openFiles = newArray();
for (i=0; i<list.length; i++) {
  if (endsWith(list[i], ".fits")) {
    openFiles = Array.concat(openFiles, dir + "/" + list[i]);
  }
}
Array.sort(openFiles);

setBatchMode("hide");
for (i=0; i<openFiles.length; i++) {
  open(openFiles[i]);
}

run("Images to Stack", "name=Stacked images");
setBatchMode("show");

run("Animation Options...", "speed=3 first=1 last=3 loop");
run("Start Animation");


EOT

# Execute the macro script with AstroImageJ
$AIJ_PATH -macro $MACRO_FILE

# Delete the temporary macro file
rm $MACRO_FILE

# Inform the user that stacking is complete
echo "All images in the directory $DIRECTORY have been opened and stacked in AstroImageJ."

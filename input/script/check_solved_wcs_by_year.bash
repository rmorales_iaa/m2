#!/bin/bash
#set -x
#--------------------------------------------
#user input
YEAR=$1
#--------------------------------------------
INPUT_DIR=/mnt/uxmal_groups/image_storage/om_cal_oct_2024/om_cal_$YEAR/science
OUTPUT_DIR=/mnt/uxmal_groups/image_storage/om_cal_oct_2024/om_cal_$YEAR/wcs
FILE_EXTENSION=.f*
#--------------------------------------------
startTime=$(date +'%s')
#--------------------------------------------
INPUT_COUNT="$(find -L $INPUT_DIR -maxdepth 1  -name *"$FILE_EXTENSION" -printf x | wc -c)"
OUTPUT_COUNT="$(find -L $OUTPUT_DIR -maxdepth 1  -name *"$FILE_EXTENSION" -printf x | wc -c)"
echo "---------->Input directory file count" : $INPUT_COUNT
echo "---------->Output directory file count": $OUTPUT_COUNT

if [ $INPUT_COUNT != $OUTPUT_COUNT ]
then
  FILE_1=sorted_input
  FILE_2=sorted_output
  DIFF_DIR=$OUTPUT_DIR/not_solved
  DIFF_FILE=$DIFF_DIR/image_list


  if [ -d $DIFF_DIR ]; then
    rm -r $DIFF_DIR
  fi

  mkdir $DIFF_DIR

  echo "WCS can not solved on:" $(( $INPUT_COUNT - $OUTPUT_COUNT )) " images. Please review the directory: '"$DIFF_DIR"'"
  find -L $INPUT_DIR  -maxdepth 1 -name *"$FILE_EXTENSION" -type f -printf "%f\n" | sort > $FILE_1
  find -L $OUTPUT_DIR -maxdepth 1 -name *"$FILE_EXTENSION" -type f -printf "%f\n" | sort > $FILE_2
  comm -23 $FILE_1 $FILE_2 > $DIFF_FILE
  rm $FILE_1
  rm $FILE_2

  #copy the not solved images
  echo "Copying not solved files"
  while IFS='' read -r imageName || [[ -n "${imageName}" ]]; do
    cp $INPUT_DIR/$imageName $DIFF_DIR
  done < "$DIFF_FILE"

  if [ -z "$(find $DIFF_DIR -mindepth 1)" ]; then
    rm -fr $DIFF_DIR
  fi

fi
#--------------------------------------------
echo "---------->Elapsed time: $(($(date +'%s') - $startTime))s"
#--------------------------------------------
#end of file

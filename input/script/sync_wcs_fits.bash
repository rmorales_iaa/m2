#!/bin/bash

#--------------------------------------------------------------------
SRC_DIR=$1
DEST_DIR_BASE="/mnt/uxmal_groups/image_storage/om_cal/om_cal_"
#--------------------------------------------------------------------

find "$SRC_DIR" -type f | sort | while IFS= read -r file_a; do

  file_name=$(basename "$file_a")   

  if [[ "$file_name" =~ .*science_([0-9]{4})_.* ]]; then
    year="${BASH_REMATCH[1]}"

    DEST_DIR=$DEST_DIR_BASE$year/wcs/

    file_b=$(find "$DEST_DIR" -type f -name "$file_name" | head -n 1)
    
    if [ -n "$file_b" ]; then
      echo "Updating $file_a"
      echo "      -> $file_b"
      cp "$file_a" "$file_b"
    fi  
  fi      
done
echo "End of processing"

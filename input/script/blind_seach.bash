#!/bin/bash

for YEAR in {2022..2023}
do
  dropcaches_system_ctl
  cd /home/rafa/proyecto/m2/deploy/
  java -Xss4m  -jar m2.jar --photometry-absolute blind  --dir /mnt/uxmal_groups/image_storage/om_cal/om_cal_$YEAR/wcs/ --blind
  java -Xss4m  -jar m2.jar  --photometry-absolute blind --report --blind
  cd /home/rafa/proyecto/m2/input/script
  ./join_csv_simple.bash 
  mv /home/rafa/Downloads/joined_file.csv /home/rafa/Downloads/$YEAR.csv
done

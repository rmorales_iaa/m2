CREATE TABLE IF NOT EXISTS images (
    fileName         VARCHAR(1024) NOT NULL,
    wcsFit           VARCHAR(256),
    raMin            DOUBLE PRECISION,
    raMax            DOUBLE PRECISION,
    decMin           DOUBLE PRECISION,
    decMax           DOUBLE PRECISION,
    fovX             REAL,
    fovY             REAL,
    pixScaleX        REAL,
    pixScaleY        REAL,
    PRIMARY KEY (fileName)
);

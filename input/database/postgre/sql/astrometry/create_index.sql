DROP INDEX IF EXISTS idx_images_radec;
CREATE INDEX idx_images_radec ON images (raMin, raMax, decMin, decMax);

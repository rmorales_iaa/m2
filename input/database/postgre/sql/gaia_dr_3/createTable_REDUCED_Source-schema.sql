DROP TABLE IF EXISTS gaia_source_reduced CASCADE;
CREATE TABLE gaia_source_reduced (
    source_id                        BIGINT NOT NULL,

    parallax_over_error              REAL,

    astrometric_n_obs_al             SMALLINT,
    astrometric_chi2_al              REAL,

    phot_g_mean_flux                 DOUBLE PRECISION,
    phot_g_mean_flux_error           REAL,
    phot_g_mean_flux_over_error      REAL,

    phot_bp_mean_flux                DOUBLE PRECISION,
    phot_bp_mean_flux_error          REAL,
    phot_bp_mean_flux_over_error     REAL,

    phot_rp_mean_flux                DOUBLE PRECISION,
    phot_rp_mean_flux_error          REAL,
    phot_rp_mean_flux_over_error     REAL,

    phot_bp_n_contaminated_transits  SMALLINT,
    phot_bp_n_blended_transits       SMALLINT,

    phot_rp_n_contaminated_transits  SMALLINT,
    phot_rp_n_blended_transits       SMALLINT,

    phot_proc_mode                   SMALLINT,

    bp_rp                            REAL,
    bp_g                             REAL,
    g_rp                             REAL,

    radial_velocity                  REAL,
    radial_velocity_error            REAL,

    PRIMARY KEY (source_id),
    FOREIGN KEY (source_id) REFERENCES gaia_source_minimal(source_id)
);

DROP TABLE IF EXISTS gaia_source_minimal CASCADE;
CREATE TABLE gaia_source_minimal (
    source_id                        BIGINT NOT NULL,

    ra                               DOUBLE PRECISION NOT NULL,
    ra_error                         REAL,

    dec                              DOUBLE PRECISION NOT NULL,
    dec_error                        REAL,

    parallax                         DOUBLE PRECISION,
    parallax_error                   REAL,

    phot_g_mean_mag                  REAL,
    phot_bp_mean_mag                 REAL,
    phot_rp_mean_mag                 REAL,

    phot_bp_rp_excess_factor         REAL,

    ruwe                             REAL,

    pmra                             DOUBLE PRECISION,
    pmra_error                       REAL,

    pmdec                            DOUBLE PRECISION,
    pmdec_error                      REAL,

    PRIMARY KEY (source_id)
)

DROP TABLE IF EXISTS gaia_source CASCADE;
CREATE TABLE gaia_source (
    solution_id                      BIGINT NOT NULL,
    designation                      TEXT NOT NULL,
    source_id                        BIGINT PRIMARY KEY
    random_index                     BIGINT,
    ref_epoch                        DOUBLE PRECISION,

    ra                               DOUBLE PRECISION,
    ra_error                         REAL,
    dec                              DOUBLE PRECISION,
    dec_error                        REAL,

    parallax                         DOUBLE PRECISION,
    parallax_error                   REAL,
    parallax_over_error              REAL,

    pm                               REAL,
    pmra                             DOUBLE PRECISION,
    pmra_error                       REAL,
    pmdec                            DOUBLE PRECISION,
    pmdec_error                      REAL,

    ra_dec_corr                      REAL,

    ra_parallax_corr                 REAL,
    ra_pmra_corr                     REAL,
    ra_pmdec_corr                    REAL,

    dec_parallax_corr                REAL,
    dec_pmra_corr                    REAL,
    dec_pmdec_corr                   REAL,

    parallax_pmra_corr               REAL,
    parallax_pmdec_corr              REAL,
    pmra_pmdec_corr                  REAL,

    astrometric_n_obs_al             SMALLINT,
    astrometric_n_obs_ac             SMALLINT,
    astrometric_n_good_obs_al        SMALLINT,
    astrometric_n_bad_obs_al         SMALLINT,
    astrometric_gof_al               REAL,
    astrometric_chi2_al              REAL,
    astrometric_excess_noise         REAL,
    astrometric_excess_noise_sig     REAL,
    astrometric_params_solved        SMALLINT,
    astrometric_primary_flag         BOOLEAN,

    nu_eff_used_in_astrometry        REAL,

    pseudocolour                     REAL,
    pseudocolour_error               REAL,
    ra_pseudocolour_corr             REAL,
    dec_pseudocolour_corr            REAL,
    parallax_pseudocolour_corr       REAL,
    pmra_pseudocolour_corr           REAL,
    pmdec_pseudocolour_corr          REAL,

    astrometric_matched_transits     SMALLINT,
    visibility_periods_used          SMALLINT,
    astrometric_sigma5d_max          REAL,
    matched_transits                 SMALLINT,
    new_matched_transits             SMALLINT,
    matched_transits_removed         SMALLINT,

    ipd_gof_harmonic_amplitude       REAL,
    ipd_gof_harmonic_phase           REAL,
    ipd_frac_muti_peak               SMALLINT,
    ipd_frac_odd_win                 SMALLINT,

    ruwe                             REAL,

    scan_direction_strength_k1       REAL,
    scan_direction_strength_k2       REAL,
    scan_direction_strength_k3       REAL,
    scan_direction_strength_k4       REAL,
    scan_direction_mean_k1           REAL,
    scan_direction_mean_k2           REAL,
    scan_direction_mean_k3           REAL,
    scan_direction_mean_k4           REAL,

    duplicated_source                BOOLEAN,

    phot_g_n_obs                     SMALLINT,
    phot_g_mean_flux                 DOUBLE PRECISION,
    phot_g_mean_flux_error           REAL,
    phot_g_mean_flux_over_error      REAL,
    phot_g_mean_mag                  REAL,

    phot_bp_n_obs                    SMALLINT,
    phot_bp_mean_flux                DOUBLE PRECISION,
    phot_bp_mean_flux_error          REAL,
    phot_bp_mean_flux_over_error     REAL,
    phot_bp_mean_mag                 REAL,

    phot_rp_n_obs                    SMALLINT,
    phot_rp_mean_flux                DOUBLE PRECISION,
    phot_rp_mean_flux_error          REAL,
    phot_rp_mean_flux_over_error     REAL,
    phot_rp_mean_mag                 REAL,

    phot_bp_rp_excess_factor         REAL,
    phot_bp_n_contaminated_transits  SMALLINT,
    phot_bp_n_blended_transits       SMALLINT,

    phot_rp_n_contaminated_transits  SMALLINT,
    phot_rp_n_blended_transits       SMALLINT,

    phot_proc_mode                   SMALLINT,

    bp_rp                            REAL,
    bp_g                             REAL,
    g_rp                             REAL,

    radial_velocity                  REAL,
    radial_velocity_error            REAL,
    rv_method_used                   SMALLINT,
    rv_nb_transits                   SMALLINT,
    rv_nb_deblended_transits         SMALLINT,
    rv_visibility_periods_used       SMALLINT,
    rv_expected_sig_to_noise         REAL,
    rv_renormalised_gof              REAL,
    rv_chisq_pvalue                  REAL,
    rv_time_duration                 REAL,
    rv_amplitude_robust              REAL,
    rv_template_teff                 REAL,
    rv_template_logg                 REAL,
    rv_template_fe_h                 REAL,
    rv_atm_param_origin              SMALLINT,

    vbroad                           REAL,
    vbroad_error                     REAL,
    vbroad_nb_transits               SMALLINT,

    grvs_mag                         REAL,
    grvs_mag_error                   REAL,
    grvs_mag_nb_transits             SMALLINT,

    rvs_spec_sig_to_noise            REAL,

    phot_variable_flag               TEXT,

    l                                DOUBLE PRECISION,
    b                                DOUBLE PRECISION,

    ecl_lon                          DOUBLE PRECISION,
    ecl_lat                          DOUBLE PRECISION,

    in_qso_candidates                BOOLEAN,
    in_galaxy_candidates             BOOLEAN,

    non_single_star                  SMALLINT,

    has_xp_continuous                BOOLEAN,
    has_xp_sampled                   BOOLEAN,
    has_rvs                          BOOLEAN,
    has_epoch_photometry             BOOLEAN,
    has_epoch_rv                     BOOLEAN,
    has_mcmc_gspphot                 BOOLEAN,
    has_mcmc_mc                      BOOLEAN,
    in_andromeda_survey              BOOLEAN,

    classprob_dsc_combmod_quasar     REAL,
    classprob_dsc_combmod_galaxy     REAL,
    classprob_dsc_combmod_star       REAL,

    teff_gspphot                     REAL,
    teff_gspphot_lower               REAL,
    teff_gspphot_upper               REAL,

    logg_gspphot                     REAL,
    logg_gspphot_lower               REAL,
    logg_gspphot_upper               REAL,

    mh_gspphot                       REAL,
    mh_gspphot_lower                 REAL,
    mh_gspphot_upper                 REAL,

    distance_gspphot                 REAL,
    distance_gspphot_lower           REAL,
    distance_gspphot_upper           REAL,

    azero_gspphot                    REAL,
    azero_gspphot_lower              REAL,
    azero_gspphot_upper              REAL,

    ag_gspphot                       REAL,
    ag_gspphot_lower                 REAL,
    ag_gspphot_upper                 REAL,

    ebpminrp_gspphot                 REAL,
    ebpminrp_gspphot_lower           REAL,
    ebpminrp_gspphot_upper           REAL,

    libname_gspphot                  TEXT
)
#!/bin/bash
#----------------------------------------------------
set -x # Debugging enabled
#----------------------------------------------------
INPUT_DIR=$1
#----------------------------------------------------
TMP_DIR=/home/rafa/Downloads/deleteme/gnuastro_tmp
OUTPUT_CSV_DIR=$TMP_DIR/CSV
#----------------------------------------------------
if [ ! -d "$TMP_DIR" ]; then
    # Create the directory
    mkdir -p "$TMP_DIR"
    mkdir -p "$OUTPUT_CSV_DIR"
    echo "Directory created: $TMP_DIR"
else
    echo "Directory already exists: $TMP_DIR"
fi

#----------------------------------------------------
for INPUT_IMAGE in "$INPUT_DIR"/*.fits; do
    #----------------------------------------------------
    INPUT_IMAGE_NAME=$(basename -- "$INPUT_IMAGE")
    INPUT_IMAGE_EXT="${INPUT_IMAGE_NAME##*.}"
    #----------------------------------------------------
    echo "------------------- Processing file  $INPUT_IMAGE_NAME ------------------- "
    #----------------------------------------------------
    OUTPUT_IMAGE="$TMP_DIR/${INPUT_IMAGE_NAME%.*}_out.$INPUT_IMAGE_EXT"
    SEG_IMAGE="$TMP_DIR/${INPUT_IMAGE_NAME%.*}_seg.$INPUT_IMAGE_EXT"
    CAT_CSV="$OUTPUT_CSV_DIR/${INPUT_IMAGE_NAME%.*}_cat"
    #----------------------------------------------------

    astnoisechisel "$INPUT_IMAGE" --output="$OUTPUT_IMAGE" --hdu=0 
    astsegment "$OUTPUT_IMAGE" --output="$SEG_IMAGE" 
    astmkcatalog "$SEG_IMAGE" --ids --ra --dec --x --y --output="$CAT_CSV"

    #rm "$OUTPUT_IMAGE"
    #rm "$SEG_IMAGE"

done
#----------------------------------------------------

# end of file

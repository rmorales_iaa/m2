#!/bin/bash
#----------------------------------------------------
#set -x # Debugging enabled
#----------------------------------------------------
INPUT_IMAGE=$1
TMP_DIR=$2
#----------------------------------------------------
  INPUT_IMAGE_NAME=$(basename -- "$INPUT_IMAGE")
  INPUT_IMAGE_EXT="${INPUT_IMAGE_NAME##*.}"
  #--------------------------------------------------
  DETECT_IMAGE="$TMP_DIR/${INPUT_IMAGE_NAME%.*}_det.$INPUT_IMAGE_EXT"
  SEG_IMAGE="$TMP_DIR/${INPUT_IMAGE_NAME%.*}_seg.$INPUT_IMAGE_EXT"
  FINAL_IMAGE="$TMP_DIR/${INPUT_IMAGE_NAME%.*}.$INPUT_IMAGE_EXT"
  #--------------------------------------------------

  OUTLIER_NUM=15
  while [ "$OUTLIER_NUM" -gt 3 ]
  do
    astnoisechisel "$INPUT_IMAGE" --output="$DETECT_IMAGE" --hdu=0 --outliernumngb="$OUTLIER_NUM"

    if [ -f "$DETECT_IMAGE" ]; then
      astsegment "$DETECT_IMAGE" --output="$SEG_IMAGE"
      astfits --copy=2 --primaryimghdu  --output=$FINAL_IMAGE  $SEG_IMAGE
      OUTLIER_NUM=0
    else
      ((OUTLIER_NUM--))
    fi

  done

  rm $DETECT_IMAGE
  rm $SEG_IMAGE
#----------------------------------------------------

# end of file

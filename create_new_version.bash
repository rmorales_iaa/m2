#!/bin/bash
#------------------------------------
set -x #activate debug
#------------------------------------
#update version info
VERSION=v2.7.0
MESSAGE="First version"
#------------------------------------
APP=m2

#DEPLOY_DIR must be sync with github repo
DEPLOY_DIR=deploy/git_compiled_versions/$APP/
#------------------------------------
#sync the new version and the version of the build.sbt
echo "sync the new version:'$VERSION' in build.sbt"

LINE=$(grep "VERSION=" create_new_version.bash)
NEW_VERSION=${LINE#*=v}
sed -i "s/^lazy val programVersion=.*/lazy val programVersion=\"$NEW_VERSION\"/" build.sbt
#------------------------------------
echo "compiling m2"
git tag -a $VERSION -m '$MESSAGE'
git push origin $VERSION

./myGit $MESSAGE
./makeFatJar

#------------------------------------
#update m2 configuration
echo "update m2 configuration"

COMMON_OPTS="--verbose --archive  --progress  --recursive  --ignore-existing"
DEST_DIR="deploy/git_compiled_versions/m2/"

#------------------------------
echo "copying lib"
SOURCE_DIR="lib"
rsync $COMMON_OPTS  $SOURCE_DIR $DEST_DIR
#------------------------------

#------------------------------
echo "copying native"
SOURCE_DIR="native"
rsync $COMMON_OPTS  $SOURCE_DIR $DEST_DIR
#------------------------------

#------------------------------
echo "copying input"
SOURCE_DIR="input"
EXCLUDE_DIR_SEQ="--exclude input/spice --exclude input/occultation/objects"
rsync $COMMON_OPTS $EXCLUDE_DIR_SEQ $SOURCE_DIR $DEST_DIR 
#------------------------------
#update remote repo with compiled jar and new configuration

cd $DEPLOY_DIR
git tag -a $VERSION -m '$MESSAGE'
git push origin $VERSION
./myGit $MESSAGE
cd ..
cd ..
cd ..
#------------------------------

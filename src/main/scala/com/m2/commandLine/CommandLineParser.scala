/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  03/dic/2018
 * Time:  01h:38m
 * Description: https://github.com/scallop/scallop
 */
//=============================================================================
package com.m2.commandLine
//=============================================================================
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.time.Time
import com.m2.Version
import org.rogach.scallop._
//=============================================================================
import com.common.util.file.MyFile._
import com.common.util.path.Path._
//=============================================================================
//=============================================================================
object CommandLineParser {
  //---------------------------------------------------------------------------
  final val COMMAND_QUERY                      = "query"
  final val COMMAND_ASTROMETRY_CATALOG         = "astrometry-catalog"
  final val COMMAND_FIT_WCS                    = "fit-wcs"
  final val COMMAND_PHOTOMETRY_ABSOUTE         = "photometry-absolute"
  final val COMMAND_LIGHT_CURVE                = "light-curve"
  final val COMMAND_PHOTOMETRY_DIFFERENTIAL    = "photometry-differential"
  final val COMMAND_FIND_COMMON_FOV            = "findCommonFOV"
  final val COMMAND_MERGE_DIFF_PHOT            = "merge-differential-photometry"
  final val COMMAND_CONVERT_TO_INT_DATA_TYPE   = "convert-to-int-data-type"
  final val COMMAND_DB_LIST                    = "db-list"
  final val COMMAND_EXPLORE_ROT_PERIOD         = "explore-rot-period"
  final val COMMAND_ALIGN                      = "align"
  final val COMMAND_OCCULTATION                = "occultation"
  final val COMMAND_ADD_METADATA               = "add-metadata"
  final val COMMAND_RENAME                     = "rename"
  final val COMMAND_SPICE_SPK_DATABASE         = "spice-spk-db"
  final val COMMAND_VERSION                    = "version"
  final val COMMAND_ORBIT_RESIDUAL             = "orbit-residual"
  final val COMMAND_WEB                        = "web"
  final val COMMAND_UPDATE_WEB_RESULTS         = "updateWebResults"
  final val COMMAND_FUMO                       = "fumo"
  final val COMMAND_MPO_LOCATION               = "mpo-location"
  final val COMMAND_ORBIT_INTEGRATION          = "orbit-integration"
  //---------------------------------------------------------------------------
  final val VALID_COMMAND_SEQ = Seq(
      COMMAND_QUERY
    , COMMAND_ASTROMETRY_CATALOG
    , COMMAND_PHOTOMETRY_ABSOUTE
    , COMMAND_FIT_WCS
    , COMMAND_LIGHT_CURVE
    , COMMAND_PHOTOMETRY_DIFFERENTIAL
    , COMMAND_FIND_COMMON_FOV
    , COMMAND_MERGE_DIFF_PHOT
    , COMMAND_CONVERT_TO_INT_DATA_TYPE
    , COMMAND_DB_LIST
    , COMMAND_EXPLORE_ROT_PERIOD
    , COMMAND_ALIGN
    , COMMAND_OCCULTATION
    , COMMAND_ADD_METADATA
    , COMMAND_RENAME
    , COMMAND_SPICE_SPK_DATABASE
    , COMMAND_VERSION
    , COMMAND_ORBIT_RESIDUAL
    , COMMAND_WEB
    , COMMAND_UPDATE_WEB_RESULTS
    , COMMAND_FUMO
    , COMMAND_MPO_LOCATION
    , COMMAND_ORBIT_INTEGRATION
  )
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.m2.commandLine.CommandLineParser._
class CommandLineParser(args: Array[String]) extends ScallopConf(args) with MyLogger {
  version(Version.value + "\n")
  banner(s"""m2 syntax
            |m2 [configurationFile][command][parameters]
            |[commnads and parameters]
            |\t[$COMMAND_VERSION]
            |\t[$COMMAND_QUERY]
            |\t[$COMMAND_ASTROMETRY_CATALOG]
            |\t[$COMMAND_PHOTOMETRY_ABSOUTE]
            |\t[$COMMAND_FIT_WCS]
            |\t[$COMMAND_LIGHT_CURVE]|
            |\t[$COMMAND_PHOTOMETRY_DIFFERENTIAL]
            |\t[$COMMAND_FIND_COMMON_FOV]
            |\t[$COMMAND_MERGE_DIFF_PHOT]
            |\t[$COMMAND_CONVERT_TO_INT_DATA_TYPE]
            |\t[$COMMAND_DB_LIST]
            |\t[$COMMAND_EXPLORE_ROT_PERIOD]
            |\t[$COMMAND_ALIGN]
            |\t[$COMMAND_OCCULTATION]
            |\t[$COMMAND_ADD_METADATA]
            |\t[$COMMAND_RENAME]
            |\t[$COMMAND_SPICE_SPK_DATABASE]
            |\t[$COMMAND_ORBIT_RESIDUAL]
            |\t[$COMMAND_WEB]
            |\t[$COMMAND_UPDATE_WEB_RESULTS]
            |\t[$COMMAND_FUMO]
            |\t[$COMMAND_ORBIT_INTEGRATION]
            |\t[$COMMAND_VERSION]
            | #---------------------------------------------------------------------------------------------------------
            |Example 0: Show version
            |  java -jar m2.jar --$COMMAND_VERSION
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 1: Find and get the images contained in the mongo database "astrometry" that includes the object 'n'
            | . Where 'n' can be the final or temporal designation. It is assumed that 'n' is an mpo (minor planet object). In other case, see 1.3
            | The spice spk of the mpo must be present in the directory 'input/spice/kernels/spk/objects/'.
            | It also produces an script to automatize the calls to calculate absolute and differential photometry,
            |  including the light curve generation and the merge of all observing nights.
            |  It is saved in the same directory of the program calling.
            |  The V-R used for generating the script is "0.432". If none V-R is provided, then the compiled V-R database is
            |  consulted, returning the very first VR for the object.
            |
            | java -jar m2.jar --query 'n' --vr 0.432
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 1.1: Same as 1, but link the images, do not copy them
            |
            |  java -jar m2.jar --query 'n'  --vr 0.432 --link
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 1.2: Same as 1, but only generate the script, no images are managed
            |
            |  java -jar m2.jar --query 'n'  --vr 0.432 --no-images
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 1.3: Same as 1.1, but using d as image dir (none the default one)
            |
            |  java -jar m2.jar --query 'n'  --vr 0.432 --no-images --dir d
            |
            | #-------------------------------------------
            | Example 1.4: Same as example 1 but only manage images starting in '2022-04-14' (inclusive)
            | and ending in '2022-05-14' (inclusive) having  equal or more than '400' exposure time in seconds having
            | taken in image filter 'Johnson_B' at telescope 'CAHA_1_23' with instrument 'i'
            |
            |  java -jar m2.jar --query n  --vr 0.432 --start-date 2022-04-14 --end-date 2022-05-14 --exposure-time 400 --image-filter Johnson_B --telescope 'CAHA_1_23' --instrument i
            |
            |  #--------------------------------------------------------------------------------------------------------
            | Example 1.6: Same as example 1.3 but considering the object 'n' as a star expressing the right ascension in
            |  hours, minutes, seconds, fractions of seconds and declination in degrees, minutes, seconds, fraction of seconds.
            |  The separator for each item will be infered
            |  It is possible to define (by default is zero) the proper motion (pm) in right ascension and declination in mas/year
            |  and the reference epoch in years used to obtaing the position (by default 2000.0)
            |  This type of query avoids a call to vizier catalog
            |
            |  java -jar m2.jar --query n  --vr 0.432 --star  --ra  "05 54 45.73" --dec "+10 55 57.0" --pmra 12.21 --pmdec -53.3 --reference-epoch 2000.0
            |
            |#-------------------------------------------
            | Example 1.7: Same as example 1 but parsing the csv file 'csv'. The format of the file is the following:
            | #syntax: type;name[;vr]
            | #where:
            | # 'type' is one of: {mpo,star} . In other words: a minor planet object or a star
            | # 'name' is the name of the object to be processed
            | # 'vr' it is assumed to be 0.5 if it is omitted
            | #---------------------------------------------
            | #mpo example with vr provided
            | #mpo;chiron;0.37
            | #---------------------------------------------
            | #mpo example without vr
            | #mpo;chiron
            | #---------------------------------------------
            | #star example
            | #star;j23064-050
            | #---------------------------------------------
            |
            |  java -jar m2.jar --query csv
            | #---------------------------------------------------------------------------------------------------------
            | Example 2: Parse all images contained in the directory 'dir' and store the om tags and the min max (rad,dec)
            | into mongo database "astrometry" plus extra data of the image.
            | If a image was previously parsed its entry in the database is updated.
            | Do not searching files in sub-directories.
            |
            |  java -jar m2.jar --astrometry-catalog dir
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 2.1: Same as 2 but searching files in sub-directories
            |
            |  java -jar m2.jar --astrometry-catalog dir --recursive
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 3: Perform a iterative fit of the wcs for all images stored in the directory 'in'
            |  The fit is calculated using the detected sources and GAIA catalog with a least squares optimization.
            |
            |  java -jar m2.jar --fit-wcs in
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 3.1: Same as 3 but avoiding the recalculation of wcs fitting if is present the flag 'M2WCSFIT' in the
            | FITS header. This flag indicates that it has been previously fitted
            |
            |  java -jar m2.jar --fit-wcs in  --check-flag
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 3.2: Same as 3 but ignoring the images with non precise wcs SIP
            |
            |  java -jar m2.jar --fit-wcs in  --precise-wcs-sip
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 3.3: Same as 3.2 but ignoring FITs files that the its elapsed time
            | (from its processing time to current date) is greater than the value supplie '7'
            |
            |  java -jar m2.jar --fit-wcs in --precise-wcs-sip --only-elapsed-days-grater-than 7
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 4: Parse all images contained in the directory 'in' , containing the object 'n' and store into the
            | database information about the images and the sources found.
            | By default, only GAIA sources and the object 'n' are considered.
            | It is assumed that 'n' is an mpo (minor planet object), in other case see 5.7
            | The information of the sources include a normalized flux (counts/s) obtaining with photometry aperture.
            | By default, the apertures used will be calculated using the fwhm of the image (fwhm average of all sources)
            | and the configuration file.
            | By default, new images of the object 'n' will be added to the previous results. Whether an image was
            | previously processed, it is skipped.
            | Also calculates the absolute photometry polynomial fit using all supported catalogs: 'GAIA DR 3' and 'Apass DR 10'
            |
            |  java -jar m2.jar --photometry-absolute n  --dir in
            | #---------------------------------------------------------------------------------------------------------
            | Example 4.1: Same as example 4 but assuming no object (blind search). In that case, all knwon mpos
            | known by local copy of 'astrochecker' are detected. Plese, note that this command required a previous
            | 'sub-gaia' command with parameters 'no_object' and '--blind' over the same directory (see Example 3)
            |
            |  java -jar m2.jar --photometry-absolute no_object --dir in --blind
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 4.2: Same as example 4 but storing all found sources
            |
            |  java -jar m2.jar  --photometry-absolute n  --dir in --apply-on-all-sources
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 4.3: Same as example 4 but using fixes apertures: min 1.8 annular 12 and max 17
            |
            |  java -jar m2.jar  --photometry-absolute n  --dir in --fixed-aperture 1.8 12 17
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 4.4: Same as example 4 but generating additional results (gaia matches, polynomial fitting):
            | object croppies, png and csv with the sources detected and discarde
            |
            |  java -jar m2.jar  --photometry-absolute n  --additional-results
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 4.5: Same as example 4 but removing any information previously stored in the database
            |
            |  java -jar m2.jar  --photometry-absolute n  --drop
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 4.6: Same as 4, but assuming the the object is a star
            | This type of query implies a call to vizier catalog
            |
            |  java -jar m2.jar --photometry-absolute n  --dir in --star
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 4.7: Same as 4, but assuming the the object is a star and specific position: 'ra','dec','pmra','pmdec' and 'reference' 'epoch'
            | This type of query avoids a call to vizier catalog
            |
            |  java -jar m2.jar --photometry-absolute n  --dir in --star  --ra  11.566377575149 --dec 85.2262465569127 --pmra -2.095 --pmdec -0.961 --reference-epoch 2016.0
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 4.8: Same as 4, but with no focus (blind mode)
            |
            |  java -jar m2.jar --photometry-absolute n  /home/rafa/proyecto/m2/output/deleteme/in --blind
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 4.9: Generated a report for all images containing object 'n' (theoretically)
            | and the all photometric valid sources cointained in each one: image data, sources per image and photometric models
            |
            |  java -jar m2.jar --photometry-absolute n  --report
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 4.10: Same as 5.8  but 'n' was used previosly in 'sub-gaia' blind mode (see 3.3)
            |
            |  java -jar m2.jar --photometry-absolute n  --report --blind
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 4.11: Same as 5.8 but object is a star
            |
            |  java -jar m2.jar --photometry-absolute n  --report --star
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 4.12: Same as 5.8 but generating only the photometric models
            |
            |  java -jar m2.jar --photometry-absolute n  --report --photometric-models
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 4.13: Same as 5 but storing the results a debugging database collection with same name as the object
            | 'n' but with suffix '_debug'
            |
            |  java -jar m2.jar --photometry-absolute n --dir in --debug-storage
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 4.14: Same as 5 but storing the results a debugging database collection with same name as the object
            | 'n' but with suffix '_debug'. Use for precise astrometry setup: only the focus source is considered,
            |  isolation filters are ignored, no fwhm
            |
            |  java -jar m2.jar --photometry-absolute n --dir in --debug-storage --astrometry
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 5: Estimate the magnitude of the object 'n' using the supported photometric catalogs
            | taking V-R  as reference value in the plots. If none V-R is provided, then the compiled V-R database is
            |  consulted, returning the very first VR for the object.
            | Also it is calculated the rotation period it is assumed that 'n' is an mpo, in other case see 6.2
            |
            |  java -jar m2.jar --light-curve n --vr vr
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 5.1: Same as 5 but considering the object 'n' as a star
            |
            |  java -jar m2.jar --light-curve n  --star
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 5.2: Same as 5, but calculating the rotational period of the object
            |
            |  java -jar m2.jar --light-curve n --vr vr --rot-period
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 5.3: Same as 5, but using and storing the results in a debugging database collection with same
            | name as the object 'n' but with suffix '_debug'
            |
            |  java -jar m2.jar --light-curve n --debug-storage
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 6: Find all possible group (solutions) of images with a common field of view (FOV) and filter
            | Each image contains the object 'n'.  It is assumed that 'n' is an mpo (minor planet object), in other case see 7.1
            | The absolute photometry of mpo 'n' with the option --apply-on-all-sources (see example 5.3) must be
            |  previously executed and also the calculated the light curve
            | A valid solution (group) have a minimum FOV intersection of 50%, 10 common sources and it is composed by
            | at least 10 images.
            |
            | java -jar m2.jar --find-common-fov n  --fov-percentage 50  --min-common-source 10 --min-images-per-solution 10
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 6.1: Same as 6 but considering the object 'n' as a star
            |
            | java -jar m2.jar --find-common-fov n  --fov-percentage 50  --min-common-source 10 --min-images-per-solution 10 --star
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 6.2: Same as 6 but using and storing the results in a debugging database collection with same
            | name as the object 'n' but with suffix '_debug'
            |
            | java -jar m2.jar --find-common-fov n  --fov-percentage 50  --min-common-source 10 --min-images-per-solution 10 --debug-storage
            | #---------------------------------------------------------------------------------------------------------
            | Example 7: Calculate the differential photometry of the object with name 'n' and V-R 'v_r'.
            | It is assumed that 'n' is an mpo (minor planet object), in other case see 8.1
            | The absolute photometry with parameter '--apply-on-all-sources' must be previously calculated (see example 5).
            | The searching of the best frequency using the Lomb-Scargle algorithm is calculated using the min and max
            | periods in hours('1','12') and a sample per peak of '64'.
            | The number of peaks in the curve is '2'
            | The final common sources (after applying all filters) must equal or greater than 10.
            | The theoretical curve is fitted using a Fourier series fit of degree '2'.
            |
            |  java -jar m2.jar --photometry-differential n --vr V-R --min-common-source 10 --min-period-hours 1 --max-period-hours 12 --frequency-samples-per-peak 64 --peak-count 2 --fourier-series-fit-degree 2
            | #---------------------------------------------------------------------------------------------------------
            | Example 7.1: Same as 7 but considering the object 'n' as a star
            |
            |  java -jar m2.jar --photometry-differential n --vr V-R --min-common-source 10 --min-period-hours 1 --max-period-hours 12 --frequency-samples-per-peak 64 --peak-count 2 --fourier-series-fit-degree 2 --star
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 7.2: Same as 7 but using and storing the results in a debugging database collection with same
            | name as the object 'n' but with suffix '_debug'
            |
            |  java -jar m2.jar --photometry-differential n --vr V-R --min-common-source 10 --min-period-hours 1 --max-period-hours 12 --frequency-samples-per-peak 64 --peak-count 2 --fourier-series-fit-degree 2 --debug-storage
            | #---------------------------------------------------------------------------------------------------------
            | Example 8: Merge all the partials differential photometry of object 'n' calculated for all FOV and filter combinations
            | It is assumed that 'n' is an mpo (minor planet object), in other case see 9.1
            | that were previously calculated with command 'photometry-differential' (see example 6)
            | The merge is being calculates using a differential photometry step of '0.01' magnitudes.
            | The parameters are the same as command 'photometry-differential' except 'min-common-source' and 'V-R' that has no sense in this command.
            |
            |  java -jar m2.jar --merge-differential-photometry n --min-period-hours 1 --max-period-hours 12 --frequency-samples-per-peak 64 --peak-count 2 --fourier-series-fit-degree 2 --step 0.01
            | #---------------------------------------------------------------------------------------------------------
            | Example 8.1: Same as 8 but considering the object 'n' as a star
            |
            |  java -jar m2.jar --merge-differential-photometry n --min-period-hours 1 --max-period-hours 12 --frequency-samples-per-peak 64 --peak-count 2 --fourier-series-fit-degree 2 --step 0.01 --star
            |  #---------------------------------------------------------------------------------------------------------
            | Example 8.2: Same as 8 but using and storing the results in a debugging database collection with same
            | name as the object 'n' but with suffix '_debug'
            |
            |  java -jar m2.jar --merge-differential-photometry n --min-period-hours 1 --max-period-hours 12 --frequency-samples-per-peak 64 --peak-count 2 --fourier-series-fit-degree 2 --step 0.01 --debug-storage
            |  #---------------------------------------------------------------------------------------------------------
            | Example 9: Explore and generate rotation period plots of a csv file for object 'n'. The csv file must
            | be created previosly when the rotation period is created.
            | The searching of the best frequency using the Lomb-Scargle algorithm is calculated using the min and max
            | periods in hours('1','12') and a sample per peak of '32'.
            | It is assumed '2' peaks in the curve.
            | The theoretical curve is calculated using a Fourier series fit of degree '2'
            | Will be generated the plots only the best '10' frequencies in the directory 'output/explore/n/'
            | It is assumed that 'n' is an mpo (minor planet object), in other case see 10.1
            | The rotation period hours id fixed to 'r'
            |
            |  java -jar m2.jar --explore-rot-period n --csv n --force-rot-period-hours r --min-period-hours 1 --max-period-hours 12 --frequency-samples-per-peak 32 --peak-count 2 --fourier-series-fit-degree 2  --best 10
            | #---------------------------------------------------------------------------------------------------------
            | Example 9.1: Same as 9 but force the rotation period to 'r' hours and ansumming that 'n' is a star
            |
            |  java -jar m2.jar --explore-rot-period n --csv csv --force-rot-period-hours r --star
            | #---------------------------------------------------------------------------------------------------------
            | Example 9.2: Same as 9 but filtering by telescopes:' t1' and 't2', filter 'f1' and time range: from: '2011-03-31' to '2011-04-05'
            |
            |  java -jar m2.jar --explore-rot-period n --csv csv --min-period-hours 1 --max-period-hours 12 --frequency-samples-per-peak 32 --peak-count 2 --fourier-series-fit-degree 2  --best 1 --telescope-seq t1 t2 --filter-seq f1 --time-range-seq 2011-03-31 2011-04-05
            | #---------------------------------------------------------------------------------------------------------
            | Example 10: Align with celestial coordinates all FITS images of a directory 'dir' and stores the only the images
            | that require alignment in the directory 'out'. The images must be previosly resolved with astrometry.
            | The WCS of the aligned file is not modified, so the aligned file has a wrong WCS and must be re-solved (astrometrically).
            | Only the position of the pixels are changed, not its values.
            |
            |  java -jar m2.jar --align dir --output-dir out
            | #---------------------------------------------------------------------------------------------------------
            | Example 10.1: Same as 10 but applying to all the images a specific action.
            | Valid actions: {flip_v, flip_h, rot+90, rot-90, rot+180, rot-180, rot+270, rot-270}
            |
            |   java -jar m2.jar --align dir --output-dir out --action flip_vertical
            | #---------------------------------------------------------------------------------------------------------
            | Example 11: Calculate the occulation of the object 'n'  in a set of images stored in directory 'dir' using
            | the default occultation configuration file.
            | It is assumed that images are astrometrically solved (at least the very first one), see coomand 'add-meta-data'
            | to add the astrometry information. If it is not possible to solve them see 12.3
            |
            |  java -jar m2.jar --occultation n --dir dir
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 11.1: Same as 11 but using as configuration file for the occultation the file 'c'
            |
            |  java -jar m2.jar  --occultation n --dir dir --occ-conf c
            |  #---------------------------------------------------------------------------------------------------------
            | Example 12: Add meta data in the form of FITS header entries for an object 'n' using the FITS images stored
            |  in directory 'i', storing the new files in directory 'o.
            |  If 'n' is equal to 'NONE' then no SPK file is loaded
            |  It is recommended to use this command with occultation images
            |  It is assumed that 'n' is an mpo (minor planet object), in other case see 13.2
            |  In the key or value contains a space, replace it with the charater '_'. Example: 'LCO Cerrro Tololo a' becomes 'lco_cerro_tololo_a'
            |  Automatically, some FITS entries are added:
            |    - If the object is in the database the astrometrical FITS entries: CRPIX1,CRPIX2,CRVAL1 and CRVAL
            |    - OBJECT with the value 'n'
            |  Explicitly, are added the FITS entries: k1,v1 and k2,v2
            |  If FITS entries are present, the are overwritten
            |
            |  java -jar m2.jar --add-meta-data n --dir i --output-dir o --key-value-seq k1 v1 k2 v2
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 12.1: Same as 12 but not modifying any FITS entries if they exist
            |
            |  java -jar m2.jar --add-meta-data n --dir i --output-dir o --key-value-seq k1 v1 k2 v2 --keep
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 12.2: Same as 12 but assuming that 'n' is a star
            |
            |  java -jar m2.jar --add-meta-data n --dir i --output-dir o --key-value-seq k1 v1 k2 v2 --star
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 12.2: Same as 12 but removing any FITS entries with key: 'k1', 'k2' or 'k3'
            |
            |  java -jar m2.jar --add-meta-data n --dir i --output-dir o --key-value-seq k1 k2 k3 --remove
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 12.3: Same as 12 but trying to get the object name from FITS records. If the object name exists,
            | but there is no a SPICE SPK stored in the database, the image is copied in "o" without modification and
            | the name is reported in the output file 'f'
            |
            |  java -jar m2.jar --add-meta-data GUESS --dir i --output-dir o --output-file f
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 13: Rename all FITS files stored in directory 'in' using a crecent number with 4 digits starting with '0000.fits'.
            | The files are sorted using the observing time, from newer (very first one) to oldest (the last one).
            | The files are stored in directory 'o'
            |
            |  java -jar m2.jar --rename in --output-dir o
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 14: Update the database of SPICE SPK files using all files od dir 'dir'. A valid SPK file must start
            | with name and extension ".bsp". A valid name start with SPK ID number followed optionally by divider '_' plus extra information
            |
            |  java -jar m2.jar --spice-spk-db dir --update
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 14.1: Generate all SPICE SPK file versions of mpo 'id' stored in the database and store then in 'dir'.
            |  The 'id' can be a SPK id, mpc ID or a designation (main or alternative). When the 'id' is a name and has spaces,
            |  the surround with ""
            |
            |  java -jar m2.jar --spice-spk-db id --generate --output-dir dir
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 14.2: Same as 15.1 but generating only the last 'n' more recents
            |
            |  java -jar m2.jar --spice-spk-db id --generate --output-dir dir --last-more-recent 'n'
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 14.3: Same as 15.1 but generating only the one with version 'v'
            |
            |  java -jar m2.jar --spice-spk-db id --generate --output-dir --ver 'v'
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 14.4: Report the information of all SPICE SPK  file versions of mpo 'id' stored in the database .
            |  The 'id' can be a SPK id, mpc ID or a designation (main or alternative).
            |
            |  java -jar m2.jar --spice-spk-db id --report
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 14.5: Same as 15.4 but using the SPK sequence ID: 'id1' and 'id2'
            |
            |  java -jar m2.jar --spice-spk-db "id1,id2" --report
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 14.5: Remove all SPKs and its versions with ID: 'id1' and 'id2'. It also remove the SPK in the local
            | storage at: "output/spk/"
            |
            |  java -jar m2.jar --spice-spk-db "id1,id2" --drop
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 15: Calculate the residual of object 'n' regarding JPL's orbit. A previous absilute photometry
            |  calculation must be run. See 5.12
            |
            |   java -jar m2.jar --orbit-residual n
            |
            | #---------------------------------------------------------------------------------------------------------
            | Example 16: Transform all images of directory 'dir' from its original data type to the integer data type 't' and save
            | them in 'odir'. The type 't' must be one of the interger types declarred in FITS format
            |
            |  java -jar m2.jar --convert-to-int-data-type 'dir' --output-dir 'oDir' --int-data-type t
            | #---------------------------------------------------------------------------------------------------------
            | Example 17: List the name of all databases in stored MongoDB
            |
            |  java -jar m2.jar --db-list
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 18: Check if the object 'o' exist the SPICE SPK database
            |
            |  java -jar m2.jar --web obj --exists
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 18.1: Generate the information for the object 'obj' required by m2 web page command: 'astrometry'
            | and store result in directory 'o'
            |
            |  java -jar m2.jar --web obj --astrometry --output-dir o
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 18.2: Generate the information of V-R present in the literature for the object 'obj' required by m2 web page command: 'vr'
            | and store result in directory 'o'
            |
            |  java -jar m2.jar --web obj --vr-query --output-dir o
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 19: Update the web results using a images directory 'dir'
            |
            |  java -jar m2.jar --update-web-results dir
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 20: Find unknown moving objects (fumo) in a sequence of images stored at 'dir' storing the results
            | at 'out' considering only the images that have at least '30' minutes between them. The minor planet object
            | 'mpo' must be ignored if it is present in the images
            |
            |  java -jar m2.jar --fumo dir --output-dir out --min-elapsed-minutes-between-images 30 --mpo mpo
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 20.1: Same as 20 but only calculating step 1:
            |  Registration, solve astrometry, normalize, subtract median and stack
            |
            |  java -jar m2.jar --fumo dir --output-dir out --min-elapsed-minutes-between-images 30  --mpo mpo --step 1
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 20.2: Same as 14 but only calculating step 2:
            |  Source detection
            |
            |  java -jar m2.jar --fumo dir --output-dir out --min-elapsed-minutes-between-images 30  --mpo mpo--step 2
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 20.3: Same as 14 but only calculating step 3:
            |  Trajectory calculation
            |
            |  java -jar m2.jar --fumo dir --output-dir out --min-elapsed-minutes-between-images 30  --mpo mpo --step 3
            |
            |#---------------------------------------------------------------------------------------------------------
            | Example 20.4: Same as 14 but doing all 3 steps, no mpo filterig
            | It the calculations does not exists, then the behavior is like 14
            |
            |  java -jar m2.jar --fumo dir --output-dir out  --min-elapsed-minutes-between-images 30
            |#---------------------------------------------------------------------------------------------------------
            | Example 21: Find the location, it it exists, of mpo 'mpo' at image 'img'. If parameter 'mpo' is missing,
            | then the astrocheker will be used to find any mpo located at the image
            |
            |  java -jar m2.jar --mpo-location img --mpo mpo
            |
            |  #---------------------------------------------------------------------------------------------------------
            | Example 22: Integrate the orbit of all MPOs of MPC mongo database 'mpo_catalog.mpc' for the date
            | '2002-01-01' using up to '15' days. The result is a tsv file that will stored at directory 'd'.
            | The format od the date must be "YYYY-MM-DD"
            |
            |  java -jar m2.jar --orbit-integration 2002-01-01 --step-day-size 15 --dir d
            |
            | #---------------------------------------------------------------------------------------------------------
            | #---------------------------------------------------------------------------------------------------------
            |Options:
            |""".stripMargin)
  footer("\nFor detailed information, please consult the documentation!")

  val configurationFile = opt[String](default = Some("input/configuration/main.conf")
    ,  short = 'c'
    ,  descr = "m2 configuration file\n")

  val version = opt[Boolean](required = false
    , noshort = true
    , descr = "Program version\n"
  )

  val fitWcs = opt[String](noshort = true
    , short = 'f'
    , descr = "Fit the WCS of all images of a directory" + "\n")

  val photometryAbsolute = opt[String](noshort = true
    , short = 'h'
    , descr = "Calculate the absolute photometry on the input directory" + "\n")

  val photometryDifferential = opt[String](noshort = true
    , short = 'h'
    , descr = "Calculat the differential photometry on the input directory" + "\n")

  val lightCurve = opt[String](noshort = true
    , short = 'l'
    , descr = "Calculate the light curve of an object" + "\n")

  val dbList = opt[Boolean](noshort = true
    , short = 'b'
    , descr = " List the name of all databases in stored MongoDB" + "\n")

  val vr = opt[Double](required = false
    , short = 'v'
    , default = Some(0.5d)
    , descr = "Photometric V-R value\n")

  val astrometryCatalog = opt[String](required = false
    , noshort = true
    , descr = "Add to the database the astrometry om tags and the min max (ra,dec) of all images of a directory\n")

  val query = opt[String](required = false
    , noshort = true
    , descr = "Find all images that contains an mpo\n")

  val findUnknownMovingObject = opt[String](required = false
    , noshort = true
    , descr = "Find all unknown moving objects in a sequence of images\n")

  val findCommonFov = opt[String](required = false
    , noshort = true
    , descr = "Find all possible common field of view (Fov) of all images of one mpo\n")

  val outputDir = opt[String](required = false
    , noshort = true
    , descr = "Output directory\n")

  val outputFile = opt[String](required = false
    , noshort = true
    , descr = "Output file\n")

  val remoteHost = opt[String](required = false
    , noshort = true
    , descr = "Remote host name\n")

  val convertToIntDataType = opt[String](required = false
    , noshort = true
    , descr = "convert the data type of an image to a integer data type\n")

  val intDataType = opt[Int](required = false
    , noshort = true
    , descr = "bit per pixel data type according to FITS format [8,16,32,64]\n")

  val fixedAperture = opt[List[Double]](required = false
    , default = Some(List[Double]())
    , noshort = true
    , descr = "fixed apertures (min,anular,max) used in photometry")

  val minPeriodHours = opt[Double](required = false
    , default = Some(1)
    , noshort = true
    , descr = "min period in hours in single peak to be used for searching best frequency in Lomb-Scargle algorithm")

  val maxPeriodHours = opt[Double](required = false
    , default = Some(12)
    , noshort = true
    , descr = "max period in hours in single peak to be used for searching best frequency in Lomb-Scargle algorithm")

  val frequencySamplesPerPeak = opt[Int](required = false
    , default = Some(64)
    , noshort = true
    , descr = "the approximate number of desired Examples across the typical peak for searching best frequency in Lomb-Scargle algorithm")

  val peakCount = opt[Int](required = false
    , default = Some(1)
    , noshort = true
    , descr = "Number of peaks in the cuerve when plotting\n")

  val fourierSeriesFitDegree = opt[Int](required = false
    , default = Some(2)
    , noshort = true
    , descr = "Degree of the Fourier series formula use to fit a set of values\n")

  val startDate = opt[String](required = false
    , noshort = true
    , descr = "Starting date (inclusive)\n")

  val endDate = opt[String](required = false
    , noshort = true
    , descr = "End date (inclusive)\n")

  val fovPercentage = opt[Double](required = false
    , noshort = true
    , descr = "min field of view intersection percentage\n")

  val minImagesPerSolution = opt[Int](required = false
    , noshort = true
    , descr = "min number of images of a solution\n")

  val instrument = opt[String](required = false
    , noshort = true
    , descr = "instrument filter name\n")

  val imageFilter = opt[String](required = false
    , noshort = true
    , descr = "image filter name\n")

  val exposureTime = opt[Double](required = false
    , noshort = true
    , descr = "exposure time\n")

  val dir = opt[String](required = false
    , noshort = true
    , descr = "directory name\n")

  val applyOnAllSources = opt[Boolean](required = false
    , noshort = true
    , descr = "apply photometry absolute in all sources\n")

  val minCommonSource = opt[Int](required = false
    , noshort = true
    , descr = "min number of common sources in a solution\n")

  val mergeDifferentialPhotometry = opt[String](required = false
    , noshort = true
    , descr = "Merge a sequence of differential photometry calcualted in diffetent FOV and filter\n")

  val step = opt[Double](required = false
    , default = Some(0.01d)
    , noshort = true
    , descr = "differential photometry step magnitudes using in merge seasons\n")

  val forceRotPeriodHours = opt[Double](required = false
    , noshort = true
    , descr = "Force to a specific rotation period in hours\n")

  val rootPath = opt[String](required = false
    , noshort = true
    , descr = "root path to be used\n")

  val best = opt[Int](required = false
    , noshort = true
    , descr = "Number of best rotation periods\n")

  val csv = opt[String](required = false
    , noshort = true
    , descr = "Name of a csv file\n")

  val outputCsv = opt[String](required = false
    , noshort = true
    , descr = "Name of a output csv file\n")

  val exploreRotPeriod = opt[String](required = false
    , noshort = true
    , descr = "Explore a csv generating plots with best rotational periods\n")

  val telescopeSeq = opt[List[String]](required = false
    , noshort = true
    , descr = "Array of telescopes \n")

  val filterSeq = opt[List[String]](required = false
    , noshort = true
    , descr = "Array of filters \n")

  val timeRangeSeq = opt[List[String]](required = false
    , noshort = true
    , descr = "Array of time ranges. Format: (startDate,endDate) where xDate is 'yyyy-MM-ddTHH:mm:ss.SSS' or 'yyyy-MM-dd'\n")

  val additionalResults = opt[Boolean](required = false
    , noshort = true
    , descr = "Create additional results for the active command (see related documentation in the command)\n")

  val drop = opt[Boolean](required = false
    , noshort = true
    , descr = "Remove current content in the database\n")

  val report = opt[Boolean](required = false
    , noshort = true
    , descr = "Generate a report using precalculated values that are stores in the database. Or report the information of a mpo stored in the SPICE SPK database\n")

  val star = opt[Boolean](required = false
    , noshort = true
    , descr = "Indicates that the object is a star and not an mpo\n")

  val blind = opt[Boolean](required = false
    , noshort = true
    , descr = "No image focus is provided\n")

  val rotPeriod = opt[Boolean](required = false
    , noshort = true
    , descr = "calculate the rotational period\n")

  val checkFlag = opt[Boolean](
      required = false
    , noshort = true
    , default = Some(true)
    , descr = "avoid the recalculation of wcs fitting if is pressent the FITS flag 'M2WCSFIT' that indicates that it has been previously calculated\n")

  val noImages = opt[Boolean](required = false
    , noshort = true
    , descr = "only generate the script, no images are managed\n")

  val photometricModels = opt[Boolean](required = false
    , noshort = true
    , descr = "only generate all photometric models\n")

  val align = opt[String](required = false
    , noshort = true
    , descr = "align all images of a directory with celestial coordinates without modyfing the wcs\n")

  val occultation = opt[String](required = false
    , noshort = true
    , descr = "find the occultation sources in set og images\n")

  val action = opt[String](required = false
    , noshort = true
    , descr = "action to be done to align all images of a directroy\n")

  val telescope = opt[String](required = false
    , noshort = true
    , descr = "a telescope name\n")

  val addMetaData = opt[String](required = false
    , noshort = true
    , descr = "add FITS entries to an image\n")

  val keyValueSeq = opt[List[String]](required = false
    , noshort = true
    , descr = "list of key-values used as FITS entries\n")

  val keep = opt[Boolean](required = false
    , noshort = true
    , descr = "keep current values, do not overwritten\n")

  val remove = opt[Boolean](required = false
    , noshort = true
    , descr = "remove a FITS entry\n")

  val debugStorage = opt[Boolean](required = false
    , noshort = true
    , descr = "store the results of the object in a debugging database collection. Same name as the object but with suffix '_debug'\n")

  val occConf = opt[String](required = false
    , noshort = true
    , descr = "configuration file used in the occultation command\n")

  val rename = opt[String](required = false
    , noshort = true
    , descr = "rename all FITS files of a directory\n")

  val spiceSpkDb = opt[String](required = false
    , noshort = true
    , descr = "report the information of a mpo stored in the SPICE SPK database\n")

  val generate = opt[Boolean](required = false
    , noshort = true
    , descr = "generate a SPK file contained in the SPICE SPK database\n")

  val lastMoreRecent = opt[Int](required = false
    , noshort = true
    , descr = "generate only the 'n' more recent SPK files contained in the SPICE SPK database\n")

  val update = opt[Boolean](required = false
    , noshort = true
    , descr = "update the SPICE SPK database\n")

  val ver = opt[String](required = false
    , noshort = true
    , descr = "SPICE SPK version\n")

  val astrometry = opt[Boolean](
      required = false
    , noshort = true
    , descr = "using astrometry setup when calculating photometry\n")

  val orbitResidual = opt[String](
      required = false
    , noshort = true
    , descr = "Calculate the residual of object regarding JPL's orbit.\n")

  val web = opt[String](
    required = false
    , noshort = true
    , descr = "Generate the output for m2 web\n")

  val updateWebResults = opt[String](
    required = false
    , noshort = true
    , descr = "Update the web results using a images directory\n")

  val exists = opt[Boolean](
    required = false
    , noshort = true
    , descr = "check if a object exists\n")

  val ra = opt[String](
    required = false
    , noshort = true
    , descr = "Right ascension\n")

  val dec = opt[String](
    required = false
    , noshort = true
    , descr = "Declination\n")

  val pmra = opt[Double](
    required = false
    , noshort = true
    , descr = "proper motion in right ascension\n")

  val pmdec = opt[Double](
    required = false
    , noshort = true
    , descr = "proper motion in declination\n")

  val referenceEpoch = opt[Double](
    required = false
    , noshort = true
    , descr = "reference epoch of the measure\n")

  val preciseWcsSip = opt[Boolean](
    required = false
    , noshort = true
    , default = Some(false)
    , descr = "ignore images with no precise WCS SIP\n")

  val vrQuery = opt[Boolean](required = false
    , default = Some(true)
    , descr = "Photometric V-R value present in the literature\n")

  val onlyElapsedDaysGraterThan = opt[Int](
      required = false
    , descr = "Process only the FITs files that the its elapsed time (from its processing time to current date) is greater than the value supplied \n")

  val link = opt[Boolean](
    required = false
    , noshort = true
    , default = Some(false)
    , descr = "link the images, do not copy them\n")

  val fumo = opt[String](
    required = false
    , noshort = true
    , descr = "find unknown moving object\n")

  val minElapsedMinutesBetweenImages = opt[Int](
    required = false
    , noshort = true
    , default = Some(5)
    , descr = "find unknown moving object\n")

  val mpo = opt[String](
    required = false
    , noshort = true
    , default = None
    , descr = "minor planet objet: main or alternative designation, mpc ID or SPK ID\n")

  val mpoLocation = opt[String](
    required = false
    , noshort = true
    , descr = "find a mpo in a image\n")

  val orbitIntegration = opt[String](
    required = false
    , noshort = true
    , descr = "integrate the orbit of all MPOs of MPC database\n")

  val stepDaySize = opt[Int](
    required = false
    , noshort = true
    , default = Some(15)
    , descr = "range of days to integrate\n")

  val recursive = opt[Boolean](
    required = false
    , noshort = true
    , default = Some(false)
    , descr = "find files in sub-directories\n")
  //---------------------------------------------------------------------------
  validate (configurationFile) { f =>
    if (!fileExist(f) && (!directoryExist(f))) Left(s"Invalid user argument 'configurationFile'. The configuration file '$f' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (dir) { p =>
    if (!directoryExist(p)) Left(s"Invalid user argument 'dir'. The path '$p' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (convertToIntDataType) { p =>
    if (!directoryExist(p)) Left(s"Invalid user argument 'convertToIntDataType'. The path '$p' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (intDataType) { p =>
    if ((p != 8) && (p != 16) && (p != 32) && (p != 64))  Left(s"Invalid user argument 'intDataType'. The user data type: $intDataType must be one of [8,16,32,64]")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (startDate) { s =>
    if (!Time.isValidLocalDate(s)) Left(s"Invalid user argument 'userStartDate'. The date '$s' is not a valid local date (e.g. 2022-05-14)")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate (endDate) { s =>
    if (!Time.isValidLocalDate(s)) Left(s"Invalid user argument 'userStartDate'. The date '$s' is not a valid local date (e.g. 2022-05-14)")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate(csv) { s =>
    if (!MyFile.fileExist(s)) Left(s"Invalid user argument for command 'explore-rot-period'. The csv file '$s' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  validate(occConf) { s =>
    if (!MyFile.fileExist(s)) Left(s"Invalid user argument for command 'occultation'. The configuration file '$s' does not exist")
    else Right()
  }
  //---------------------------------------------------------------------------
  val commandSeq = Seq(version
                      , fitWcs
                      , photometryAbsolute
                      , lightCurve
                      , photometryDifferential
                      , mergeDifferentialPhotometry
                      , dbList
                      , astrometryCatalog
                      , query
                      , convertToIntDataType
                      , findUnknownMovingObject
                      , findCommonFov
                      , exploreRotPeriod
                      , align
                      , occultation
                      , addMetaData
                      , rename
                      , spiceSpkDb
                      , orbitResidual
                      , web
                      , updateWebResults
                      , fumo
                      , mpoLocation
                      , orbitIntegration)
  mutuallyExclusive(commandSeq:_*)
  requireOne       (commandSeq:_*)
  verify()
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file CommandLineParser.scala
//=============================================================================

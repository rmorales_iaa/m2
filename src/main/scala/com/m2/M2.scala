/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  16/Feb/2020
 * Time:  00h:44m
 * Description: None
 */
//=============================================================================
package com.m2
//=============================================================================
import com.common.configuration.MyConf
import com.common.coordinate.conversion.Conversion
import com.common.database.mongoDB.database.carmencita.CarmencitaDB
import com.common.database.mongoDB.database.gaia.GaiaDB
import com.common.database.mongoDB.spiceSPK.SpiceSPK_DB
import com.common.database.postgreDB.astrometry.AstrometryDB
import com.common.database.vizier.Vizier
import com.common.fits.metaData.MetaData
import com.common.fits.wcs.fit._
import com.common.geometry.point.Point2D_Double
import com.common.hardware.cpu.CPU
import com.common.image.astrometry.Align
import com.common.image.astrometry.fov.CommonFov
import com.common.image.astrometry.orbit.OrbitStats
import com.common.image.astrometry.query.AstrometryQuery
import com.common.image.dataTypeConversion.ImageDataTypeConversion
import com.common.image.focusType._
import com.common.image.fumo.Fumo
import com.common.image.mpo.MpoLocation
import com.common.image.occultation.SourceOccultation
import com.common.image.photometry.absolute.PhotometryAbsolute
import com.common.image.photometry.absolute.lightCurve.LightCurve
import com.common.image.photometry.absolute.photometricModel.catalog.gaia.GaiaPhotometricModel
import com.common.image.photometry.differential.PhotometryDifferential
import com.common.image.photometry.differential.merge.Merge
import com.common.image.photometry.differential.rotationalPeriod.{ExploreRotationalPeriod, RotationalPeriod}
import com.common.image.util.UtilImage
import com.common.image.web.{QueryAstrometry, QueryVR, UpdateWebResults}
import com.common.jpl.Spice
import com.common.logger.MyLogger
import com.common.orbitIntegrator.OrbitIntegrator
import com.common.util.file.MyFile
import com.common.util.path.Path
import com.common.util.string.MyString
import com.common.util.time.Time
import com.common.util.time.Time._
import com.m2.M2.M2_OUTPUT_ROOT_PATH
import com.m2.commandLine.CommandLineParser
import com.m2.commandLine.CommandLineParser._
//=============================================================================
import ch.qos.logback.classic.{Level, LoggerContext}
import org.slf4j.LoggerFactory

import java.io.{BufferedWriter, File, FileWriter}
import java.time.{Instant, LocalDate, LocalDateTime}
import java.util.concurrent.ConcurrentLinkedQueue
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object M2 {
  //---------------------------------------------------------------------------
  private var verboseFlag = false
  //---------------------------------------------------------------------------
  val M2_OUTPUT_ROOT_PATH            = "output/results/"
  val M2_SOURCE_DETECTION_ROOTH_PATH = "0_source_detection"
  val M2_ASTROMETRY_ROOT_PATH        = "1_astrometry"
  val M2_PHOTOMETRY_ROOT_PATH        = "2_photometry"
  //---------------------------------------------------------------------------
  val COMMAND_INFO_LEFT_SIDE_SIZE    = 50
  //---------------------------------------------------------------------------
  private final val sep = "\t"
  private final val COMMAND_STATS_CSV_FILENAME = "output/command_stats.csv"
  private final val COMMAND_STATS_CSV_HEADER = Seq("object","command","timestamp","elapsed_time_hours","image_processed_data_GiB")
  //---------------------------------------------------------------------------
  var carmencitaDB: CarmencitaDB = null
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.m2.M2._
case class M2() extends App with MyLogger {
  //---------------------------------------------------------------------------
  private var lastObject = ""
  private var lastCommand = ""
  //---------------------------------------------------------------------------
  private def initialActions(): Unit = {

    // set time zone to UTC
    Instant.now.atZone(zoneID_UTC)

    //disable reactor logging up to level error
    LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext].getLogger("reactor.util").setLevel(Level.ERROR)

    //disable mongo logging up to level error
    LoggerFactory.getILoggerFactory().asInstanceOf[LoggerContext].getLogger("org.mongodb.driver").setLevel(Level.ERROR)

    //sextractor
    Path.ensureDirectoryExist("output/sextractor/")
    Path.ensureDirectoryExist(MyConf.c.getString("Astrometry.temporalDirectory"))

    //delete temporary directories
    Path.ensureDirectoryExist(MyConf.c.getString("AsteroidChecker.queryDir"))
    Path.ensureDirectoryExist("output/tmp/")

    //set verbose flag
    verboseFlag = MyConf.c.getInt("Verbosity.verbose") == 1
  }
  //---------------------------------------------------------------------------
  private def finalActions(startMs: Long): Unit = {

    //clean not used directories
    //problems when running in spark
    //Path.deleteDirectoryIfExist("output/sextractor/")
    //Path.deleteDirectoryIfExist("output/astrometry/")

    val path = MyConf.c.getString("Astrometry.temporalDirectory")
    Path.deleteDirectoryIfExist(path)
    Path.deleteDirectoryIfExist(MyConf.c.getString("AsteroidChecker.queryDir"))

    //write command stats
    val elapsedHours = (System.currentTimeMillis - startMs) / MILLISECONDS_IN_ONE_HOUR.toDouble
    if (lastCommand != null && !lastCommand.isEmpty) writeCommandStats(elapsedHours.toString)

    warning(s"Moose2 elapsed time: ${Time.getFormattedElapseTimeFromStart(startMs)}")

    //finalize pendient tasks and clear memory
    System.runFinalization()
    System.gc()
  }
  //---------------------------------------------------------------------------
  private def getRoothPathSeq(composedName: String) ={
    val objectDetectionRootPath         = s"$M2_OUTPUT_ROOT_PATH/$composedName/$M2_SOURCE_DETECTION_ROOTH_PATH/"
    val astrometryRootPath              = s"$M2_OUTPUT_ROOT_PATH/$composedName/$M2_ASTROMETRY_ROOT_PATH/"
    val photometryAbsoluteRootPath      = s"$M2_OUTPUT_ROOT_PATH$composedName/$M2_PHOTOMETRY_ROOT_PATH/${PhotometryAbsolute.ROOT_PATH}"
    val photometryDifferentialRootPath  = s"$M2_OUTPUT_ROOT_PATH$composedName/$M2_PHOTOMETRY_ROOT_PATH/${PhotometryDifferential.ROOT_PATH}"

    (objectDetectionRootPath, astrometryRootPath, photometryAbsoluteRootPath, photometryDifferentialRootPath)
  }
  //---------------------------------------------------------------------------
  private def writeCommandStats(elapsedTime: String) = {
    var bw: BufferedWriter = null
    if (!MyFile.fileExist(COMMAND_STATS_CSV_FILENAME)) {
      bw = new BufferedWriter(new FileWriter(new File(COMMAND_STATS_CSV_FILENAME)))
      bw.write(COMMAND_STATS_CSV_HEADER.mkString("\t") + "\n")
    }
    else bw = new BufferedWriter(new FileWriter(new File(COMMAND_STATS_CSV_FILENAME),true))
    bw.write(lastObject + sep +
      lastCommand + sep +
      LocalDateTime.now() + sep +
      elapsedTime + sep +
      "\n" )
    bw.close()
  }
  //---------------------------------------------------------------------------
  private def getVR(cl: CommandLineParser
                    , imageFocus: ImageFocusMPO) = {
    if (cl.vr.isSupplied) cl.vr()
    else  {
      val query = QueryVR(imageFocus.name, Some(imageFocus))
      val r = query.query(simpleMode = true)
      query.close()
      r
    }
  }
  //---------------------------------------------------------------------------
  //(raDec, raDecPM, referenceEpoch)
  private def getImageFocusStar(name: String
                                , suppliedPosition: Option[( Option[String] //ra
                                                         , Option[String] //dec
                                                         , Option[Double] //pmra
                                                         , Option[Double] //pmdec
                                                         , Option[Double])] = None) // refence epoch
    : Option[ImageFocusStar] = {

    if (suppliedPosition.isDefined && suppliedPosition.get._1.isDefined) {
      return Some(ImageFocusStar(
        name
        , Point2D_Double(Conversion.getRa(suppliedPosition.get._1.get).get, Conversion.getDec(suppliedPosition.get._2.get).get)
        , Point2D_Double(suppliedPosition.get._3.get, suppliedPosition.get._4.get)
        , suppliedPosition.get._5.get))
    }

    //carmencita has preference
    if (carmencitaDB == null) carmencitaDB = CarmencitaDB()
    val carmencitaPosition = carmencitaDB.findObjectPositionInfo(name)
    val position =
      if (!carmencitaPosition.isEmpty) carmencitaPosition.get
      else {
        warning(s"Star: '$name' did not found in Carmencita database")
        Vizier.getObjectPositionInfo(name).getOrElse {
          error(s"Can not get the position of star: '$name'")
          return None
        }
      }
    Some(ImageFocusStar(
        name
      , Point2D_Double(position.ra, position.dec)
      , Point2D_Double(position.pmra, position.pmDec)
      , position.epoch))
  }
  //---------------------------------------------------------------------------
  def getImageFocus(_objectName: String
                    , inputDir: String  = ""
                    , isStar: Boolean  = false
                    , blindSearch: Boolean = false
                    , suppliedPosition: Option[(Option[String] //ra
                                              , Option[String] //dec
                                              , Option[Double] //pmra
                                              , Option[Double] //pmdec
                                              , Option[Double])] = None
                    , verbose: Boolean = verboseFlag)
    : Option[ImageFocusTrait] = {

    val objectName = _objectName.trim
    if (blindSearch) {
      info(s"${MyString.rightPadding("\tblind mode (none imageFocus)", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inputDir'")
      Some(ImageFocusNone(objectName))
    }
    else {
      val normalizedName = objectName
        .trim
        .replaceAll(" +", " ")
        .replaceAll(" ", "_")
        .replaceAll("'", "")
        .toLowerCase()

      if (isStar) getImageFocusStar(normalizedName, suppliedPosition)
      else {
        val requestName =
          if (normalizedName.startsWith("mpo_")) normalizedName.split("_")(1)
          else normalizedName.replaceAll("_", " ")

        ImageFocusMPO.build(requestName, source = null, verbose)
      }
    }
  }
  //---------------------------------------------------------------------------
  private def commandVersion(): Boolean = {
    println(Version.value)
    true
  }
  //---------------------------------------------------------------------------
  //astrometry query
  def commadQuery(cl: CommandLineParser): Boolean = {
    //---------------------------------------------------------------------------
    warning(s"Command:'$COMMAND_QUERY'")
    lastCommand = COMMAND_QUERY
    lastObject  = cl.query()
    //---------------------------------------------------------------------------
    val objectName = cl.query()

    val startDate    = if (!cl.startDate.isSupplied) None else Some(LocalDate.parse(cl.startDate()))
    val endDate      = if (!cl.endDate.isSupplied) None else Some(LocalDate.parse(cl.endDate()))
    val telescope    = if (cl.telescope.isSupplied) Some(cl.telescope()) else None
    val filter       = if (cl.imageFilter.isSupplied) Some(cl.imageFilter()) else None
    val instrument   = if (cl.instrument.isSupplied) Some(cl.instrument()) else None
    val exposureTime = if (cl.exposureTime.isSupplied) Some(cl.exposureTime()) else None
    val remoteHost   = if (cl.remoteHost.isSupplied) Some(cl.remoteHost()) else None
    val noImages     = cl.noImages.isSupplied
    val linkImages   = if (cl.link.isSupplied) true else false
    val imageDir     = if (cl.dir.isSupplied) Some(cl.dir()) else None

    val imageFocusMetaDataSeq =
      if (MyFile.fileExist(objectName)) {
        info(s"${MyString.rightPadding("\tparsing csv file", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$objectName'")
        AstrometryQuery.parseCsv(objectName) map { item =>
          (getImageFocus(item.name.trim, "", item.isStar, blindSearch = false).getOrElse(return false)
          , item.vr
          , item.isStar)
        }
      }
     else {
       val isStar = cl.star.isSupplied
       val focus = getImageFocus(objectName, "", isStar, blindSearch = false)
       val vr = if (isStar || focus.isEmpty) cl.vr() else getVR(cl,focus.get.asInstanceOf[ImageFocusMPO])
       Seq((focus.getOrElse(return false)
           , vr
           , isStar))
     }

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    if (startDate.isDefined)    info(s"${MyString.rightPadding("\tstart date", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${startDate.get}'")
    if (endDate.isDefined)      info(s"${MyString.rightPadding("\tend date", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${endDate.get}'")
    if (telescope.isDefined)    info(s"${MyString.rightPadding("\ttelescope  ", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${telescope.get}")
    if (filter.isDefined)       info(s"${MyString.rightPadding("\tfilter", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${filter.get}")
    if (instrument.isDefined)   info(s"${MyString.rightPadding("\tinstrument", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${instrument.get}")
    if (exposureTime.isDefined) info(s"${MyString.rightPadding("\texposure time >=", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${exposureTime.get}")
    if (remoteHost.isDefined)   info(s"${MyString.rightPadding("\tremote host", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${remoteHost.get}'")
    if (noImages)               info(s"${MyString.rightPadding("\tonly script is created", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$noImages'")
    if (imageDir.isDefined)     info(s"${MyString.rightPadding("\tusing image dir", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${imageDir.get}'")
    if (linkImages)             info(s"${MyString.rightPadding("\tuse image link instead of copy", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${linkImages}'")

    imageFocusMetaDataSeq foreach { case (imageFocus,vr,isStar) =>

      info(s"Processing object: '${imageFocus.getID}'")
      imageFocus.print(COMMAND_INFO_LEFT_SIDE_SIZE)
      if (!isStar) info(s"${MyString.rightPadding("\tV-R", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$vr'")

      if (isStar) AstrometryQuery.queryStar(imageFocus
                                            , remoteHost
                                            , noImages
                                            , imageDir
                                            , linkImages)
      else {
        Spice.init()
        AstrometryQuery(
            Seq(imageFocus)
          , Seq(vr)
          , remoteHost
          , noImages
          , startDate
          , endDate
          , telescope
          , filter
          , instrument
          , exposureTime
          , imageDir
          , linkImages = linkImages)
        Spice.close()
      }
      AstrometryQuery.clearInfoStorage()
    }
    true
  }
  //---------------------------------------------------------------------------
  //astrometry catalog
  def commadAstrometryCatalog(cl: CommandLineParser): Boolean = {
    warning(s"Command:'$COMMAND_ASTROMETRY_CATALOG'")
    lastCommand = COMMAND_ASTROMETRY_CATALOG
    lastObject  = "none"

    val inputDir = cl.astrometryCatalog()
    val rootPath = if (cl.rootPath.isSupplied) Some(cl.rootPath()) else None
    val recursive = cl.recursive.isSupplied

    info(s"\tcore count                     : '${CPU.getCoreCount()}'")

    if (rootPath.isDefined)
    info(s"\tusing root path for images     : '${cl.rootPath()}'")
    info(s"\tFind images in sub-directories : '$recursive'")

    val db = AstrometryDB(inputDir, rootPath, recursive)
    db.process()
    db.close()

    true
  }
  //---------------------------------------------------------------------------
  def commadFitWcs(cl: CommandLineParser): Boolean = {
    warning(s"Command:'$COMMAND_FIT_WCS'")
    lastCommand = COMMAND_FIT_WCS

    val inputDir   = cl.fitWcs()
    val checkFlag  = cl.checkFlag.isSupplied
    val preciseWCS_SIP  = cl.preciseWcsSip.isSupplied
    val onlyElapsedDaysLessThan = if (cl.onlyElapsedDaysGraterThan.isSupplied) Some(cl.onlyElapsedDaysGraterThan()) else None

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    info(s"${MyString.rightPadding("\tinput dir", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inputDir'")
    info(s"${MyString.rightPadding("\tprecise WCS SIP", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$preciseWCS_SIP'")
    info(s"${MyString.rightPadding("\tcheck flag to avoid recalculation of wcs fit", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$checkFlag'")
    if (onlyElapsedDaysLessThan.isDefined)
      info(s"${MyString.rightPadding("\tProcessing only FITS files with elapsed time is greater than", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$onlyElapsedDaysLessThan' days")

    FitWcs.fitDirectory(
      inputDir
      , checkFlag
      , preciseWCS_SIP
      , onlyElapsedDaysLessThan
      , verbose = verboseFlag)

    true
  }
  //---------------------------------------------------------------------------
  //absolute photometry
  def commadPhotometryAbsolute(cl: CommandLineParser): Boolean = {
    //-------------------------------------------------------------------------
    warning(s"Command:'$COMMAND_PHOTOMETRY_ABSOUTE'")
    lastCommand = COMMAND_PHOTOMETRY_ABSOUTE
    lastObject = cl.photometryAbsolute()
    //-------------------------------------------------------------------------

    val objectName = cl.photometryAbsolute()
    val inputDir = if (cl.dir.isSupplied) cl.dir() else ""
    val isStar = cl.star.isSupplied
    var applyOnAllSources = if (isStar) true else cl.applyOnAllSources.isSupplied
    var additionalResults = cl.additionalResults.isSupplied
    val report = cl.report.isSupplied
    val blindSearch = cl.blind.isSupplied

    val drop = cl.drop.isSupplied || blindSearch

    val onlyPhotometricModels = cl.photometricModels.isSupplied

    val ra = if (cl.ra.isSupplied) Some(cl.ra()) else None
    val dec = if (cl.ra.isSupplied) Some(cl.dec()) else None
    val pmra = if (cl.ra.isSupplied) Some(cl.pmra()) else None
    val pmdec = if (cl.ra.isSupplied) Some(cl.pmdec()) else None
    val referenceEpoch = if (cl.ra.isSupplied) Some(cl.referenceEpoch()) else None
    val suppliedPosion = Some((ra,dec,pmra,pmdec,referenceEpoch))

    val imageFocus = getImageFocus(objectName
                                   , inputDir
                                   , isStar
                                   , blindSearch
                                   , suppliedPosion).getOrElse(return false)

    val debugStorage = cl.debugStorage.isSupplied
    val astrometry = cl.astrometry.isSupplied

    if (astrometry) {
      applyOnAllSources = false
      additionalResults = false
    }

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    if(!report) {
      info(s"${MyString.rightPadding("\tinput dir", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inputDir'")
      imageFocus.print(COMMAND_INFO_LEFT_SIDE_SIZE)
      val apertureString = if (cl.fixedAperture().isEmpty) "using apertures calculated from image fwhm average"
      else s"using fixed apertures min:${cl.fixedAperture()(0)} anular:${cl.fixedAperture()(1)} max:${cl.fixedAperture()(2)}"
      info(s"${MyString.rightPadding("\tphotometry apertures", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$apertureString'")
    }

    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID

    //build mpo storage
    val imageFocusStorage =
      if (report || blindSearch)  None
      else Some((imageFocus.composedName, new ConcurrentLinkedQueue[ImageFocusInfo]()))

    //report parameters used
    if (imageFocusStorage.isEmpty && !report) info(s"${MyString.rightPadding("\tfind all known mpos(BLIND SEARCH)", COMMAND_INFO_LEFT_SIDE_SIZE)}: 'true'")
    if (!report) info(s"${MyString.rightPadding("\tdrop previous results", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$drop'")
    if (applyOnAllSources && !report) info(s"${MyString.rightPadding("\tphotometry applied on all sources", COMMAND_INFO_LEFT_SIDE_SIZE)}: 'true'")
    if (report) info(s"${MyString.rightPadding("\tgenerating report for", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$objectName'")
    if (blindSearch) info(s"${MyString.rightPadding("\tblind mode (no image focus)", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$blindSearch'")
    if (onlyPhotometricModels) info(s"${MyString.rightPadding("\treport only phtometric models", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$onlyPhotometricModels'")
    if (additionalResults) info(s"${MyString.rightPadding("\tgenerating additional results", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$additionalResults'")
    if (debugStorage) info(s"${MyString.rightPadding("\tusing debug storage database collection", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$debugStorage' collection name:'${imageFocus.getDebuggingID}'")
    if (astrometry) info(s"${MyString.rightPadding("\tusing astrometry setup", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$astrometry'")

    if (ra.isDefined) info(s"${MyString.rightPadding("\tfixed position ra", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${ra.get}'")
    if (dec.isDefined) info(s"${MyString.rightPadding("\tfixed position dec", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${dec.get}'")
    if (pmra.isDefined) info(s"${MyString.rightPadding("\tfixed position pm ra", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${pmra.get}'")
    if (pmdec.isDefined) info(s"${MyString.rightPadding("\tfixed position pm dec", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${pmdec.get}'")
    if (referenceEpoch.isDefined) info(s"${MyString.rightPadding("\tfixed position reference epoch", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${referenceEpoch.get}'")

    if (drop && !report) Path.resetDirectory(s"$M2_OUTPUT_ROOT_PATH/$objectID")

    val (objectDetectionRootPath,astrometryRootPath,photometryAbsoluteRootPath,_) = getRoothPathSeq(objectID)

    //manage paths
    if (!report && additionalResults) Path.createDirectoryIfNotExist(objectDetectionRootPath)
    if (!Path.directoryExist(astrometryRootPath)) Path.createDirectoryIfNotExist(astrometryRootPath)
    if (!Path.directoryExist(photometryAbsoluteRootPath)) Path.createDirectoryIfNotExist(photometryAbsoluteRootPath)

    if (report) {  //report
      if (drop) Path.resetDirectory(objectDetectionRootPath)
      PhotometryAbsolute.report(imageFocus
                                , onlyPhotometricModels
                                , objectDetectionRootPath
                                , photometryAbsoluteRootPath
                                , debugStorage)
    }
    else {  //photometry absolute
      //init the JPL spice library
      if (!isStar && !blindSearch) Spice.init(verbose = verboseFlag)

      //create the catalog
      val spkVersion: Option[String] = None
      Path.ensureDirectoryExist(objectDetectionRootPath + "/detailed_analysis/")
      PhotometryAbsolute.processDir(
        imageFocus
        , inputDir
        , objectDetectionRootPath + "/detailed_analysis/"
        , applyOnAllSources
        , drop
        , imageFocusStorage
        , fixedApertureSeq = if (cl.fixedAperture().isEmpty) None else Some(cl.fixedAperture().toArray)
        , isStar
        , blindSearch
        , additionalResults = verboseFlag
        , spkVersion
        , debugStorage = debugStorage
        , astrometry
        , verbose = verboseFlag)

      if (!isStar && !blindSearch) Spice.close()

      if (imageFocusStorage.isDefined)
        ImageFocusInfo.detectionSummary(objectDetectionRootPath + PhotometryAbsolute.PHOTOMETRY_ABSOLUTE_DETECTION_SUMMARY_CSV
                                        , imageFocusStorage.get
                                        , drop)

      //build the photometric models (polynomial fit)
      val gaiaPhotModel = GaiaPhotometricModel(objectID)
      val inputImageNameSeq = Path.getSortedFileList(inputDir,".fits") map (s=> MyFile.removeFileExtension(s.getName))

      gaiaPhotModel.calculate(photometryAbsoluteRootPath
                               , inputImageNameSeq
                               , additionalResults)
      gaiaPhotModel.close()
    }
    true
  }
  //---------------------------------------------------------------------------
  def commandLightcurve(cl: CommandLineParser): Boolean = {
    warning(s"Command:'$COMMAND_LIGHT_CURVE'")
    lastCommand = COMMAND_LIGHT_CURVE
    lastObject = cl.lightCurve()

    val objectName    = cl.lightCurve()
    val isStar        = cl.star.isSupplied
    val rotPeriod     = cl.rotPeriod.isSupplied
    val imageFocus    = getImageFocus(objectName, inputDir= "", isStar, blindSearch = false).getOrElse(return false)
    val debugStorage  = cl.debugStorage.isSupplied

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    imageFocus.print(COMMAND_INFO_LEFT_SIDE_SIZE)
    info(s"${MyString.rightPadding("\tcalulate rotational period", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$rotPeriod'")
    if (debugStorage) info(s"${MyString.rightPadding("\tusing debug storage database collection", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$debugStorage' collection name:'${imageFocus.getDebuggingID}'")

    val vr = getVR(cl, imageFocus.asInstanceOf[ImageFocusMPO])
    if(!imageFocus.isStar)
      info(s"${MyString.rightPadding("\tmpo V-R", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$vr'")

    //manage paths
    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID
    val (objectDetectionRootPath, astrometryRootPath, photometryAbsoluteRootPath,_) = getRoothPathSeq(objectID)

    if (!Path.directoryExist(astrometryRootPath)) Path.resetDirectory(astrometryRootPath)
    if (!Path.directoryExist(photometryAbsoluteRootPath)) Path.resetDirectory(photometryAbsoluteRootPath)

    if(!isStar) Spice.init()
    val spkVersion: Option[String] = None
    LightCurve.calculate(imageFocus
                         , vr
                         , rotPeriod
                         , astrometryRootPath
                         , photometryAbsoluteRootPath
                         , spkVersion
                         , debugStorage = debugStorage
                         , verbose = verboseFlag)
    if(!isStar) Spice.close()

    LightCurve.compiledResultsSummary(objectDetectionRootPath
                                      , astrometryRootPath
                                      , photometryAbsoluteRootPath
                                      , imageFocus.isStar
                                      , debugStorage)
    true
  }
  //---------------------------------------------------------------------------
  def commadCommonFov(cl: CommandLineParser): Boolean = {
    warning(s"Command:'$COMMAND_FIND_COMMON_FOV'")
    lastCommand = COMMAND_FIND_COMMON_FOV
    lastObject  = cl.findCommonFov()

    val objectName            = cl.findCommonFov()
    val isStar                = cl.star.isSupplied
    val fovPercentage         = cl.fovPercentage()
    val minImagesPerSolution  = cl.minImagesPerSolution()
    val minCommonSource       = cl.minCommonSource()
    val debugStorage = cl.debugStorage.isSupplied
    val imageFocus = getImageFocus(objectName, inputDir = "", isStar, blindSearch = false).getOrElse(return false)

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    if (!isStar) imageFocus.print(COMMAND_INFO_LEFT_SIDE_SIZE)
    info(s"${MyString.rightPadding("\tmin fov percentage intersection per solution", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$fovPercentage'")
    info(s"${MyString.rightPadding("\tmin images per solution", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$minImagesPerSolution'")
    info(s"${MyString.rightPadding("\tmin common sources per solution", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$minCommonSource'")
    if (debugStorage) info(s"${MyString.rightPadding("\tusing debug storage database collection", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$debugStorage' collection name:'${imageFocus.getDebuggingID}'")

    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID
    val (_, _, _, photometryDifferentialRootPath) = getRoothPathSeq(objectID)
    if (!Path.directoryExist(photometryDifferentialRootPath)) Path.resetDirectory(photometryDifferentialRootPath)

    CommonFov(imageFocus
              , fovPercentage
              , minImagesPerSolution
              , minCommonSource
              , photometryDifferentialRootPath
              , debugStorage = debugStorage
              , verbose = verboseFlag)
    true
  }
  //---------------------------------------------------------------------------
  def commandPhotometryDifferential(cl: CommandLineParser): Boolean = {
    warning(s"Command:'$COMMAND_PHOTOMETRY_DIFFERENTIAL'")
    lastCommand = COMMAND_PHOTOMETRY_DIFFERENTIAL
    lastObject  = cl.photometryDifferential()

    val objectName              = cl.photometryDifferential()
    val isStar                  = cl.star.isSupplied
    val vr                      = cl.vr()
    val minCommonSource         = cl.minCommonSource()
    val minPeriodHours          = cl.minPeriodHours()
    val maxPeriodHours          = cl.maxPeriodHours()
    val frequencySamplesPerPeak = cl.frequencySamplesPerPeak()
    val peakCount               = cl.peakCount()
    val fourierSeriesFitDegree  = cl.fourierSeriesFitDegree()
    val imageFocus              = getImageFocus(objectName, inputDir = "", isStar, blindSearch = false).getOrElse(return false)
    val debugStorage            = cl.debugStorage.isSupplied

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    imageFocus.print(COMMAND_INFO_LEFT_SIDE_SIZE)
    if (!isStar) info(s"${MyString.rightPadding("\tmpo V-R", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$vr'")

    info(s"${MyString.rightPadding("\tmin common sources per solution", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$minCommonSource'")
    info(s"${MyString.rightPadding("\tfrequency analysis: min period hours", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$minPeriodHours'")
    info(s"${MyString.rightPadding("\tfrequency analysis: max period hours", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$maxPeriodHours'")
    info(s"${MyString.rightPadding("\tfrequency analysis: samples per peak", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$frequencySamplesPerPeak'")
    info(s"${MyString.rightPadding("\trotational period peaks count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$peakCount'")
    info(s"${MyString.rightPadding("\tFourier series fit degree", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$fourierSeriesFitDegree'")
    if (debugStorage) info(s"${MyString.rightPadding("\tusing debug storage database collection", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$debugStorage' collection name:'${imageFocus.getDebuggingID}'")

    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID
    val (_, _, _, photometryDifferentialRootPath) = getRoothPathSeq(objectID)
    if (!Path.directoryExist(photometryDifferentialRootPath)) Path.resetDirectory(photometryDifferentialRootPath)

    if (!isStar) Spice.init()

    PhotometryDifferential(
        imageFocus
      , vr
      , minCommonSource
      , minPeriodHours
      , maxPeriodHours
      , peakCount
      , frequencySamplesPerPeak
      , fourierSeriesFitDegree
      , photometryDifferentialRootPath
      , debugStorage = debugStorage
      , verbose = verboseFlag)

    if (!isStar) Spice.close()
    true
  }
  //---------------------------------------------------------------------------
  def commandMergeDiffPhot(cl: CommandLineParser): Boolean = {
    warning(s"Command:'$COMMAND_MERGE_DIFF_PHOT'")
    lastCommand = COMMAND_MERGE_DIFF_PHOT
    lastObject = cl.mergeDifferentialPhotometry()

    val objectName = cl.mergeDifferentialPhotometry()
    val minPeriodHours = cl.minPeriodHours()
    val maxPeriodHours = cl.maxPeriodHours()
    val frequencySamplesPerPeak = cl.frequencySamplesPerPeak()
    val peakCount = cl.peakCount()
    val fourierSeriesFitDegree = cl.fourierSeriesFitDegree()
    val step = cl.step()
    val isStar = cl.star.isSupplied
    val imageFocus: ImageFocusTrait = getImageFocus(objectName, inputDir = "", isStar, blindSearch = false).getOrElse(return false)
    val debugStorage = cl.debugStorage.isSupplied

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    imageFocus.print(COMMAND_INFO_LEFT_SIDE_SIZE)
    info(s"${MyString.rightPadding("\tfrequency analysis: min period hours", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$minPeriodHours'")
    info(s"${MyString.rightPadding("\tfrequency analysis: max period hours", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$maxPeriodHours'")
    info(s"${MyString.rightPadding("\tfrequency analysis: samples per peak", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$frequencySamplesPerPeak'")
    info(s"${MyString.rightPadding("\trotational period peaks count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$peakCount'")
    info(s"${MyString.rightPadding("\tFourier series fit degree", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$fourierSeriesFitDegree'")
    info(s"${MyString.rightPadding("\tdifferential magnitude step", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$step'")
    if (debugStorage) info(s"${MyString.rightPadding("\tusing debug storage database collection", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$debugStorage' collection name:'${imageFocus.getDebuggingID}'")

    val objectID = if (debugStorage) imageFocus.getDebuggingID else imageFocus.getID
    val (_, _, _, photometryDifferentialRootPath) = getRoothPathSeq(objectID)
    if (!Path.directoryExist(photometryDifferentialRootPath)) Path.resetDirectory(photometryDifferentialRootPath)
    val inputDir = s"$photometryDifferentialRootPath/${RotationalPeriod.ROTATIONAL_PERIOD_PARTIAL_DIR}"
    Merge(
      imageFocus
      , minPeriodHours //single peak
      , maxPeriodHours //single peak
      , peakCount
      , frequencySamplesPerPeak
      , fourierSeriesFitDegree
      , step
      , inputDir
      , photometryDifferentialRootPath + Merge.ROTATIONAL_PERIOD_MERGE_OUTPUT_DIR)
    true
  }
  //---------------------------------------------------------------------------
  def commadExploreRotationalPeriod(cl: CommandLineParser): Boolean = {
    warning(s"Command:'$COMMAND_EXPLORE_ROT_PERIOD'")
    lastCommand = COMMAND_EXPLORE_ROT_PERIOD
    lastObject = cl.exploreRotPeriod()

    val objectName = cl.exploreRotPeriod()
    val isStar = cl.star.isSupplied
    val imageFocus = getImageFocus(objectName, inputDir = "", isStar, blindSearch = false).getOrElse(return false)

    val csvFile = cl.csv()
    val minPeriodHours = cl.minPeriodHours()
    val maxPeriodHours = cl.maxPeriodHours()
    val frequencySamplesPerPeak = cl.frequencySamplesPerPeak()
    val peakCount = cl.peakCount()
    val fourierSeriesFitDegree = if (cl.best.isSupplied) cl.fourierSeriesFitDegree() else 2
    val best = if (cl.best.isSupplied) cl.best() else 1
    val debugStorage = cl.debugStorage.isSupplied

    val suffix = if(cl.debugStorage.isSupplied) "_debug" else ""
    var oDir = s"$M2_OUTPUT_ROOT_PATH/${imageFocus.getID}$suffix/${ExploreRotationalPeriod.EXPLORE_OUTPUT_DIR}min_${minPeriodHours.toInt}h_max_${maxPeriodHours.toInt}h"

    val forceRotPeriodHours = if (cl.forceRotPeriodHours.isSupplied) {
      oDir = s"$M2_OUTPUT_ROOT_PATH/${imageFocus.getID}$suffix/${ExploreRotationalPeriod.EXPLORE_OUTPUT_DIR}fixed_${cl.forceRotPeriodHours().toInt}h"
      Some(cl.forceRotPeriodHours())
    }
    else None

    val telescopeFilterSeq = if (cl.telescopeSeq.isSupplied) Some(cl.telescopeSeq().toArray) else None
    val filterSeq = if (cl.filterSeq.isSupplied) Some(cl.filterSeq().toArray) else None
    val timeRangeSeq = if (cl.timeRangeSeq.isSupplied) Some(cl.timeRangeSeq().toArray) else None

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    imageFocus.print(COMMAND_INFO_LEFT_SIDE_SIZE)

    if (forceRotPeriodHours.isDefined) info(s"${MyString.rightPadding("\tforce to rotation period", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${forceRotPeriodHours.get}'")
    else {
      info(s"${MyString.rightPadding("\tfrequency analysis: min period hours", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$minPeriodHours'")
      info(s"${MyString.rightPadding("\tfrequency analysis: max period hours", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$maxPeriodHours'")
    }

    info(s"${MyString.rightPadding("\tfrequency analysis: samples per peak", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$frequencySamplesPerPeak'")
    info(s"${MyString.rightPadding("\trotational period peaks count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$peakCount'")
    info(s"${MyString.rightPadding("\tFourier series fit degree", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$fourierSeriesFitDegree'")
    info(s"${MyString.rightPadding("\tcsv file", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$csvFile'")
    info(s"${MyString.rightPadding("\tbest rotational periods to explore", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$best'")
    if (debugStorage) info(s"${MyString.rightPadding("\tusing debug storage database collection", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$debugStorage'")
    info(s"${MyString.rightPadding("\toutput dir", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$oDir'")

    if (telescopeFilterSeq.isDefined) info(s"${MyString.rightPadding("\tusing telescope filter sequence", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${telescopeFilterSeq.get.mkString("{", ",", "}")}'")
    if (filterSeq.isDefined) info(s"${MyString.rightPadding("\tusing filter sequence", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${filterSeq.get.mkString("{", ",", "}")}'")
    if (timeRangeSeq.isDefined) info(s"${MyString.rightPadding("\tusing time range sequence", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${timeRangeSeq.get.mkString("{", ",", "}")}'")

    ExploreRotationalPeriod.run(
        imageFocus
      , csvFile
      , minPeriodHours
      , maxPeriodHours
      , frequencySamplesPerPeak
      , peakCount
      , fourierSeriesFitDegree
      , best
      , Path.resetDirectory(oDir)
      , forceRotPeriodHours
      , telescopeFilterSeq = telescopeFilterSeq
      , filterSeq = filterSeq
      , timeRangeSeq = timeRangeSeq)

    true
  }
  //---------------------------------------------------------------------------
  def commadOccultation(cl: CommandLineParser): Boolean = {

    warning(s"Command:'$COMMAND_OCCULTATION'")
    lastCommand = COMMAND_OCCULTATION
    lastObject = cl.occultation()

    val objectName = cl.occultation()
    val inputDir = cl.dir()
    val occConf = if (cl.occConf.isSupplied) Some(cl.occConf()) else None

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    info(s"${MyString.rightPadding("\tprocessing the occultation of object", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$objectName'")
    info(s"${MyString.rightPadding("\tprocessing the occultation in directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inputDir'")
    if (occConf.isDefined)
      info(s"${MyString.rightPadding("\tusing configuration file for occultation", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${occConf.get}'")

    SourceOccultation.calculate(objectName
                                , inputDir
                                , occConf
                                , verbose = verboseFlag)

    true
  }
  //---------------------------------------------------------------------------
  def commadSpkDb(cl: CommandLineParser): Boolean = {

    warning(s"Command:'$COMMAND_SPICE_SPK_DATABASE'")
    lastCommand = COMMAND_SPICE_SPK_DATABASE

    val s = cl.spiceSpkDb()
    val generate = cl.generate.isSupplied
    val update = cl.update.isSupplied
    val report = cl.report.isSupplied
    val drop = cl.drop.isSupplied

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    info(s"${MyString.rightPadding("\tobject", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$s'")
    if (update) {
      info(s"${MyString.rightPadding("\tupdating SPICE SPK database with directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$s'")
      SpiceSPK_DB.update(s)
    }
    else {
      if (generate) {
        val oDir = cl.outputDir()
        val moreRecent = if(cl.lastMoreRecent.isSupplied) Some(cl.lastMoreRecent()) else None
        val version = if(cl.ver.isSupplied) Some(cl.ver()) else None
        info(s"${MyString.rightPadding("\toutput directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$oDir'")

        if(moreRecent.isDefined)
          info(s"${MyString.rightPadding("\tLast more rececent", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${moreRecent.get}'")

        if (version.isDefined)
          info(s"${MyString.rightPadding("\tversion", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${version.get}'")

        SpiceSPK_DB.generate(s,Path.resetDirectory(oDir), moreRecent, version)
      }
      if (report) {
        s.split(",").foreach { spkID =>
          val spkName = spkID.trim
          print(s"---------- SPK ID: $spkName starts ----------\n")
          SpiceSPK_DB.report(spkName)
          print(s"---------- SPK ID: $spkName ends   ----------\n")
        }
      }
      else
        if (drop) {
          s.split(",").foreach { spkID =>
            info(s"Removig from database the SPK:'$spkID'")
            SpiceSPK_DB.drop(spkID.trim)
          }
      }
    }
    true
  }
  //---------------------------------------------------------------------------
  def commadWeb(cl: CommandLineParser): Boolean = {

    warning(s"Command:'$COMMAND_WEB'")
    lastCommand = COMMAND_WEB
    lastObject = cl.web()

    val objectName = cl.web()
    val astrometry = cl.astrometry.isSupplied
    val exists = cl.exists.isSupplied
    val vr = cl.vrQuery.isSupplied

    val optImageFocus = getImageFocus(objectName, inputDir = "", isStar = false, blindSearch = false, verbose = !vr)

    if (optImageFocus.isEmpty && !vr)
      return error(s"Unknown object '$objectName'")

    val oDir = if (exists) "" else cl.outputDir()

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    info(s"${MyString.rightPadding("\tgetting the information of", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${objectName.replaceAll("'","")}'")

    if (astrometry) {
      info(s"${MyString.rightPadding("\tsub-command astrometry:", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$astrometry'")
      info(s"${MyString.rightPadding("\toutput directory:", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$oDir'")
      optImageFocus.get.print(COMMAND_INFO_LEFT_SIDE_SIZE)
    }

    if (vr) {
      info(s"${MyString.rightPadding("\tsub-command vr:", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$vr'")
      info(s"${MyString.rightPadding("\toutput directory:", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$oDir'")
      if (optImageFocus.isDefined)
        optImageFocus.get.print(COMMAND_INFO_LEFT_SIDE_SIZE)
    }

    if (exists) {
      info(s"${MyString.rightPadding("\tsub-command exists:", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$exists'")
      optImageFocus.get.print(COMMAND_INFO_LEFT_SIDE_SIZE)
      true
    }
    else {
      if (astrometry) {
        QueryAstrometry.query(optImageFocus.get.asInstanceOf[ImageFocusMPO], oDir, linkImages = false)
        true
      }
      else
        if (vr) {
          val imageFocus =
            if (optImageFocus.isDefined) Some(optImageFocus.get.asInstanceOf[ImageFocusMPO])
            else None
          QueryVR(objectName, imageFocus, oDir).query()
          true
        }
        else error(s"Unknown sub command for command 'web'")
    }
  }

  //---------------------------------------------------------------------------
  def commadUpdateWebResults(cl: CommandLineParser): Boolean = {

    warning(s"Command:'$COMMAND_UPDATE_WEB_RESULTS'")
    lastCommand = COMMAND_UPDATE_WEB_RESULTS
    lastObject = cl.updateWebResults()

    val dir = cl.updateWebResults()
    info(s"${MyString.rightPadding("\tprocessing directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$dir'")

    Spice.init()
    UpdateWebResults(dir)
    Spice.close()

    true
  }
  //---------------------------------------------------------------------------
  def commandOrbitResidual(cl: CommandLineParser): Boolean = {

    warning(s"Command:'$COMMAND_ORBIT_RESIDUAL'")
    lastCommand = COMMAND_ORBIT_RESIDUAL
    lastObject = cl.orbitResidual()

    val objectName = lastObject
    val imageFocus = getImageFocus(objectName, "", isStar = false, blindSearch = false).getOrElse(return false)
    val debugStorage = cl.debugStorage.isSupplied

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    if (imageFocus != null) imageFocus.print(COMMAND_INFO_LEFT_SIDE_SIZE)
    info(s"${MyString.rightPadding("\tobject", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$objectName'")
    if (debugStorage) info(s"${MyString.rightPadding("\tusing debug storage", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$debugStorage' collection name:'${imageFocus.getDebuggingID}'")

    val suffix = if(cl.debugStorage.isSupplied) "_debug" else ""
    val astrometryRootPath = s"$M2_OUTPUT_ROOT_PATH/${imageFocus.getID}$suffix/$M2_ASTROMETRY_ROOT_PATH/"

    Spice.init()
    OrbitStats.calculate(imageFocus
                         , astrometryRootPath
                         , debugStorage  = true)
    Spice.close()

    true
  }
  //---------------------------------------------------------------------------
  def commandAddMetaData(cl: CommandLineParser): Boolean = {

    warning(s"Command:'$COMMAND_ADD_METADATA'")
    lastCommand = COMMAND_ADD_METADATA
    lastObject = cl.addMetaData()

    val objectName = cl.addMetaData()
    val inDir = cl.dir()
    val outDir = cl.outputDir()
    val keyValueSeq = if (cl.keyValueSeq.isSupplied) cl.keyValueSeq() else List[String]()
    val keep = cl.keep.isSupplied
    val isStar = cl.star.isSupplied
    val remove = cl.remove.isSupplied
    val outputSpkFile = if (cl.outputFile.isSupplied) Some(cl.outputFile()) else None
    val noneObject  = objectName == "NONE"
    val imageFocus =
      if ((objectName == "GUESS") ||  noneObject) null
        else getImageFocus(objectName, inputDir = "", isStar, blindSearch = false).getOrElse(return false)

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    if(imageFocus != null) imageFocus.print(COMMAND_INFO_LEFT_SIDE_SIZE)
    info(s"${MyString.rightPadding("\tobject", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$objectName'")
    info(s"${MyString.rightPadding("\tinput directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inDir'")
    info(s"${MyString.rightPadding("\toutput directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$outDir'")
    info(s"${MyString.rightPadding("\tkey-value seq", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${keyValueSeq.mkString("{",",","}")}'")
    if (remove) info(s"${MyString.rightPadding("\tremove entries", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$remove'")
    if (outputSpkFile.isDefined) info(s"${MyString.rightPadding("\toutput file dor SPICE SPK names", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${cl.outputFile()}'")
    else info(s"${MyString.rightPadding("\tkeep current entries", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$keep'")
    if (noneObject)
      info(s"${MyString.rightPadding("\tnone object is associated with images", COMMAND_INFO_LEFT_SIDE_SIZE)}")

    MetaData(imageFocus, inDir, outDir, keyValueSeq, keep, remove, outputSpkFile, noneObject)

    true
  }
  //---------------------------------------------------------------------------
  def commandRename(cl: CommandLineParser): Boolean = {

    warning(s"Command:'$COMMAND_RENAME'")
    lastCommand = COMMAND_RENAME
    lastObject = cl.rename()

    val inDir = cl.rename()
    val outDir = cl.outputDir()

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")

    info(s"${MyString.rightPadding("\tinput directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inDir'")
    info(s"${MyString.rightPadding("\toutput directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$outDir'")

    UtilImage.rename(inDir,outDir)

    true
  }
  //---------------------------------------------------------------------------
  def commadAlign(cl: CommandLineParser): Boolean = {

    warning(s"Command:'$COMMAND_ALIGN'")
    lastCommand = COMMAND_ALIGN
    lastObject = cl.align()

    val inputDir = cl.align()
    val outputDir = cl.outputDir()
    val action = if (cl.action.isSupplied) Some(cl.action()) else None

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    info(s"${MyString.rightPadding("\talign the images in directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inputDir'")
    info(s"${MyString.rightPadding("\toutput directory directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$outputDir'")
    if (action.isDefined)
      info(s"${MyString.rightPadding("\taction", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${action.get}'")

    Align.processDirectory(inputDir,outputDir,action)

    true
  }
  //---------------------------------------------------------------------------
  //convert to int data type
  def commadConvertToIntDataType(cl: CommandLineParser) = {
    warning(s"Command:'$COMMAND_CONVERT_TO_INT_DATA_TYPE'")
    lastCommand = COMMAND_CONVERT_TO_INT_DATA_TYPE
    lastObject  = "none"

    val inDir     = cl.convertToIntDataType()
    val outputDir = cl.outputDir()
    val newBitPix = cl.intDataType()

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    info(s"${MyString.rightPadding("\tinput inDir", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inDir'")
    info(s"${MyString.rightPadding("\toutput dir", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$outputDir'")
    info(s"${MyString.rightPadding("\tnew data type bit size", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$newBitPix'")

    ImageDataTypeConversion.processDir(inDir,outputDir,newBitPix)
    true
  }
  //---------------------------------------------------------------------------
  //database list
  def commadDB_List(cl: CommandLineParser) = {
    warning(s"Command:'$COMMAND_DB_LIST'")
    lastCommand = COMMAND_DB_LIST
    lastObject  = "none"

    val gaiaDB = GaiaDB(MyConf(MyConf.c.getString("Database.mongoDB.gaia")))
    val nameSeq = gaiaDB.getDatabaseNameSeq
    info(s"Current connection to MogoDB has: ${nameSeq.length} databases")
    nameSeq.zipWithIndex.foreach { case (s,i) => info(s" Database ${f"$i%03d"}: '$s'")}
    gaiaDB.close()
    true
  }
  //---------------------------------------------------------------------------
  //tno location
  def commadMpoLocation(cl: CommandLineParser) = {
    warning(s"Command:'$COMMAND_MPO_LOCATION'")
    lastCommand = COMMAND_MPO_LOCATION
    lastObject = "none"

    val imageName = cl.mpoLocation()
    val mpo =
      if (cl.mpo.isSupplied) {
        val r  = getImageFocus(cl.mpo(), "", false, false)
        if (r.isDefined) Some(r.get.asInstanceOf[ImageFocusMPO])
        else None
      }
      else None

    info(s"${MyString.rightPadding("\timage", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$imageName'")
    if (mpo.isDefined)
      info(s"${MyString.rightPadding("\tMPO", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${mpo.get.composedName}'")
    else
      info(s"${MyString.rightPadding("\tfinding all known MPOs using asteroid checker", COMMAND_INFO_LEFT_SIDE_SIZE)}: 'true'")

    MpoLocation.locate(imageName, mpo)
    true
  }

  //---------------------------------------------------------------------------
  //orbit integration
  def commadOrbitIntegration(cl: CommandLineParser): Boolean = {
    warning(s"Command:'$COMMAND_ORBIT_INTEGRATION'")
    lastCommand = COMMAND_ORBIT_INTEGRATION
    lastObject = "none"

    val startDate = cl.orbitIntegration()
    var timeStamp: LocalDateTime = null
    Try {
      timeStamp = LocalDateTime.parse(startDate + "T00:00:00")
    }
    match {
      case Success(_) =>
      case Failure(_: Exception) =>
        error(s"The input date:'$startDate' is not valid")
        return false
    }
    val stepSizeDays = cl.stepDaySize()
    val outputDir = cl.dir()

    info(s"${MyString.rightPadding("\tstart date", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${timeStamp.toString}'")
    info(s"${MyString.rightPadding("\tStep size in days", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$stepSizeDays'")
    info(s"${MyString.rightPadding("\tOutput directory", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$outputDir'")

    OrbitIntegrator.allMpoIntegration(timeStamp, stepSizeDays, outputDir)
    true
  }
  //---------------------------------------------------------------------------
  //fumo (finding unknown moving objects)
  def commadFumo(cl: CommandLineParser): Boolean = {
    warning(s"Command:'$COMMAND_FUMO'")
    lastCommand = COMMAND_FUMO
    lastObject = "none"

    val inDir = cl.fumo()
    val outputDir = cl.outputDir()
    val minElapsedMinutesBetweenImages = cl.minElapsedMinutesBetweenImages()
    val mpoToIgnore = if (cl.mpo.isSupplied) Some(cl.mpo()) else None

    val stepSeq =
      if (cl.step.isSupplied) Array(cl.step().toInt)
      else Array(1,2,3)

    info(s"${MyString.rightPadding("\tcore count", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${CPU.getCoreCount()}'")
    info(s"${MyString.rightPadding("\tinput dir", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$inDir'")
    info(s"${MyString.rightPadding("\toutput dir", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$outputDir'")
    info(s"${MyString.rightPadding("\tmin elapsed minuted between images", COMMAND_INFO_LEFT_SIDE_SIZE)}: '$minElapsedMinutesBetweenImages'")
    info(s"${MyString.rightPadding("\tExecuting only step", COMMAND_INFO_LEFT_SIDE_SIZE)}: '${stepSeq.mkString("{",",","}")}'")

    Fumo.calculate(
      inDir
      , outputDir
      , minElapsedMinutesBetweenImages
      , stepSeq
      , verbose = verboseFlag
    )

    true
  }
  //---------------------------------------------------------------------------
  def commandManager(cl: CommandLineParser) : Boolean = {

    //version
    if (cl.version.isSupplied)                     return commandVersion()

    //astrometry
    if (cl.query.isSupplied)                       return commadQuery(cl)
    if (cl.astrometryCatalog.isSupplied)           return commadAstrometryCatalog(cl)
    if (cl.fitWcs.isSupplied)                      return commadFitWcs(cl)

    //absolute photometry
    if (cl.photometryAbsolute.isSupplied)          return commadPhotometryAbsolute(cl)
    if (cl.lightCurve.isSupplied)                  return commandLightcurve(cl)

    //differential photometry
    if (cl.findCommonFov.isSupplied)               return commadCommonFov(cl)
    if (cl.photometryDifferential.isSupplied)      return commandPhotometryDifferential(cl)
    if (cl.mergeDifferentialPhotometry.isSupplied) return commandMergeDiffPhot(cl)
    if (cl.exploreRotPeriod.isSupplied)            return commadExploreRotationalPeriod(cl)

    //occultation
    if (cl.occultation.isSupplied)                 return commadOccultation(cl)

    //SPICE SPK database
    if (cl.spiceSpkDb.isSupplied)                  return commadSpkDb(cl)

    //web
    if (cl.web.isSupplied)                         return commadWeb(cl)
    if (cl.updateWebResults.isSupplied)            return commadUpdateWebResults(cl)

    //utilities
    if (cl.orbitResidual.isSupplied)               return commandOrbitResidual(cl)
    if (cl.addMetaData.isSupplied)                 return commandAddMetaData(cl)
    if (cl.rename.isSupplied)                      return commandRename(cl)
    if (cl.align.isSupplied)                       return commadAlign(cl)
    if (cl.convertToIntDataType.isSupplied)        return commadConvertToIntDataType(cl)
    if (cl.dbList.isSupplied)                      return commadDB_List(cl)
    if (cl.mpoLocation.isSupplied)                 return commadMpoLocation(cl)
    if (cl.orbitIntegration.isSupplied)            return commadOrbitIntegration(cl)

    //fumo
    if (cl.fumo.isSupplied)                        return commadFumo(cl)

    true
  }
  //---------------------------------------------------------------------------
  def run(cl: CommandLineParser): Unit = {
    val startMs = System.currentTimeMillis

    initialActions()


   commandManager(cl)
//    SandBox.mainTest(this)

//    Eastro.generateCsv()


    finalActions(startMs)
  }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file M2.scala
//=============================================================================
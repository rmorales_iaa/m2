/*
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//-----------------------------------------------------------------------------
package com.m2
//---------------------------------------------------------------------------
import com.common.geometry.point.Point2D_Double
import com.common.estimator.skyPosition.EstimatedSkyPosition
import com.common.image.catalog.ImageCatalog
import com.common.image.focusType.ImageFocusMPO
import com.common.image.telescope.Telescope
import com.common.jpl.Spice
import com.common.jpl.Spice.loadKernel
import com.common.logger.MyLogger
import com.common.util.file.MyFile
import com.common.util.time.Time
//---------------------------------------------------------------------------
import scala.io.Source
import java.io.{BufferedWriter, File, FileWriter}
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
//-----------------------------------------------------------------------------
object Eastro extends MyLogger {
  //---------------------------------------------------------------------------
  private var imageDB: ImageCatalog = null
  var eAstroMap = scala.collection.mutable.Map[String, FocusItem]()
  var m2_OrbitMap = scala.collection.mutable.Map[String, FocusItem]()
  //---------------------------------------------------------------------------
  case class FocusItem(imageName: String
                       , observingTime: String
                       , observingTimeMidPoint: String
                       , ra: Double
                       , dec: Double
                       , expectedRa: Double
                       , expectedDec: Double
                       , raResidualMas: Double
                       , decResidualMas: Double
                       , pixPos: Point2D_Double = Point2D_Double.POINT_NAN) {
    //-------------------------------------------------------------------------
    def getRaDec = Point2D_Double(ra, dec)
    //-------------------------------------------------------------------------
    def getRaDecResidual = Point2D_Double(raResidualMas, decResidualMas)
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def loadEastroCsv(csvFile:String): Unit = {
    val bufferedSource = Source.fromFile(csvFile)
    var tableOn = false
    var lineCount = 0

    for (line <- bufferedSource.getLines) {
      if (tableOn) {
        lineCount += 1
        if (lineCount > 1) {
          val s = line.trim
            .replaceAll("\t", " ")
            .replaceAll(" +", " ")
            .split(" ")
          if (s.isEmpty || s.size < 30) return

          val observingTime = s(12)
          val exposureTime = s(13).toDouble

          val ra = s(14).toDouble * 15
          val dec = s(15).toDouble


          val xPos = s(19).toDouble
          val yPos = s(20).toDouble

          val jplExpectedRa = s(23).toDouble * 15
          val jplExpectedDec = s(24).toDouble

          val raDecResidualMas = Point2D_Double(jplExpectedRa,jplExpectedDec).getResidualMas(Point2D_Double(ra,dec))

          val observingTimeMidPoint = LocalDateTime.parse(observingTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
            .plusNanos(Time.secondsToNanos(exposureTime / 2d))

          val imageName = MyFile.getFileNameNoPathNoExtension(s(32))
          val m2_ImageName = m2_OrbitMap.keys.filter( _.contains(imageName)).head

          eAstroMap(m2_ImageName) = FocusItem(
              m2_ImageName
            , observingTime
            , observingTimeMidPoint.toString
            , ra
            , dec
            , jplExpectedRa
            , jplExpectedDec
            , raDecResidualMas.x
            , raDecResidualMas.y
            , pixPos = Point2D_Double(xPos, yPos) + Point2D_Double.POINT_ONE)
        }

      }
      else if (line.startsWith("---------------------")) tableOn = true
    }
    bufferedSource.close
  }

  //---------------------------------------------------------------------------
  private def loadM2_OrbitReport(objectName: String
                                 , csvFile: String): Unit = {
    val bufferedSource = Source.fromFile(csvFile)
    var tableOn = false
    var lineCount = 0

    for (line <- bufferedSource.getLines) {
      if (tableOn) {
        lineCount += 1
        if (lineCount >= 1) {
          val s = line.trim
            .split("\t")
          if (s.isEmpty || s.size < 21) return

          val imageName = s(0)
          val telescope = s(2)
          val exposureTime = s(4).toDouble
          val observingTimeMidPoint = s(5)


          val xPos = s(10).toDouble
          val yPos = s(11).toDouble

          val ra = s(12).toDouble
          val dec = s(13).toDouble

          val id = objectName.replace("_debug","").split("_")(1)
          val imageFocus = ImageFocusMPO.build(id, source = null).get

          val observingTime = LocalDateTime.parse(observingTimeMidPoint, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                                 .minusNanos(Time.secondsToNanos(exposureTime / 2d))

          loadKernel(imageFocus.getSpkFilePath(),verbose = true)
          val jplExpectedRaDec = EstimatedSkyPosition.getPosWithFallBack(
              imageFocus
            , Telescope.getOfficialCode(telescope)
            , Telescope.getSpkId(telescope)
            , observingTime.toString)

          val residualMas = jplExpectedRaDec.getResidualMas(Point2D_Double(ra,dec))

          m2_OrbitMap(imageName) = FocusItem(
            imageName
            , observingTime.toString
            , observingTimeMidPoint
            , ra
            , dec
            , jplExpectedRaDec.x
            , jplExpectedRaDec.y
            , residualMas.x
            , residualMas.y
            , pixPos = Point2D_Double(xPos, yPos))
        }

      }
      else tableOn = true
    }
    bufferedSource.close
  }
  //---------------------------------------------------------------------------
  private def getEastroMap(csvFile:String) = {
    //-------------------------------------------------------------------------
    if (eAstroMap.isEmpty) loadEastroCsv(csvFile)
    eAstroMap
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  private def getM2_OrbitMap(objectName: String,csvFile:String) = {
    if (m2_OrbitMap.isEmpty) loadM2_OrbitReport(objectName,csvFile)
    m2_OrbitMap
  }
  //---------------------------------------------------------------------------
  private def writeCsv(csvFile:String) = {
    val bw = new BufferedWriter(new FileWriter(new File(csvFile)))
    val sep = "\t"
    bw.write(s"image$sep" +
      s"observing_time$sep"+
      s"observing_time_mid_point$sep"+

      s"eastro_x_pix(pix)${sep}eastro_y_pix(pix)$sep" +
      s"m2_x_pix(pix)${sep}m2_y_pix(pix)$sep" +

      s"eastro_ra(deg)${sep}eastro_dec(deg)$sep" +
      s"eastro_jpl_ra(deg)${sep}eastro_jpl_dec(deg)$sep" +
      s"eastro_ra_residual(mas)${sep}eastro_dec_residual(mas)$sep" +

      s"m2_ra(deg)${sep}m2_dec(deg)$sep" +
      s"m2_jpl_ra(deg)${sep}m2_jpl_dec(deg)$sep" +
      s"m2_ra_residual(mas)${sep}m2_dec_residual(mas)$sep" +

      s"eastro_m2_pix_dist(pix)${sep}eastro_m2_ra_dec_residual_dist(mas)\n")

    eAstroMap.toArray.sortWith(_._1 < _._1).foreach { case (imageName,eastroFocusItem)=>
      if (m2_OrbitMap.contains(imageName)) {
        val m2FocusItem = m2_OrbitMap(imageName)
        bw.write(imageName + sep +
          eastroFocusItem.observingTime + sep +
          eastroFocusItem.observingTimeMidPoint + sep +

          eastroFocusItem.pixPos.x + sep + eastroFocusItem.pixPos.y + sep +
          m2FocusItem.pixPos.x + sep + m2FocusItem.pixPos.y + sep +

          eastroFocusItem.ra + sep + eastroFocusItem.dec + sep +
          eastroFocusItem.expectedRa + sep + eastroFocusItem.expectedDec + sep +
          eastroFocusItem.raResidualMas + sep + eastroFocusItem.decResidualMas + sep +

          m2FocusItem.ra + sep + m2FocusItem.dec + sep +
          m2FocusItem.expectedRa + sep + m2FocusItem.expectedDec + sep +
          m2FocusItem.raResidualMas + sep + m2FocusItem.decResidualMas + sep +

          eastroFocusItem.pixPos.getDistance(m2FocusItem.pixPos) + sep +
          eastroFocusItem.getRaDecResidual.getDistance(m2FocusItem.getRaDecResidual) +
          "\n")
      }
    }

    info(s"Generate file: '$csvFile'")
    bw.close()
  }
  //---------------------------------------------------------------------------
  def generateCsv(): Unit = {

    Spice.init()
    //val objectName       = "mpo_20447178_2005_ro43"
    val objectName       = "mpo_20055638_2002_ve95"

    //val eAstroReport     = "/home/rafa/Downloads/deleteme/eastro/mpcreport-2005 RO43-OSN1.5-b2-GUCAC4-DR3-OM-20231025-param.txt"
    val eAstroReport     = "/home/rafa/Downloads/deleteme/eastro/mpcreport-2002 VE95-OSN1.5-b2-GUCAC4-DR3-OM-20231025-param.txt"

    val m2OrbitCsvReport = s"/home/rafa/proyecto/m2/output/results/${objectName}_debug/0_source_detection/detection_summary.csv"
    val outputCsv        = s"output/${objectName}_debug_eastro_m2.csv"

    imageDB = ImageCatalog(objectName)
    getM2_OrbitMap(objectName, m2OrbitCsvReport)
    getEastroMap(eAstroReport)
    writeCsv(outputCsv)
    imageDB.close()

    Spice.close()
  }
  //---------------------------------------------------------------------------
}

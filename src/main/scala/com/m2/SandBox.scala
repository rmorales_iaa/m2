/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  15/Feb/2020
 * Time:  15h:57m
 * Description: None
 */
//=============================================================================
package com.m2
//=============================================================================
import com.common.configuration.MyConf
import com.common.csv.CsvRead
import com.common.database.mongoDB.observatories.ObservatoriesDB
import com.common.estimator.skyPosition.EstimatedSkyPosition
import com.common.fits.simpleFits.SimpleFits._
import com.common.fits.synthetize.SyntheticImage
import com.common.fits.wcs.fit.FitWcs
import com.common.geometry.matrix.matrix2D.Matrix2DSourceDetectionByAxisPartition
import com.common.geometry.point.Point2D_Double
import com.common.hardware.cpu.CPU
import com.common.image.estimator.bellShape2D.gaussian.{Gaussian2D_Elliptical, Gaussian2D_EllipticalRotated}
import com.common.image.estimator.bellShape2D.moffat.{Moffat2D_Elliptical, Moffat2D_EllipticalRotated}
import com.common.image.estimator.bellShape2D.normal.{Normal2D_Elliptical, Normal2D_EllipticalRotated}
import com.common.image.estimator.centroid.Centroid
import com.common.image.estimator.fwhm.Fwhm
import com.common.image.focusType.{ImageFocusInfo, ImageFocusMPO}
import com.common.image.mask.Mask
import com.common.image.myImage.MyImage
import com.common.image.occultation.occImage.OccImage
import com.common.image.photometry.absolute.PhotometryAbsolute
import com.common.image.registration._
import com.common.jpl.Spice
import com.common.jpl.horizons.Horizons
import com.common.jpl.web.JplWebQuery
import com.common.logger.MyLogger
import com.common.orbitIntegrator.OrbitIntegrator
import com.common.stat.StatDescriptive
import com.common.timeSeries.LombScargle.LombScargle
import com.common.util.file.MyFile
import com.common.util.parallelTask.ParallelTask
import com.common.util.path.Path
import com.common.util.time.Time
//=============================================================================
import java.time.{LocalDate, LocalDateTime}
import java.util.concurrent.ConcurrentLinkedQueue
import scala.Console.println
import scala.util.{Failure, Success, Try}
//=============================================================================
//=============================================================================
object SandBox extends  MyLogger {

  //---------------------------------------------------------------------------
  def WCS_conversion() = {
    val img = MyImage("/fast_disk/tmp/didimos/didymos_base/science_2022_07_08T05_22_14_630_LIVERPOOL_2048x2056_sdss_i_60s_h_e_20220707_109_1_1_1_io_Didymos.fits")
    val pix_pos_base = Point2D_Double(123.31314683217,1303.10128200297)

    val skyPos = img.pixToSky(pix_pos_base)
    val pixPos = img.skyToPix(skyPos);


    println(pixPos)
    println(skyPos)
  }
  //---------------------------------------------------------------------------
  def getEstPos(m2: M2): Unit = {

    Spice.init()
    val inputImage = "/home/rafa/Downloads/deleteme/one_image/science_2022_07_08T05_22_14_630_LIVERPOOL_2048x2056_sdss_i_60s_h_e_20220707_109_1_1_1_io_Didymos.fits"
    val objectName = "didymos"

    val imageFocus = m2.getImageFocus(objectName).getOrElse(return)

    imageFocus.asInstanceOf[ImageFocusMPO].getEstPosition(MyImage(inputImage))

    println()
    Spice.close()
  }
  //---------------------------------------------------------------------------
  def fitWcs(m2: M2): Unit = {
    val inputDir = "/home/rafa/Downloads/deleteme/single_file/"
    val objectName = "varuna"
    val isStar = false

    val imageFocus = m2.getImageFocus(
      objectName
      , ""
      , isStar
      , blindSearch = false).getOrElse(return)
    FitWcs.fitDirectory(inputDir
                        , checkFlag = false
                        , preciseWCS_SIP = true
                        , onlyElapsedDaysLessThan = None
                        , verbose = true)

    println()
  }
  //---------------------------------------------------------------------------
  def absolutePhotometrySingleFile(m2: M2): Unit = {

    val outputDir  = "output/debug/"
    val imageName = "/home/rafa/proyecto/m2/output/fumo_15min_tikal/output_triplet/000117/registered_wcs_solved/moving_objects/moving_objects_subtracted_normalized/science_2003_06_18T22_33_52_000_OSN_1_5_1024x1024_open_300s_quaoar-013cle_ccd_None.fits"



    val objectName = "quaoar"
    val fixedAperture = None
    val applyOnAllSources = false
    val isStar = false


    /*
    val objectName = "leona"
    val fixedAperture = None
    val applyOnAllSources = false
    val isStar = false*/

    /*
        val objectName = "2013_xc26"
        val fixedAperture = None
        val applyOnAllSources = false
        val isStar = false
    */

    /*
        val objectName   = "didymos"
        val fixedAperture =  None
        val applyOnAllSources = true
        val isStar = false

     */
    /*
        //1995_or
        val objectName = "1995_or"
        val fixedAperture = None
        val applyOnAllSources = true
        val isStar = false
    */
    //------------------------------------------------------------
    //val afiStorage = None //for blind astrometry

    Spice.init()

    val imageFocus = m2.getImageFocus(objectName
      , ""
      , isStar
      , blindSearch = false).getOrElse(return)

    val afiStorage = Some((imageFocus.composedName,new ConcurrentLinkedQueue[ImageFocusInfo]())) //focused
    Path.resetDirectory(outputDir)

    PhotometryAbsolute.processImage(
      MyImage(imageName)
      , imageFocus
      , applyOnAllSources
      , afiStorage
      , fixedAperture
      , isStar = false
      , blindSearch = false
      , outputDir
      , additionalResults = true
      , debugStorage = true
      , verbose = true
    )

    Spice.close()

    if (afiStorage.isDefined) ImageFocusInfo.detectionSummary("output/debugging_summary", afiStorage.get, drop = true)
  }
  //---------------------------------------------------------------------------
  def getTelescopeXYZ() = {
    // observatry 1 is full, use observatory 2 definition file
    // ls -la /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory_8.bsp
    // rm /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory_8.bsp
    // if pinpoint does not exist, download from: https://naif.jpl.nasa.gov/naif/utilities_PC_Linux_64bit.html
    //  /home/rafa/images/tools/spice/latest_spice/pinpoint -def /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory_def_8 -spk /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory_8.bsp
    // ls -la /home/rafa/proyecto/m2/input/spice/kernels/spk/observatories/observatory_8.bsp

    //check that the bsp generated is loaded in the meta kernel
    // kate /home/rafa/proyecto/m2/input/spice/kernels/meta/m2_common_meta_kernel.txt

    //same line as it appears in find_orb.observatories.info. Generated with: https://github.com/Bill-Gray/lunar/blob/master/mpc_code.cpp
    //compiled with: make mpc_code

    //val observatory = ObservatoriesDB.findFirstByName("not").get
    val observatory = ObservatoriesDB.findByID("500").get

    println(s"---------- '${observatory.name}' ----------")

    observatory.printSpkLocation
    observatory.printSpkDefinition

    println(s"---------- '${observatory.name}' ----------")

    println()
  }
  //---------------------------------------------------------------------------
  def sourceDetectionComparision() = {

    val img = MyImage("/home/rafa/Downloads/deleteme/single_file/a.fits")


    val subImageWidth = 67
    val subImageHeight = 67
    val noiseTide = 9390.20315
    val maskSeq = Array[Mask]() //Array(MaskRentangle(102,103,1946,1953, Point2D(2048,2056), positiveArea = true).asInstanceOf[Mask])
    val sourceRestriction = Some((8, 3000))


    val NEW_SOURCE_SEQ = Matrix2DSourceDetectionByAxisPartition.findSource(
      m = img
      , subImageWidth
      , subImageHeight
      , noiseTide
      , maskSeq
      , sourceSizeRestriction = sourceRestriction)
      .sortWith( _.offset.x <  _.offset.x )

    info("NEW_SOURCE_SEQ->" + NEW_SOURCE_SEQ.size )


    val OLD_SOURCE_SEQ = img.findSource(
      noiseTide
      , maskSeq
      , sourceSizeRestriction = sourceRestriction)
      .toSegment2D_seq()
      .sortWith( _.offset.x <  _.offset.x )

    info("OLD_SOURCE_SEQ->" + OLD_SOURCE_SEQ.size)


    if (NEW_SOURCE_SEQ.length !=  OLD_SOURCE_SEQ.length)
      error("Source count mismatch")

    //    NEW_SOURCE_SEQ.find( _.id == 87688).get.prettyPrint()
    //  OLD_SOURCE_SEQ.find( _.id == 87351).get.prettyPrint()

    val OLD_SOURCE_OFFSET_SeQ = OLD_SOURCE_SEQ map ( _.offset)
    val ONLY_IN_NEW_SEq =
      NEW_SOURCE_SEQ
        .filter( s=> !OLD_SOURCE_OFFSET_SeQ.contains(s.offset))
        .sortWith( _.offset.x <  _.offset.x )
    info("ONLY_IN_NEW_SEq->" + ONLY_IN_NEW_SEq.size)
    println()
  }
  //---------------------------------------------------------------------------
  def centroid_check() = {

    val img = MyImage("/home/rafa/Downloads/deleteme/synthetize_five_source.fits")
    val imageMask = img.getMaskSeq

    val source_seq = img.findSourceSeq(noiseTide = 10.194, imageMask, sourceSizeRestriction = Some(4,2000)).toSegment2D_seq()
    val source = source_seq.head

    //source.prettyPrint()
    Fwhm.calculate(img, Array(source), verbose= true)

    val centroidAlgorithmSeq = MyConf.c.getStringSeq("SourceCentroid.fallbackAlgorithmSeq")
    Centroid.calculateCentroiWithFallBack(
      Array(source)
      , img
      , centroidAlgorithmSeq
      , verbose = true)

    //sourceSeq.head.prettyPrint()
    println( source.getCentroid())

  }
  //---------------------------------------------------------------------------
  def centroidComparision() = {


    val s = "/home/rafa/Downloads/deleteme/synthetize_five_source.fits"
    val img = MyImage(s)


    val sourceSeq = img.findSource(
      noiseTide = 10.194
      , sourceSizeRestriction = None//Some((4, 300000))
    ).toSegment2D_seq().sortWith(_.offset.y < _.offset.y)

    val biggerSource = sourceSeq.map( source=> (source,source.getPixCount())).sortWith(_._2 > _._2).head._1
    biggerSource.prettyPrint()
    Centroid.calculateCentroiWithFallBack(
      sourceSeq = Array(biggerSource)
      , img
      , List("cntrd")
      , verbose = true)
    biggerSource.prettyPrint()
    val expectedCentre = Point2D_Double(76.779314405549,729.523769373062)  //generated with synthetic image


    val m = biggerSource.toMatrix2D()

    val e  = Gaussian2D_Elliptical.fit(m)
    val er = Gaussian2D_EllipticalRotated.fit(m)
    val mf = Moffat2D_Elliptical.fit(m)
    val mfr= Moffat2D_EllipticalRotated.fit(m)
    val n  = Normal2D_Elliptical.fit(m)
    val nr = Normal2D_EllipticalRotated.fit(m)
    val bvn =  com.common.image.estimator.bellShape2D.bivariateNormal.BivariateNormal.fit(m)


    println(s"Synthetized position         : $expectedCentre")
    println(s"Gaussian2D_Elliptical        : ${e.get.centroid} -> ${expectedCentre.getDistance(e.get.centroid)}")
    println(s"Gaussian2D_EllipticalRotated : ${er.get.centroid} -> ${expectedCentre.getDistance(er.get.centroid)}")
    println(s"Moffat2D_Elliptical          : ${mf.get.centroid} -> ${expectedCentre.getDistance(mf.get.centroid)}")
    println(s"Moffat2D_EllipticalRotated   : ${mfr.get.centroid} -> ${expectedCentre.getDistance(mfr.get.centroid)}")
    println(s"Normal2D_Elliptical          : ${n.get.centroid} -> ${expectedCentre.getDistance(n.get.centroid)}")
    println(s"Normal2D_EllipticalRotated   : ${nr.get.centroid} -> ${expectedCentre.getDistance(nr.get.centroid)}")
    println(s"BivariateNormal              : ${bvn.get.centroid} -> ${expectedCentre.getDistance(bvn.get.centroid)}")
    println(s"m2(${biggerSource.getCentroidAlgorithm()})   : ${biggerSource.getCentroid()} -> ${expectedCentre.getDistance(biggerSource.getCentroid())}")

  }

  //---------------------------------------------------------------------------
  def save_image_source_tsv_and_png(): Unit = {

    val img = MyImage("/home/rafa/Downloads/deleteme/one_image/didymos.fits")
    val region = img.findSource(240.1, sourceSizeRestriction = Some(4,2000))
    val sourceSeq = Array(region.toSegment2D_seq().find( _.id == 54616).get)

    sourceSeq.head.prettyPrint()

    //calculate img fwhm
    Fwhm.calculate(img, sourceSeq, verbose = true)

    //calculate centroid and (ra,dec)
    Centroid.calculateCentroiWithFallBack(sourceSeq
      , img
      , MyConf.c.getStringSeq("SourceCentroid.fallbackAlgorithmSeq")
      , verbose = true)

    region.saveAsCsv("output/source.tsv")
    region.synthesizeImageWithColor("output/sources.png") //, font = Some(new Font(Font.MONOSPACED, Font.PLAIN, 10)))
  }

  //---------------------------------------------------------------------------
  def lombScargle(): Unit = {

    val minPeriodHours = 200
    val maxPeriodHours = 1500
    val frequencySamplesPerPeak = 32
    val timeCsvColName = "julian_date"
    val magCsvColName  = "estimated_absolute_magnitude"
    val sigmaScale = 2.5

    val csv = CsvRead.readCsv("/home/rafa/proyecto/m2/output/results/mpo_20000319_leona/2_photometry/absolute/light_curve/all_image_filters/by_catalog_filter/GAIA_DR3/JOHNSON_R/rotational_period/best_period_0/estimated_magnitude.csv")
    val timeSeq = csv.getRowSeq.map(row=> row.getDouble(timeCsvColName))
    val magnitudeSeq = csv.getRowSeq.map(row=> row.getDouble(magCsvColName))

    val pair = (magnitudeSeq zip timeSeq).sortWith(_._1 < _._1)
    val pairSigmaClipping = StatDescriptive.sigmaClippingDoubleWithPair(pair, sigmaScale = sigmaScale)
      .sortWith(_._2 < _._2)

    val r = LombScargle.calculate(
      tSeq = pairSigmaClipping map (_._2)  //secod magnitude
      , vSeq = pairSigmaClipping map (_._1)  //first time
      , freq_samples_per_peak = frequencySamplesPerPeak
      , freq_minimum_frequency = Some(24d / maxPeriodHours) //in days. min bestFreqInDays correspond to the max period
      , freq_maximum_frequency = Some(24d / minPeriodHours) //in days. max bestFreqInDays correspond to the min period
    )

    val frequencySeq = r.get._1
    val spectralPowerSeq = r.get._2
    val maxPowerIndex = spectralPowerSeq.indices.maxBy(spectralPowerSeq)
    val bestPeriodDays = 1d / frequencySeq(maxPowerIndex)
    val bestPeriodHours = bestPeriodDays * 24

    println(s"Best (freq_cycle_days,spectral_power): (${frequencySeq(maxPowerIndex)},${spectralPowerSeq(maxPowerIndex)})")
    println(s"Best period                          : $bestPeriodDays (days) single peak")
    println(s"Best period                          : $bestPeriodHours (hours) single peak")
    println(s"Best period                          : ${bestPeriodHours * 2} (hours) double peak")

    println(r)
  }

  //---------------------------------------------------------------------------
  def jplWeb() = {

    val objectName      = "2004 XA192"
    val normalizedName  = "mpo_20230965_2004_xa192"
    val orbit           = "JPL#19"
    val observatoryCode = "J86"
    val timeSeq         = "2023-10-26 01:11:16.17"

    println(JplWebQuery.getPos(
      objectName
      , normalizedName
      , orbit
      , observatoryCode
      , Array(timeSeq)
    ).head)
  }

  //---------------------------------------------------------------------------
  def fixFITS() = {
    //-------------------------------------------------------------------------
    val inDir = "/home/rafa/Downloads/work_with_users/nico/betelgeuse/laSagra/Betelgeuse-occ-2023-12-12_TETRA2/betelgeuse-occ/01_14_07"
    val outDir = "/home/rafa/Downloads/work_with_users/nico/betelgeuse/laSagra/Betelgeuse-occ-2023-12-12_TETRA2/om/"
    val imageSeq = Path.getSortedFileListRecursive(inDir,Seq(".fits"))

    val oDir = Path.resetDirectory(outDir)
    new MyParallelImageSeq(imageSeq.toArray.map(_.getAbsolutePath))
    //-------------------------------------------------------------------------
    class MyParallelImageSeq(seq: Array[String]) extends ParallelTask[String](
      seq
      , threadCount = CPU.getCoreCount()
      , isItemProcessingThreadSafe = true) {
      //-----------------------------------------------------------------------
      def userProcessSingleItem(imageName: String) =
        Try {
          fixBetelgeuseTetra2(imageName)
        }
        match {
          case Success(_) =>
          case Failure(e) =>
            error(e.getMessage + s" Error catalogging the image '$imageName' continuing with the next image")
            error(e.getStackTrace.mkString("\n"))
        }
      //-----------------------------------------------------------------------
    }
    def fixBetelgeuseTetra2(imageName: String) = {
      val img  = MyImage(imageName)
      val fits = img.getSimpleFits()
      val time = fits.getStringValueOrEmptyNoQuotationWithDefault("GPS_ST","").replace("Z","")
      val fractionSeconds = fits.getStringValueOrEmptyNoQuotationWithDefault("GPS_SU","")

      val seq = fractionSeconds.split("\\.")
      val numericPart = (seq(0) + (if (seq.length == 1) "" else seq(1)) ).toLong
      val formattedMicroseconds = f"$numericPart%07d"

      val newDate = time.split("\\.").head + "." + formattedMicroseconds
      fits.updateRecord(("DATE-OBS",s"'$newDate'"))
      img.saveAsFits(s"$oDir/${img.getRawName()}.fits"
                    , userFitsRecordSeq = fits.getHeaderRecordSeq().toArray)
    }
    //-------------------------------------------------------------------------
  }
  //---------------------------------------------------------------------------
  def generateEntrySeqForMassiveQuery(m2: M2) = {
    //sort the csv first using a calc sheet (as LibreCalc)
    val csv = CsvRead.readCsv("/home/rafa/Downloads/mpo_stats.csv", itemDivider = ",")
    val objectEntrySeq = csv.getRowSeq.map(row => row.getString("object"))
    val countEntrySeq = csv.getRowSeq.map(row => row.getLong("count"))

    Spice.init()
    objectEntrySeq
      .zipWithIndex
      .foreach  {  case (entry,i) =>

      val count =   countEntrySeq(i)
      val itemSeq = entry.drop(1)
                         .dropRight(1)
                         .split(":")

      val spkID = itemSeq(1).split(" ").head
      val objectName = itemSeq(3)
        .split("alternateName")
        .head
        .trim
        .toLowerCase
        .replaceAll("'", "")
        .replaceAll(" ", "_")

      //get vr
      val imageFocus = m2.getImageFocus(objectName, inputDir = "", isStar = false, blindSearch = false, verbose = false)

      if(imageFocus.isEmpty)
        println(s"************************ ERROR:'$objectName' with spkid:'$spkID' not found in SPK DB ************************")
      else {
        val queryVR = com.common.image.web.QueryVR(objectName, Some(imageFocus.get.asInstanceOf[ImageFocusMPO]), outputDir = "")
        val vr = queryVR.queryLcdbMostRecentEntry()

        val queryEntry = s""""$objectName,$vr,mpo_${spkID}_$objectName" , \\  #pos:'$i' count:'$count'"""

        println(queryEntry)
      }
    }
    Spice.close()
  }
  //---------------------------------------------------------------------------
  def downloadSPK(m2: M2) = {
    val mpcID_Seq = Seq(
      136472)


    val horizons = Horizons()
    val conf = MyConf(MyConf.c.getString("Database.smallBodyDB"))
    val startDate = LocalDate.parse(conf.getString("SmallBodyDB.httpRequest.startDate"))
    val endDate = LocalDate.parse(conf.getString("SmallBodyDB.httpRequest.endDate"))


    Horizons.downloadSPK("makemake"
      , version = "????"
      , 136472
      , startDate
      , endDate
      , "output/deleteme")


    val outputDir = Path.resetDirectory("output/spk_missing_2/")

    mpcID_Seq.foreach { mpcID =>

      val r = horizons.getSpkFileNameByMPC_ID(mpcID)

      if (r.isEmpty) println(s"error----->'$mpcID'")
      else {
        val seq = r.split("_")
        val spkid = seq.head.toLong
        val name = MyFile.removeFileExtension(seq.last)
        Horizons.downloadSPK(name
                            , version = "????"
                            , spkid
                            , startDate
                            , endDate
                            , outputDir)
      }
    }
  }
  //---------------------------------------------------------------------------
  def registration () = {

    val catalogImageName ="/home/rafa/proyecto/m2/output/fumo_000271/input_triplet/000000/science_2011_07_01T22_55_33_000_TNG_1050x1050_JOHNSON_R_300s_Quaoar_R_0020_lrs_QUAOAR.fits"
    val sampleImageName = "/home/rafa/proyecto/m2/output/fumo_000271/input_triplet/000000/science_2011_07_02T00_37_01_000_TNG_1050x1050_JOHNSON_R_300s_Quaoar_R_0018_lrs_QUAOAR.fits"

    val catalogImage = MyImage(catalogImageName)
    val sampleImage = MyImage(sampleImageName)

    val conf = MyConf(MyConf.c.getString("Registration.configurationFile"))
    val REGISTRATION_MIN_OVERLAPPING_PERCENTAGE = conf.getFloat("Registration.minOverlappingPercentage")
    val REGISTRATION_ASTROALIGN_PY_CALL = conf.getString("Registration.algorithm.astroAlignPy.call")

    val rAstroAlign = RegistrationByAstroAlignPy.calculate(
      sampleImageName
      , catalogImageName
      , REGISTRATION_ASTROALIGN_PY_CALL)

    val rClosestSource = RegistrationByClosestSource.calculate(
      OccImage(catalogImage)
      , OccImage(sampleImage))

    val rAstrometry = RegistrationByAstrometry.calculate(
        catalogImage
      , sampleImage)

    val rPolygonInvariant = RegistrationByPolygonInvariant.calculate(catalogImage
      , sampleImage
      , true)

    println()
  }

  //---------------------------------------------------------------------------
  def orbitIngegration() = {

    //val mpoDB = MpoDB()
    //val mpo = mpoDB.getByNumber(1).get
    //mpo.getSof()

    val stepSizeInDays = 16
    val mpcIdSeq =  Array(1L)
    OrbitIntegrator.run(LocalDateTime.parse("2014_11_01T00_00_00.000",Time.localDateTimeFormatterWithMillis)
                        , mpcIdSeq
                        , stepSizeInDays
                        , verbose = true)

    println()
  }

  //---------------------------------------------------------------------------
  def mpoLocation(m2: M2) = {

    /*
    val img = MyImage("/home/rafa/Downloads/deleteme/occ/chaos/very_small/q_e_20230329_1_1_1_1.fits")
    val mpoName = "chaos"

    val mpo = m2.getImageFocus(mpoName).get.asInstanceOf[ImageFocusMPO]

    Spice.init()
    val r = EstimatedSkyPosition.getPosAtImageWithFallBack(mpo, img, verbose = true)
    Spice.close()*/

    Spice.init()


    val mpoName = "salacia"
    val mpo = m2.getImageFocus(mpoName).get.asInstanceOf[ImageFocusMPO]

    val r = EstimatedSkyPosition.getPosWithFallBack(
        mpo
      , "493"
      , "399801"
      , "2004-01-19T21:34:22"
    )

    Spice.close()
  }

  //---------------------------------------------------------------------------
  def rotation() = {

    val img = MyImage("/home/rafa/Downloads/deleteme/test/science_2003_05_21T02_45_30_000_OSN_1_5_1024x1024_open_200s_quaoar-003cle_ccd_None.fits")
    val angle = img.getWcs().getRotationAngle()

    val (northIsUp,eastIsRight) = img.getOrientation()

    if (!northIsUp && !eastIsRight) {
      val newImg = img.rotation90(clockwise = false, invertY = true)
      newImg.saveAsFits("output/rotatedFlip.fits")
    }

    val newImg = img.rotation90(clockwise = true)
    newImg.saveAsFits("output/rotated.fits")
    println()
  }
  //---------------------------------------------------------------------------
  private def getFileNameSeq(in:String) = {
    Path.getSortedFileList(in, ".fits") map { p =>
      val name = p.getName
      (name.drop(8).take(23), p.getAbsolutePath)
    }
  }

  //---------------------------------------------------------------------------
  def fitsNotFitted() = {

    val nameSeq = getFileNameSeq("/mnt/uxmal_groups/spark/tmp/mpo_20050000_quaoar")
    nameSeq.foreach { case (_,path) =>
       val fits = MyImage(path).getSimpleFits()
       if (!fits.existKey("M2_WCS_F")) println(path)
    }

  }
  //---------------------------------------------------------------------------
  def fixFOV_pixScale() = {

    val year = 2016
    //val notSolvedSeq = getFileNameSeq(s"/mnt/uxmal_groups/image_storage/om_cal_oct_2024/om_cal_$year/wcs/not_solved")
    val notSolvedSeq = getFileNameSeq("/home/rafa/Downloads/deleteme/small_set_3/")

    val keyValueSeq = Seq(
        (KEY_OM_PIX_SCALE_X  , "0.6264")
      , (KEY_OM_PIX_SCALE_Y  , "0.6264")
      , (KEY_OM_FOV_X        , "21.3811")
      , (KEY_OM_FOV_Y        , "21.4646")
      , (KEY_CRPIX_1         , "1024.5")
      , (KEY_CRPIX_2         , "1028.5")
      , (KEY_CRVAL_1         , "274.668441469")
      , (KEY_CRVAL_2         , "-7.92123353143")
    )

    notSolvedSeq.zipWithIndex.foreach { case (notSolved, i) =>
      info(s"Fixing FITS on image ${i + 1} of ${notSolvedSeq.length}")
      val img = MyImage(notSolved._2)
      val fits = img.getSimpleFits()
      keyValueSeq.foreach { case (key,value) =>
        fits.updateRecord(key,value)
      }
      img.saveAsFits(img.name)
    }
  }
  //---------------------------------------------------------------------------
  def fixWCS_solving(m2: M2) = {
    //---------------------------------------------------------------------------
    val keySeq = Seq(
        KEY_OM_PIX_SCALE_X
      , KEY_OM_PIX_SCALE_Y
      , KEY_OM_FOV_X
      , KEY_OM_FOV_Y
      , KEY_OM_FOV_Y
      , KEY_CRPIX_1
      , KEY_CRPIX_2
      , KEY_CRVAL_1
      , KEY_CRVAL_2
      , KEY_CD_1_1
      , KEY_CD_1_2
      , KEY_CD_2_1
      , KEY_CD_2_2
    )

    val removeKeySeq = Seq(
        KEY_CROT_1
      , KEY_CROT_2
      , KEY_CDELT_1
      , KEY_CDELT_2
      , KEY_CD_1_1
      , KEY_CD_1_2
      , KEY_CD_2_1
      , KEY_CD_2_2
    )
    //---------------------------------------------------------------------------}
    def getImage(solvedSeq:List[(String,String)]
                 , notSolved:(String,String)
                 , prevImage: Boolean): Option[MyImage] = {

      info(s"Finding in solved images ${if(prevImage) "prev" else "post"} image of not solved image:'${MyFile.getFileNameNoPathNoExtension(notSolved._2)}' ")
      solvedSeq.zipWithIndex.foreach { case ((solvedTimeStamp,_),pos) =>
        if (solvedTimeStamp > notSolved._1)
          if (prevImage) return Some(MyImage(solvedSeq(pos - 1)._2))
          else return Some(MyImage(solvedSeq(pos)._2))
      }
      None
    }
    //---------------------------------------------------------------------------

    def updateWcsWithImage(scrImge: MyImage, destImage: MyImage) = {
      val srcFits = scrImge.getSimpleFits()
      val srcKeyValueMap = keySeq.map(key=> (key,srcFits.getStringValueOrEmptyNoQuotation(key)))

      val destImgFits = destImage.getSimpleFits()
      srcKeyValueMap.foreach { case (key,value) =>
        destImgFits.updateRecord(key,value)
      }
      destImage.saveAsFits(destImage.name)
    }
    //---------------------------------------------------------------------------
    def updateWcsWithObject(destImage: MyImage
                            , fixedObjectName: Option[String]) = {
      val destImgFits = destImage.getSimpleFits()
      val objectName = fixedObjectName.getOrElse(destImgFits.getStringValueOrDefault(KEY_OM_OBJECT,""))
      destImgFits.removeKeySeq(removeKeySeq)

      if (!objectName.isEmpty) {

        val focus = m2.getImageFocus(objectName)
        if (focus.isDefined) {

          val objectLocation = focus.get.asInstanceOf[ImageFocusMPO].getEstRaDecPosition(destImage)
          val pixMidPos = Point2D_Double(destImage.xMax, destImage.yMax) / 2d

          if (objectLocation.isDefined) {

            val pixIncrement = pixMidPos / objectLocation.get
            val finalObjectLocation = objectLocation.get

            info(s"Image:'${destImage.getRawName()}' CRPIX=$pixMidPos CRVAL=$finalObjectLocation")
            destImgFits.updateRecord(KEY_CRPIX_1, pixMidPos.x.toString)
            destImgFits.updateRecord(KEY_CRPIX_2, pixMidPos.y.toString)
            destImgFits.updateRecord(KEY_CRVAL_1, finalObjectLocation.x.toString)
            destImgFits.updateRecord(KEY_CRVAL_2, finalObjectLocation.y.toString)

            destImage.saveAsFits(destImage.name)
          }
        }
      }

    }
    //---------------------------------------------------------------------------
    def solve(solvedSeq: List[(String,String)] = List[(String,String)]()
             , notSolvedSeq: List[(String,String)] = List[(String,String)]()
             , tryType: Int = 2
             , fixedObjectName: Option[String] = Some("2002TC302")) = {

      info(s"Solved image count:'${solvedSeq.length}'")
      info(s"Not solved image count:'${notSolvedSeq.length}'")

      notSolvedSeq.zipWithIndex.foreach { case (notSolved,i) =>

        info(s"Try type: $tryType Not solved image ${i+1} of ${notSolvedSeq.length}")

        val destImg = MyImage(notSolved._2)

        tryType match {
          case 0 => //use parameters of prev image
            val srcImg = getImage(solvedSeq, notSolved, prevImage = true)
            if (srcImg.isDefined) {
              info(s"       Found solved image prev image                   :'${srcImg.get.getRawName()}'")
              updateWcsWithImage(srcImg.get, destImg)
            }

          case 1 => //use parameters of post image
            val srcImg = getImage(solvedSeq, notSolved, prevImage = false)
            if (srcImg.isDefined) {
              info(s"       Found solved image post  image                  :'${srcImg.get.getRawName()}'")
              updateWcsWithImage(srcImg.get, destImg)
            }

          //try to use pos by object
          case 2 =>
            updateWcsWithObject(destImg, fixedObjectName)
        }
      }
      println(s"Try $tryType ends")
    }
    //---------------------------------------------------------------------------

    Spice.init()

    //0 prev images
    //1 post images
    //2 location of object


/*
    //0 prev images
    //1 post images
    //2 location of object
    val tryType = 0  //set to 0 to use prev and post images
    val solvedSeq  = getFileNameSeq(s"/mnt/uxmal_groups/image_storage/om_cal/om_cal_2024/wcs")
    val notSolvedSeq = getFileNameSeq(s"/mnt/uxmal_groups/image_storage/om_cal/om_cal_2024/wcs/not_solved/")
    solve(solvedSeq
      , notSolvedSeq
      , tryType
      , fixedObjectName = None)
*/
/*
    //0 prev images
    //1 post images
    //2 location of object
    val tryType = 1  //set to 0 to use prev and post images
    val solvedSeq  = getFileNameSeq(s"/mnt/uxmal_groups/image_storage/om_cal/om_cal_2024/wcs")
    val notSolvedSeq = getFileNameSeq(s"/mnt/uxmal_groups/image_storage/om_cal/om_cal_2024/wcs/not_solved/")
    solve(solvedSeq
      , notSolvedSeq
      , tryType
      , fixedObjectName = None)

*/


    //0 prev images
    //1 post images
    //2 location of object
    val tryType = 2  //set to 0 to use prev and post images
    val solvedSeq  = List[(String,String)]()
    val notSolvedSeq = getFileNameSeq(s"/mnt/uxmal_groups/image_storage/om_cal/om_cal_2024/wcs/not_solved/")
    val fixedObjectName = Some("varuna")
    solve(solvedSeq
         , notSolvedSeq
         , tryType
         , fixedObjectName)




    Spice.close()
  }
  //---------------------------------------------------------------------------
  def normalize() = {
    val inDir = "/home/rafa/Downloads/deleteme/small_set_2/"
    val outDir = Path.resetDirectory("/home/rafa/Downloads/deleteme/small_set_2_normalized")
    Path.getSortedFileList(inDir,".fits").foreach { p=>

     // val img = MyImage(p.getAbsolutePath).normalize(2.5)
      var img = MyImage(p.getAbsolutePath).normalizeHistogram()

      for(i<-0 to 3)
        img = MyImage(p.getAbsolutePath).normalizeHistogram()

      img.saveAsFits(outDir + img.getRawName()  + ".fits")
    }
  }
  //---------------------------------------------------------------------------
  def synthetizeImage() = {
    val img = SyntheticImage(xMax = 256
                            , yMax = 1024
                            , backgroundValue = 10
                            , backgroundValuePercentageVariation = 2)

    img.addRandomGaussian2D_SourceSeq(
        sourceCount = 10
      , fitsFileName = "output/synthetize_ten_sources.fits"
      , tsvFileName  = "output/synthetize_ten_sources.tsv"
      , sigmaMultiplier = 4
      , pixStepAxisX = 0.5
      , pixStepAxisY = 0.5)

    /*
    img.addSequenceOfBivariateNormal(
        sourceCount = 1
      , fitsFileName = "output/synthetize_one_source.fits"
      , tsvFileName  = "output/synthetize_one_source.tsv"
      , sigmaMultiplier = 4
      , pixStepAxisX = 0.5
      , pixStepAxisY = 0.5
      , dataMultiplier = 10e4 )
     */
  }
  //---------------------------------------------------------------------------
  def mainTest(m2: M2): Unit = {

     fixWCS_solving(m2)

    //getEstPos(m2)
    //downloadSPK(m2)
    //WCS_conversion()
    //save_image_source_tsv_and_png()

    //centroid_check
    //centroidComparision()

    //WCS_conversion()
    //getEstPos(m2)
    //fitWcs(m2)

    //synthetizeImage
    //centroidComparision

    //normalize
    //rotation

  //  fitsNotFitted()

    //postgre()
//    getTelescopeXYZ
    //  mpoLocation(m2)



    //registration
//   generateEntrySeqForMassiveQuery(m2)

    //absolutePhotometrySingleFile(m2)



    // val noiseTide = 9390.20315
    //val f = "/home/rafa/Downloads/deleteme/single_file/a.fits"

    //val noiseTide =1502.0
    //val f = "/home/rafa/Downloads/deleteme/small_set/javalambre.fits"
    //val f = "/home/rafa/Downloads/javalambre_extended_6GiB.fits"
   // sourceDetectionClassic(f, noiseTide)
    //sourceDetectionComparision()



    //fixFITS()
    //subGAIA(m2)
    //getGroup(m2)
    //fitWcs(m2)



    //opencv
    //gnuAstro()


    //speed
    //jplWeb()
    //checkFile()
    //centroid()
    //sourceDetection()
    //sourceDetectionSimple()

    // sourceDetectionSubImage

    //lombScargle()

    //deleteFromAstrometry
    //getTelescopeXYZ
    // addMpoNameToCsv
    //    createLinkDirFromCsv



    //  opencv()

    //  imagePlot()

    // println()
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//=============================================================================
//End of file SandBox.scala
//=============================================================================



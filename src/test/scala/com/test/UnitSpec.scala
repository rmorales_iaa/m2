/**
 * Created by: Rafael Morales (rmorales@iaa.es)
 * Date:  28/mar/2018
 * Time:  11h:41m
 * Description: Unit testing for FITS parser.
 * The parser implements the rules of the grammar defined in appendix A of FITS standard version 4.0 (2017 Dec 1)
 */
//=============================================================================
package com.test

//=============================================================================
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.must.Matchers
import org.scalatest.{Inside, Inspectors, OptionValues}
//=============================================================================
//See http://www.scalatest.org/user_guide/defining_base_classes
abstract class UnitSpec extends AsyncFlatSpec
  with Matchers
  with OptionValues
  with Inside
  with Inspectors
//=============================================================================
//=============================================================================
//End of file UnitSpec.scala
//=============================================================================
//=============================================================================
package com.test.matrix
//=============================================================================
import com.common.geometry.matrix.matrix2D.{Matrix2D, Matrix2D_Kernel}
import com.common.geometry.point.Point2D
import com.test.UnitSpec
//=============================================================================
//=============================================================================
class Matrix2DConvolution_spec extends UnitSpec {
  //---------------------------------------------------------------------------
  "a matrix 1x1 " must "properly convolve with kernel 1x1"  in {

    val m = Matrix2D(1, 1, Array(2), offset = Point2D(10, 212)).invertAllRow
    m.prettyPrint()

    val k = Matrix2D_Kernel(1, 1, Array(-1d), 0 ,0).invertAllRow
    k.toMatrix2D.prettyPrint()

    k.convolve(m)
    assert(true)
  }
  //---------------------------------------------------------------------------
  "a matrix 3x3 " must "properly convolve with kernel 2x2" in {

    val m = Matrix2D(3, 3
      , Array(9,4,8,
              7,8,4,
              3,2,2)
      , offset = Point2D(10, 212)).invertAllRow
    m.prettyPrint()

    val k = Matrix2D_Kernel(2, 2, Array(-0.25d,-0.25d,-0.25d,-0.25d), 1, 0).invertAllRow
    k.toMatrix2D.prettyPrint()

    k.convolve(m)
    assert(true)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Matrix2D_Spec.scala
//=============================================================================
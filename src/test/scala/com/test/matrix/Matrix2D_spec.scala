//=============================================================================
package com.test.matrix
//=============================================================================
import com.common.geometry.matrix.matrix2D.Matrix2D
import com.common.geometry.point.Point2D
import com.common.geometry.segment.Segment1D
import com.common.geometry.segment.segment2D._
import com.test.UnitSpec
//=============================================================================
//=============================================================================
class Matrix2D_spec extends UnitSpec {

  //---------------------------------------------------------------------------
    "a source type -2" must "properly calculate fwhm" in {

      val m = Matrix2D(3, 1, Array(
          257, 299, 206
      ), offset = Point2D(10, 212)).invertAllRow


      val s = m.findSource(100).toSegment2D_seq().head
      s.prettyPrint()
      println("FWHM->" + s.calculateSimpleFwhmEstimation)
      assert(true)
    }

  //---------------------------------------------------------------------------
  "a source type -1" must "properly calculate fwhm" in {

    val m = Matrix2D(1, 3, Array(
        257
      , 299
      , 206
    ), offset = Point2D(10, 212)).invertAllRow


    val s = m.findSource(100).toSegment2D_seq().head
    s.prettyPrint()
    println("FWHM->" + s.calculateSimpleFwhmEstimation)
    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source type 0" must "properly calculate fwhm" in {

    val m = Matrix2D(4, 2, Array(
      0    , 257, 249, 999
      , 254, 350, 0,  355
    ), offset = Point2D(10, 212)).invertAllRow


    val s = m.findSource(100).toSegment2D_seq().head
    s.prettyPrint()
    println("FWHM->" + s.calculateSimpleFwhmEstimation)
    assert(true)
  }

  //---------------------------------------------------------------------------
  "a source type 1" must "properly calculate fwhm" in {

    val m = Matrix2D(1, 1, Array(
      313
    ), offset = Point2D(10, 212)).invertAllRow


    val s = m.findSource(100).toSegment2D_seq().head
    s.prettyPrint()
    println("FWHM->" + s.calculateSimpleFwhmEstimation)
    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source type 2" must "properly calculate fwhm" in {

    val m = Matrix2D(3, 3, Array(
        0,   257, 249
      , 254, 350, 0
    ), offset = Point2D(10, 212)).invertAllRow


    val s = m.findSource(100).toSegment2D_seq().head
    s.prettyPrint()
    println("FWHM->" + s.calculateSimpleFwhmEstimation)
    assert(true)
  }

  //---------------------------------------------------------------------------
  "a source " must "properly rot -270 v1" in {

    val m = Matrix2D(3, 3, Array(
        1, 2, 3
      , 4, 5, 6
      , 7, 8, 9
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    var newM = m.rotation270(clockwise = false)
    newM.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a source " must "properly rot -270 v2" in {

    val m = Matrix2D(2, 3, Array(
      1, 2
      , 4, 5
      , 7, 8
    ), offset = Point2D(10, 212)).invertAllRow

    m.prettyPrint()
    var newM = m.rotation270(clockwise = false)
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly rot 270 v1" in {

    val m = Matrix2D(3, 3, Array(
      1, 2, 3
      , 4, 5, 6
      , 7, 8, 9
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    var newM = m.rotation270(clockwise = true)
    newM.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a source " must "properly rot 270 v2" in {

    val m = Matrix2D(2, 3, Array(
      1, 2
      , 4, 5
      , 7, 8
    ), offset = Point2D(10, 212)).invertAllRow

    m.prettyPrint()
    var newM = m.rotation270(clockwise = true)
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly rot -180 v1" in {

    val m = Matrix2D(3, 3, Array(
      1, 2, 3
      , 4, 5, 6
      , 7, 8, 9
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    var newM = m.rotation180(clockwise = false)
    newM.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a source " must "properly rot -180 v2" in {

    val m = Matrix2D(2, 3, Array(
      1, 2
      , 4, 5
      , 7, 8
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    var newM = m.rotation180(clockwise = false)
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly rot 180 v1" in {

    val m = Matrix2D(3, 3, Array(
       1, 2, 3
      , 4, 5, 6
      , 7, 8, 9
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    val newM = m.rotation180(clockwise = true)
    newM.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a source " must "properly rot 180 v2" in {

    val m = Matrix2D(2, 3, Array(
      1, 2
      , 4, 5
      , 7, 8
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    var newM = m.rotation180(clockwise = true)
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly rot -90 v1" in {

    val m = Matrix2D(3, 3, Array(
      1, 2, 3
      , 4, 5, 6
      , 7, 8, 9
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    val newM = m.rotation90(clockwise = false)
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly rot -90 v2" in {

    val m = Matrix2D(2, 3, Array(
      1, 2
      , 4, 5
      , 7, 8
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    val newM = m.rotation90(clockwise = false)
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly rot 90 v1" in {

    val m = Matrix2D(3, 3, Array(
      1, 2, 3
      , 4, 5, 6
      , 7, 8, 9
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    val newM = m.rotation90(clockwise = true)
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly rot 90 v2" in {

    val m = Matrix2D(2, 3, Array(
        1, 2
      , 4, 5
      , 7, 8
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    val newM = m.rotation90(clockwise = true)
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly flip row v2" in {

    val m = Matrix2D(2, 3, Array(
       1, 2
      , 4, 5
      , 7, 8
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    val newM = m.flipRow()
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly flip row v1" in {

    val m = Matrix2D(3, 3, Array(
      1, 2, 3
      , 4, 5, 6
      , 7, 8, 9
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    val newM = m.flipRow()
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly flip col v2" in {

    val m = Matrix2D(2, 3, Array(
      1, 2
      , 4, 5
      , 7, 8
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    val newM = m.flipCol()
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly flip col v1" in {

    val m = Matrix2D(3, 3, Array(
       1, 2, 3
      , 4, 5, 6
      , 7, 8, 9
    ), offset = Point2D(10, 212)).invertAllRow


    m.prettyPrint()
    val newM = m.flipCol()
    newM.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly transpose v2" in {

    val m = Matrix2D(2, 3, Array(
        1, 2
      , 4, 5
      , 7, 8
    ), offset = Point2D(10, 212)).invertAllRow

    m.prettyPrint()
    val transposed = m.transpose()
    transposed.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a source " must "properly transpose v1" in {

    val m = Matrix2D(3, 3, Array(
       1,  2, 3
      ,4,  5, 6
      ,7  ,8 ,9
    ), offset = Point2D(10, 212)).invertAllRow

    m.prettyPrint()
    val transposed = m.transpose()
    transposed.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "two sources case 2" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(1, 1, Array(
      22
    ), offset = Point2D(3549, 787)).invertAllRow

    val mB = Matrix2D(1, 1, Array(
      55
    ), offset = Point2D(3548, 787)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    assert(its.isEmpty)

    val its2 = sB.getIntersection(sA, true)
    assert(its2.isEmpty)

    assert(true)
  }
  //---------------------------------------------------------------------------
  "two sources case 3" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(1, 1, Array(
      22
    ), offset = Point2D(3548, 787)).invertAllRow

    val mB = Matrix2D(1, 1, Array(
      55
    ), offset = Point2D(3548, 786)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    assert(its.isEmpty)

    val its2 = sB.getIntersection(sA, true)
    assert(its2.isEmpty)

    assert(true)
  }
  //---------------------------------------------------------------------------
  "two sources case 4" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(2, 2, Array(
      1, 2,
      3, 4,
    ), offset = Point2D(3548, 787)).invertAllRow

    val mB = Matrix2D(2, 2, Array(
      10, 20,
      30, 40,
    ), offset = Point2D(3548, 787)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "two sources case 5" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(2, 2, Array(
      1, 2,
      3, 4,
    ), offset = Point2D(20, 30)).invertAllRow

    val mB = Matrix2D(2, 2, Array(
      10, 20,
      30, 40,
    ), offset = Point2D(21, 30)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "two sources case 6" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(2, 2, Array(
      1, 2,
      3, 4,
    ), offset = Point2D(20, 30)).invertAllRow

    val mB = Matrix2D(2, 2, Array(
      10, 20,
      30, 40,
    ), offset = Point2D(20, 31)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "two sources case 7" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(2, 2, Array(
      1, 2,
      3, 4,
    ), offset = Point2D(20, 30)).invertAllRow

    val mB = Matrix2D(2, 2, Array(
      10, 20,
      30, 40,
    ), offset = Point2D(19, 30)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "two sources case 8" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(2, 2, Array(
      1, 2,
      3, 4,
    ), offset = Point2D(20, 30)).invertAllRow

    val mB = Matrix2D(2, 2, Array(
      10, 20,
      30, 40,
    ), offset = Point2D(21, 31)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "two sources case 9" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(2, 2, Array(
      1, 2,
      3, 4,
    ), offset = Point2D(20, 30)).invertAllRow

    val mB = Matrix2D(2, 2, Array(
      10, 20,
      30, 40,
    ), offset = Point2D(19, 29)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "two sources case 11" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(3, 3, Array(
      1, 2, 3,
      4, 5, 6,
      7, 8, 9,
    ), offset = Point2D(3548, 787)).invertAllRow

    val mB = Matrix2D(1, 1, Array(
      99,
    ), offset = Point2D(3549, 788)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)

  }
  //---------------------------------------------------------------------------
  "two sources with holes case 0" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(3, 3, Array(
      0, 25, 0,
      11, 0, 36,
      0, 23, 0,
    ), offset = Point2D(3548, 787)).invertAllRow

    val mB = Matrix2D(3, 3, Array(
      1, 2, 3,
      4, 5, 6,
      7, 8, 9,
    ), offset = Point2D(3548, 787)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "two sources with holes case 1" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(3, 3, Array(
      1, 2, 3,
      4, 0, 6,
      7, 8, 9,
    ), offset = Point2D(3548, 787)).invertAllRow

    val mB = Matrix2D(1, 1, Array(
      99,
    ), offset = Point2D(3549, 788)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    assert(its.isEmpty)

    val its2 = sB.getIntersection(sA, true)
    assert(its2.isEmpty)

    assert(true)
  }
  //---------------------------------------------------------------------------
  "two sources with holes case 2" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(4, 3, Array(
      1, 2, 3, 11,
      4, 0, 0, 12,
      7, 0, 0, 0,
    ), offset = Point2D(3548, 787)).invertAllRow

    val mB = Matrix2D(1, 1, Array(
      99,
    ), offset = Point2D(3511, 788)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    assert(its.isEmpty)

    val its2 = sB.getIntersection(sA, true)
    assert(its2.isEmpty)

    assert(true)
  }
  //---------------------------------------------------------------------------
  "two sources with holes case 3" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(4, 3, Array(
      1, 2, 3, 11,
      4, 0, 0, 12,
      7, 0, 0, 0,
    ), offset = Point2D(3548, 787)).invertAllRow

    val mB = Matrix2D(1, 2, Array(
      99,
      77
    ), offset = Point2D(3551, 788)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "two sources with holes case 4" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(4, 3, Array(
      1, 2, 3, 11,
      4, 0, 0, 12,
      7, 0, 0, 0,
    ), offset = Point2D(3548, 787)).invertAllRow

    val mB = Matrix2D(1, 2, Array(
      99,
      77
    ), offset = Point2D(3551, 787)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "two sources with holes case 5 with empty row" must "properly commandPhotometryDifferential its intersection" in {

    val mA = Matrix2D(4, 3, Array(
      1, 2, 3, 11,
      4, 0, 0, 0,
      7, 0, 0, 12,
    ), offset = Point2D(3548, 787)).invertAllRow

    val mB = Matrix2D(1, 3, Array(
      20,
      21,
      22,
    ), offset = Point2D(3551, 787)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get
    val sB = mB.toSegment2D_ByDetection().get

    sA.prettyPrint()
    sB.prettyPrint()


    val its = sA.getIntersection(sB, true)
    its.get.prettyPrint()

    val its2 = sB.getIntersection(sA, true)
    its2.get.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "one segments2D " must "commandPhotometryDifferential properly its centroid" in {

    val m = Matrix2D(2, 2, Array(
      4627, 4844,
      3767, 3533
    ), offset = Point2D(242, 184)).invertAllRow

    val s = Segment2D(Array(Array(Segment1D(0, 1, Array(4627, 4844)))
      , Array(Segment1D(0, 1, Array(3767, 3533))))
      , offset = Point2D(242, 184))

    s.printScalaCodeDefinition()
    val kk = s.getCentroid()


    val p = s.getCentroid()
    assert(true)

  }
  //---------------------------------------------------------------------------
  "two segments2D " must "commandPhotometryDifferential properly its intersection" in {

    val s_1 = Segment2D(Array(Array(Segment1D(2, 3, Array(2, 4)))
      , Array(Segment1D(1, 1, Array(1)))
      , Array(Segment1D(0, 1, Array(9, 7))
        , Segment1D(3, 3, Array(5)))
    )
      , offset = Point2D(0, 0))

    val s_2 = Segment2D(Array(Array(Segment1D(5, 5, Array(15)))
      , Array(Segment1D(4, 4, Array(12)), Segment1D(6, 6, Array(14)))
      , Array(Segment1D(4, 4, Array(10)))
    )
      , offset = Point2D(0, 0))

    s_1.prettyPrint()
    s_2.prettyPrint()
    //  s_1.getIntersection(s_2).get.prettyPrint()
    assert(true)
  }

  //---------------------------------------------------------------------------
  "two segments2D with offset" must "commandPhotometryDifferential properly its intersection" in {

    val s_1 = Segment2D(Array(Array(Segment1D(2, 3, Array(2, 4)))
      , Array(Segment1D(1, 1, Array(1)))
      , Array(Segment1D(0, 1, Array(9, 7)), Segment1D(3, 3, Array(5)))
    )
      , offset = Point2D(10, 20))

    val s_2 = Segment2D(Array(Array(Segment1D(5, 5, Array(15)))
      , Array(Segment1D(4, 4, Array(12)), Segment1D(6, 6, Array(14)))
      , Array(Segment1D(4, 4, Array(10)))
    )
      , offset = Point2D(10, 20))

    s_1.prettyPrint()
    s_2.prettyPrint()

    //  s_1.getIntersection(s_2).get.prettyPrint()
    // s_2.getIntersection(s_1).get.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "one source " must "properly find its border 4" in {

    val mA = Matrix2D(1, 1, Array(99), offset = Point2D(3548, 787)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get

    sA.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "one source " must "properly find its border 4-1" in {

    val mA = Matrix2D(2, 3, Array(
      9, 0,
      0, 7,
      3, 0,
    ), offset = Point2D(3548, 787)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get

    assert(true)
  }

  //---------------------------------------------------------------------------
  "one source " must "properly find its border 4-2" in {

    val mA = Matrix2D(1, 3, Array(
      1,
      7,
      1,
    ), offset = Point2D(3548, 787)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get

    sA.prettyPrint()

    assert(true)
  }


  //---------------------------------------------------------------------------
  "one source " must "properly find its border 4-3" in {

    val mA = Matrix2D(3, 1, Array(
      1, 0, 9
    ), offset = Point2D(3548, 787)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get

    sA.prettyPrint()

    assert(true)
  }

  //---------------------------------------------------------------------------
  "one source " must "properly find its border 4-4" in {

    val mA = Matrix2D(2, 3, Array(
      0, 1,
      7, 0,
      0, 1,
    ), offset = Point2D(3548, 787)).invertAllRow

    val sA = mA.toSegment2D_ByDetection().get

    sA.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Matrix2D_Spec.scala
//=============================================================================
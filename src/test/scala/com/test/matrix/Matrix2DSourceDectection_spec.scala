//=============================================================================
package com.test.matrix
//=============================================================================
import com.common.geometry.matrix.matrix2D.{Matrix2D, Matrix2DSourceDetectionByAxisPartition, Matrix2D_SourceDetection}
import com.common.geometry.point.Point2D
import com.common.image.myImage.MyImage
import com.test.UnitSpec
//=============================================================================
//=============================================================================
class Matrix2DSourceDectection_spec extends UnitSpec {
  /*
  //---------------------------------------------------------------------------
  "a matrix 1x1 no source" must "properly detect its sources" in {

    val m = Matrix2D(1, 1, Array(
      8
    ), offset = Point2D(10, 212)).invertAllRow

    m.prettyPrint()

    Matrix2D_SourceDetection(8).findSource(m).foreach { s =>
      s.prettyPrint()
    }
    assert(true)
  }
  //---------------------------------------------------------------------------
  "a matrix 1x1 " must "properly detect its sources" in {

    val m = Matrix2D(1, 1, Array(
        8
    ), offset = Point2D(10, 212)).invertAllRow

    m.prettyPrint()

    Matrix2D_SourceDetection(5).findSource(m).foreach{ s=>
      s.prettyPrint()
    }
    assert(true)
  }

  //---------------------------------------------------------------------------
  "a matrix 1x2" must "properly detect its sources" in {

    val m = Matrix2D(1,2, Array(
        5
      , 6
    ), offset = Point2D(10, 212)).invertAllRow

    m.prettyPrint()

    Matrix2D_SourceDetection( 5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a matrix 2x1" must "properly detect its sources" in {

    val m = Matrix2D(2, 1, Array(
        5, 6
    ), offset = Point2D(10, 212)).invertAllRow

    m.prettyPrint()

    Matrix2D_SourceDetection( 5).findSource(m).foreach { s =>
      s.prettyPrint()
    }
    assert(true)
  }

  //---------------------------------------------------------------------------
  "a matrix 2x2" must "properly detect its sources" in {

    val m = Matrix2D(2, 2
      , Array(9, 6,
              7, 8)
      , offset = Point2D(10, 212)).invertAllRow
    m.prettyPrint()

    Matrix2D_SourceDetection( 5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a matrix 3x3 v1" must "properly detect its sources" in {

    val m = Matrix2D(3, 3
      , Array(9, 4, 8,
              7, 8, 4,
              3, 2, 2)
      , offset = Point2D(10, 212)).invertAllRow
    m.prettyPrint()

    Matrix2D_SourceDetection( 5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a matrix 3x3 v2" must "properly detect its sources" in {

    val m = Matrix2D(3, 3
      , Array(7, 8, 9,
              6, 0, 7,
              7, 9, 9)
      , offset = Point2D(10, 212)).invertAllRow
    m.prettyPrint()

    Matrix2D_SourceDetection( 5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a matrix 3x3 v3" must "properly detect its sources" in {

    val m = Matrix2D(3, 3
      , Array(7, 8, 9,
              0, 0, 7,
              7, 9, 9)
      , offset = Point2D(10, 212)).invertAllRow
    m.prettyPrint()

    Matrix2D_SourceDetection( 5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a matrix 3x3 v4" must "properly detect its sources" in {

    val m = Matrix2D(3, 3
      , Array(7, 8, 9,
              7, 0, 0,
              7, 9, 9)
      , offset = Point2D(10, 212)).invertAllRow
    m.prettyPrint()

    Matrix2D_SourceDetection( 5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a matrix 3x3 v5" must "properly detect its sources" in {

    val m = Matrix2D(3, 3
      , Array(7, 0, 9,
              0, 0, 0,
              7, 9, 8)
      , offset = Point2D(10, 212)).invertAllRow
    m.prettyPrint()

    Matrix2D_SourceDetection( 5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a matrix 3x3 v6" must "properly detect its sources" in {

    val m = Matrix2D(3, 3
      , Array(0, 0, 9,
              0, 7, 0,
              0, 0, 9)
      , offset = Point2D(10, 212)).invertAllRow
    m.prettyPrint()

    Matrix2D_SourceDetection( 5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a matrix 3x4" must "properly detect its sources" in {
    val m = Matrix2D(4, 3
       , Array(8, 9, 0, 8,
               0, 8, 0, 0,
               6, 7, 9, 0)
      , offset = Point2D(10, 212)).invertAllRow
    m.prettyPrint()

    Matrix2D_SourceDetection( 5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a matrix big" must "properly detect its sources" in {
    val m = Matrix2D(22,15,Array(
      0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 60037,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      0,     0,     0,     0,     0, 60024, 60382, 60469, 60362, 60739, 61089, 60496, 60463, 60837, 60404, 60196,     0,     0,     0,     0,     0,     0,
      0,     0, 60713, 60908, 60849, 60629, 60765, 61051, 61423, 60910, 60531, 61033, 61053, 60307, 61525, 60775, 60708, 60422, 60173,     0,     0,     0,
      0, 60883, 61126, 60645, 61231, 60634, 61106, 60531, 61276, 61655, 61543, 60967, 61685, 61313, 61590, 61061, 61136, 60589, 60827, 60686,     0,     0,
      61148, 60600, 61161, 61245, 61156, 60751, 60914, 60885, 61245, 61739, 60938, 60367, 61685, 61453, 62333, 61342, 61872, 60590, 60680, 61281, 60227, 60365,
      60818, 61651, 60605, 61401, 61201, 61483, 61199, 60938, 60292, 60764, 60964, 62069, 61637, 60949, 61051, 61486, 61417, 61306, 60627, 60960, 61654, 60371,
      61539, 60972, 60807, 60453, 61028, 60734, 60753, 60659, 60821, 61078, 61286, 61779, 61835, 61666, 61276, 61500, 61405, 61741, 61783, 61266, 60509, 60146,
      61281, 60567, 60827, 60452, 61626, 60843, 61180, 60370, 61642, 61312, 61664, 61217, 61486, 61212, 61381, 61862, 61087, 61306, 60924, 60932, 60347,     0,
      0, 60486, 60322, 61012, 61225, 61098, 61510, 60813, 61177, 61003, 60817, 61341, 60791, 61116, 61481, 60956, 61266, 60479, 61100, 60621, 60073,     0,
      0,     0, 60773, 60881, 60850, 61211, 61141, 60997, 61054, 61471, 60576, 61279, 61518, 61448, 60457, 60992, 61184, 60674,     0,     0,     0,     0,
      0,     0,     0,     0,     0,     0, 60098,     0, 60790, 60578, 60656, 61615, 60382, 60934, 61246, 61721, 60091,     0,     0,     0,     0,     0,
      0,     0,     0,     0,     0,     0,     0,     0, 60948, 60964, 60711, 60192, 60655, 60898, 60195, 60644,     0,     0,     0,     0,     0,     0,
      0,     0,     0,     0,     0,     0,     0,     0,     0, 60436, 60877, 61106, 60644, 60879, 60227, 60090,     0,     0,     0,     0,     0,     0,
      0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 60089,     0, 60481, 60725,     0,     0,     0,     0,     0,     0,     0,     0,
      0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0,     0, 60180,     0,     0,     0,     0,     0,     0,     0,     0,     0
    ), offset = Point2D(1414,297)).invertAllRow
    m.prettyPrint()

    Matrix2D_SourceDetection(5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }*/
  /*
  //---------------------------------------------------------------------------
  "a matrix big 1" must "properly detect its sources" in {
    val m = Matrix2D(16,13,Array(
      0,     0,     0,     0,     0,     0,     0, 60954, 61024,     0,     0,     0,     0,     0,     0,     0,
      0,     0,     0,     0,     0, 62425, 62920, 62239, 62145, 62368, 60727,     0,     0,     0,     0,     0,
      0,     0, 61917, 62487, 62731, 62081, 62304, 63082, 62419, 62989, 62739, 61864, 61241, 60066,     0,     0,
      0, 62388, 62810, 63309, 63135, 62713, 63611, 63288, 63302, 63406, 63085, 63298, 63353, 62081, 60682,     0,
      0, 62997, 63443, 63529, 63561, 63130, 63166, 63134, 63756, 63539, 64327, 63830, 63559, 62965, 63051, 61411,
      62659, 63848, 63259, 63512, 63049, 63085, 64166, 63430, 63184, 63295, 63301, 63465, 63982, 62608, 63818, 60248,
      0, 62996, 62788, 62840, 63790, 63201, 63043, 63521, 63812, 63266, 62915, 63424, 62944, 63177, 61445, 60237,
      0,     0, 63029, 62214, 63462, 63042, 63230, 64377, 62774, 63278, 63507, 62739, 62680, 60586,     0,     0,
      0,     0,     0, 61875, 62346, 63042, 62364, 62480, 63063, 62869, 62838, 61492, 60652,     0,     0,     0,
      0,     0,     0,     0,     0, 62075, 62630, 62649, 62170, 62470, 62064, 60252,     0,     0,     0,     0,
      0,     0,     0,     0,     0,     0, 62692, 62037, 61875, 61463, 60786,     0,     0,     0,     0,     0,
      0,     0,     0,     0,     0,     0,     0, 62283, 62045, 61826, 60769,     0,     0,     0,     0,     0,
      0,     0,     0,     0,     0,     0,     0,     0, 61539, 60229,     0,     0,     0,     0,     0,     0
    ), offset = Point2D(75,15)).invertAllRow

    m.prettyPrint()

    Matrix2D_SourceDetection(5).findSource(m).foreach { s =>
      s.prettyPrint()
    }

    assert(true)
  }

  //---------------------------------------------------------------------------
  "a matrix big 2" must "properly detect its sources" in {
    val m = Matrix2D(16, 13, Array(
      0, 0, 0, 0, 0, 0, 0, 60954, 61024, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 62425, 62920, 62239, 62145, 62368, 60727, 0, 0, 0, 0, 0,
      0, 0, 61917, 62487, 62731, 62081, 62304, 63082, 62419, 62989, 62739, 61864, 61241, 60066, 0, 0,
      0, 62388, 62810, 63309, 63135, 62713, 63611, 63288, 63302, 63406, 63085, 63298, 63353, 62081, 60682, 0,
      0, 62997, 63443, 63529, 63561, 63130, 63166, 63134, 63756, 63539, 64327, 63830, 63559, 62965, 63051, 61411,
      62659, 63848, 63259, 63512, 63049, 63085, 64166, 63430, 63184, 63295, 63301, 63465, 63982, 62608, 63818, 60248,
      0, 62996, 62788, 62840, 63790, 63201, 63043, 63521, 63812, 63266, 62915, 63424, 62944, 63177, 61445, 60237,
      0, 0, 63029, 62214, 63462, 63042, 63230, 64377, 62774, 63278, 63507, 62739, 62680, 60586, 0, 0,
      0, 0, 0, 61875, 62346, 63042, 62364, 62480, 63063, 62869, 62838, 61492, 60652, 0, 0, 0,
      0, 0, 0, 0, 0, 62075, 62630, 62649, 62170, 62470, 62064, 60252, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 62692, 62037, 61875, 61463, 60786, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 62283, 62045, 61826, 60769, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 61539, 60229, 0, 0, 0, 0, 0, 0
    ), offset = Point2D(75, 15)).invertAllRow

    m.prettyPrint()

    val sourceSeq = Matrix2DSourceDetectionByAxisParttion.findSource(
      mainMatrix = m
      , partitionWidth = 1
      , partitionHeight = 1
      , noiseTide = 5)

    sourceSeq.foreach{ source=>
        source.prettyPrint()
    }
    assert(true)
  } */
  //---------------------------------------------------------------------------
  "a matrix big 3" must "properly detect its sources" in {

    val img = MyImage("/home/rafa/Downloads/deleteme/single_file/science_2023_02_28T23_14_28_379_LIVERPOOL_2048x2056_sdss_r_300s_h_e_20230228_34_4_1_1_io_2013XC26.fits")

    val sourceSeq = Matrix2DSourceDetectionByAxisPartition.findSource(
       img
      , partitionWidth = 256
      , partitionHeight = 256
      , noiseTide = 3700)

    sourceSeq.foreach { source =>
      source.prettyPrint()
    }


    assert(true)
  }
  //---------------------------------------------------------------------------

}

//=============================================================================
//End of file Matrix2DSourceDectection_spec.scala
//=============================================================================
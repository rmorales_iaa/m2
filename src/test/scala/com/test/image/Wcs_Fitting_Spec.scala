/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.image
//=============================================================================
import com.common.configuration.MyConf
import com.common.fits.wcs.WCS
import com.common.fits.wcs.fit.algorithm.astrometry_net.Astrometry_net
import com.common.fits.wcs.fit.algorithm.lsst.Lsst
import com.common.fits.wcs.fit.algorithm.lsst_orig.LsstOrig
import com.common.fits.wcs.fit.algorithm.moolekamp.Moolekamp
import com.common.fits.wcs.fit.algorithm.nonStandard.polynomy.GeneralPolyAlgorithm
import com.common.geometry.point.Point2D_Double
import com.common.image.myImage.MyImage
import com.common.stat.{SimpleStatDescriptive, StatDescriptive}
import com.test.UnitSpec
//=============================================================================
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.io.{BufferedReader, File, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
import scala.collection.mutable.ArrayBuffer
//=============================================================================
object Wcs_Fitting_Spec {
  //---------------------------------------------------------------------------
  private val sipOrder = 4
  //---------------------------------------------------------------------------
  private val fitsName = "/home/rafa/Downloads/deleteme/lsst/a.fits"
  private val csvName = "/home/rafa/Downloads/deleteme/lsst/b.csv"
  //private val fitsName = "/home/rafa/Downloads/deleteme/lsst2/a.fits"
  //private val csvName = "/home/rafa/Downloads/deleteme/lsst2/b.csv"

  //---------------------------------------------------------------------------
  private val (imagePixPosSeq, catalogPosRaDecSeq) = loadImageAndCsv(csvName)

  private var baseRaResidualStats: SimpleStatDescriptive = null
  private var baseDecResidualStats: SimpleStatDescriptive = null
  private var basePixX_ResidualStats: SimpleStatDescriptive = null
  private var basePixY_ResidualStats: SimpleStatDescriptive = null
  //---------------------------------------------------------------------------
  getStats(imagePixPosSeq
          , catalogPosRaDecSeq
          , MyImage(fitsName).getWcs()
          , "Base stats"
          , firstStats = true)
  //---------------------------------------------------------------------------
  private def loadImageAndCsv(csvName: String) = {

    //load positions
    val imagePixPosSeq = ArrayBuffer[Point2D_Double]()
    val catalogPosRaDecSeq = ArrayBuffer[Point2D_Double]()

    val inputFormat = CSVFormat.DEFAULT.withSkipHeaderRecord().withDelimiter('\t')
    val br = new BufferedReader(new FileReader(new File(csvName)))
    new CSVParser(br, inputFormat).getRecords.asScala.foreach { case r =>
      if (r.getRecordNumber != 1) {

        imagePixPosSeq += Point2D_Double(r.get(1).toDouble, r.get(2).toDouble)  //offset (1,1) yet applied
        val catalogRaDec = Point2D_Double(r.get(11).toDouble, r.get(12).toDouble)
        catalogPosRaDecSeq += catalogRaDec //(ra,dec) pm
      }
    }

    (  imagePixPosSeq.toArray
      , catalogPosRaDecSeq.toArray)
  }
  //---------------------------------------------------------------------------

  def getStats(imagePixPosSeq: Array[Point2D_Double]
               , catalogPosRaDecSeq: Array[Point2D_Double]
               , wcs: WCS
               , suffix: String  = ""
               , firstStats: Boolean) = {

    //ra dec stat
    val sourceRaDec =  imagePixPosSeq map (pix=> wcs.pixToSky( pix ))
    val raDecResidual = (sourceRaDec zip catalogPosRaDecSeq) map { case (source, catalog) => catalog.getResidualMas(source) }

    val raResidualStats = StatDescriptive.getWithDouble(raDecResidual map (_.x))
    val decResidualStats = StatDescriptive.getWithDouble(raDecResidual map (_.y))

    //pix stats
    val pixResidual = (imagePixPosSeq zip catalogPosRaDecSeq) map { case (source, catalog) =>
      val estPos = wcs.skyToPix(catalog)
      estPos - source
    }
    val xPixStats = StatDescriptive.getWithDouble(pixResidual map (_.x))
    val yPixStats = StatDescriptive.getWithDouble(pixResidual map (_.y))

    if (firstStats) {
      println(s"####### Residual (ra,dec) mas $suffix #############")
      println(s"------- Residual RA mas $suffix -----------")
      println("avera mas: " + raResidualStats.average)
      println("stdev mas: " + raResidualStats.stdDev)
      println(s"------- Residual DEC mas $suffix -----------")
      println("avera mas: " + decResidualStats.average)
      println("stdev mas: " + decResidualStats.stdDev)
      println("##############################################")
      println(s"####### Residual (x,y) pix $suffix #############")
      println(s"------- Residual X pix $suffix -----------")
      println("avera pix: " + xPixStats.average)
      println("stdev pix: " + xPixStats.stdDev)
      println(s"------- Residual Y pix $suffix  -----------")
      println("avera pix: " + yPixStats.average)
      println("stdev pix: " + yPixStats.stdDev)
      println("###########################################")

      baseRaResidualStats    = raResidualStats
      baseDecResidualStats   = decResidualStats
      basePixX_ResidualStats = xPixStats
      basePixY_ResidualStats = yPixStats
    }
    else {
      println("diff ra avg mas: " + (raResidualStats.average - baseRaResidualStats.average))
      println("diff dec avg mas: " + (decResidualStats.average - baseDecResidualStats.average))
      println("diff x avg pix: " + (xPixStats.average  - basePixX_ResidualStats.average))
      println("diff y avg pix: " + (yPixStats.average  - basePixY_ResidualStats.average))
    }
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
import Wcs_Fitting_Spec._
class Wcs_Fitting_Spec extends UnitSpec {

  //-------------------------------------------------------------------------
  "a WCS " must "be fitted with LSST Scala own scala implementation" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //---------------------------------------------------------------------------
    //load image
    val img = MyImage(fitsName)
    var wcs = img.getWcs()
    val algorithm = "LSST scala"

    for (i <- 0 until 1) {
      println(s"--------------- Iteration $i starts for '$algorithm' -------------------------------------------------")
      val newWcs = Lsst(
        img
        , catalogPosRaDecSeq
        , imagePixPosSeq
        , sipOrder).run()

      img.setWcs(newWcs.get)
      wcs = newWcs.get

      wcs.pretty_print()

      getStats(imagePixPosSeq
        , catalogPosRaDecSeq
        , newWcs.get
        , "WCS fitted"
        , firstStats = false)
      println(s"--------------- Iteration $i ends for '$algorithm' -------------------------------------------------")
    }

    assert(true)
  }
  //-------------------------------------------------------------------------
  "a WCS " must "be fitted with Moolekamp" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //---------------------------------------------------------------------------
    //load image
    val img = MyImage(fitsName)
    var wcs = img.getWcs()
    val algorithm = "Moolekamp"

    for (i <- 0 until 1) {
      println(s"--------------- Iteration $i starts for '$algorithm' -------------------------------------------------")
      val newWcs = Moolekamp(
        img
        , catalogPosRaDecSeq
        , imagePixPosSeq
        , wcs.sip.aOrder).run()

      img.setWcs(newWcs.get)
      wcs = newWcs.get

      getStats(imagePixPosSeq
        , catalogPosRaDecSeq
        , newWcs.get
        , "WCS fitted"
        , firstStats = false)
      println(s"--------------- Iteration $i ends for '$algorithm' -------------------------------------------------")
    }

    assert(true)
  }

  //-------------------------------------------------------------------------
  "a WCS " must "be fitted with Astronomy.net" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //---------------------------------------------------------------------------
    //load image
    val img = MyImage(fitsName)
    var wcs = img.getWcs()
    val algorithm = "Astronomy.net"

    for (i <- 0 until 1) {
      println(s"--------------- Iteration $i starts for '$algorithm' -------------------------------------------------")
      val newWcs = Astrometry_net(
        img
        , catalogPosRaDecSeq
        , imagePixPosSeq
        , sipOrder).run()

      img.setWcs(newWcs.get)
      wcs = newWcs.get

      getStats(imagePixPosSeq
        , catalogPosRaDecSeq
        , newWcs.get
        , "WCS fitted"
        , firstStats = false)
      println(s"--------------- Iteration $i ends for '$algorithm' -------------------------------------------------")
    }

    assert(true)
  }

  //-------------------------------------------------------------------------
  "a WCS " must "be fitted with LSST CC " in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //---------------------------------------------------------------------------
    //load image
    val img = MyImage(fitsName)
    var wcs = img.getWcs()
    val algorithm = "LSST CC"

    for (i <- 0 until 1) {
      println(s"--------------- Iteration $i starts for '$algorithm' -------------------------------------------------")
      val newWcs = LsstOrig(
        img
        , catalogPosRaDecSeq
        , imagePixPosSeq
        , sipOrder).run()

      img.setWcs(newWcs.get)
      wcs = newWcs.get

      getStats(imagePixPosSeq
        , catalogPosRaDecSeq
        , newWcs.get
        , "WCS fitted"
        , firstStats = false)
      println(s"--------------- Iteration $i ends for '$algorithm' -------------------------------------------------")
    }

    assert(true)
  }

  //-------------------------------------------------------------------------

  "a WCS " must "be fitted with Polynomy" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //---------------------------------------------------------------------------
    //load image
    val img = MyImage(fitsName)
    var wcs = img.getWcs()
    val algorithm = "Polynomy"

    for (i <- 0 until 1) {
      println(s"--------------- Iteration $i starts for '$algorithm' -------------------------------------------------")
      val newWcs = GeneralPolyAlgorithm(
        img
        , catalogPosRaDecSeq
        , imagePixPosSeq
        , sipOrder = 3 //fixed
        , residualPolynomialOrder = 3).run()
      img.setWcs(newWcs.get)
      wcs = newWcs.get

      getStats(imagePixPosSeq
               , catalogPosRaDecSeq
               , newWcs.get
               , "WCS fitted"
               , firstStats = false)
      println(s"--------------- Iteration $i ends for '$algorithm' -------------------------------------------------")
    }

    assert(true)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Wcs_Fitting_Spec.scala
//=============================================================================

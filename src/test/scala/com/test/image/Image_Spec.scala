/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.image
//=============================================================================
import com.common.configuration.MyConf
import com.common.geometry.point.{Point2D}
import com.common.image.myImage.MyImage
import com.test.UnitSpec
//=============================================================================
//=============================================================================
class Image_Spec extends UnitSpec {
  //---------------------------------------------------------------------------
  MyConf.c = MyConf(verbose = false)
  //---------------------------------------------------------------------------
  "an image" must "extract properly a source" in {

    val imgeName =  "/home/rafa/Downloads/work_on_users/flavia/group_by_telescope/liverpool/science_h_e_20150821_10_2_1_1_LIVERPOOL_2015_08_21T21_43_17_621_sdss_r_300s_2048x2056_io_2002MS4.fits"

    val img = MyImage(imgeName)
    val noiseTide = 3044
    assert(true)
  }
  //---------------------------------------------------------------------------
  "an image" must "commandPhotometryDifferential its background " in {

    val imgeName =  "/home/rafa/Downloads/work_on_users/flavia/group_by_telescope/vlt/science_FORS2_2013-06-02T05_28_44_715_CHIP1_VLT-U1_SCIENCE_VLT_2013_06_02T05_28_44_715_JOHNSON_V_70s_2048x1034_fors2_chip_1_2002MS4.fits"

    val img = MyImage(imgeName)
    val noiseTide = 2065
    //val subImage = img.getSubMatrix(Point2D(1496,504), Point2D(1522,532))
    val subImage = img.getSubMatrix(Point2D(1484,492), Point2D(1489,496))


    assert(true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Image_Spec.scala
//=============================================================================

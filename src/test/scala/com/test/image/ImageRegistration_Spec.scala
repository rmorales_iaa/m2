/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.image

//=============================================================================
import com.common.configuration.MyConf
import com.common.image.registration.RegistrationByPolygonInvariant
import com.test.UnitSpec
//=============================================================================
//=============================================================================
object ImageRegistration_Spec {
  //---------------------------------------------------------------------------
}
//=============================================================================
import com.test.image.ImageRegistration_Spec._
class ImageRegistration_Spec extends UnitSpec {
/*
  //-------------------------------------------------------------------------
  "a directory with FITS images " must "be registered" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //---------------------------------------------------------------------------
    val inputDir = "/home/rafa/Downloads/deleteme/small_Set_2/"
   // val inputDir = "/home/rafa/Downloads/deleteme/small_Set_3/"
    val outputDir = "output/registration"
    RegistrationByPolygonInvariant.registerAllImagedOfDirectory(inputDir,outputDir)
    assert(true)
  }
*/
  //-------------------------------------------------------------------------
  "a directory with FITS images " must "be stacked" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //---------------------------------------------------------------------------
    val inputDir = "output/registration"
    val outputFile = "output/stacked.fits"

    assert(true)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file ImageRegistration_Spec.scala
//=============================================================================

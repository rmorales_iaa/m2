/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.image
//=============================================================================
import com.common.configuration.MyConf
import com.common.dataType.conversion.DataTypeConversion
import com.common.stat.StatDescriptive
import com.test.UnitSpec
//=============================================================================
import java.nio.{ByteBuffer, ByteOrder}
import scala.util.Random
import scala.collection.mutable.ArrayBuffer
//=============================================================================
//=============================================================================
object DataType_Spec {
  //--------------------------------------------------------------------------
  def generateData() = {
    val arraySize = 10000000 * 4 // Size of the array
    val minValue = 10 - 8 // Minimum value of the random doubles
    val maxValue = 10 + 8 // Maximum value of the random doubles

    val random = new Random()
    Array.fill(arraySize)(minValue + random.nextDouble() * (maxValue - minValue))
  }
  //---------------------------------------------------------------------------
  def byteSeqToDoubleSeq(byteSeq: Array[Byte]): Seq[Double] = {
    val buffer = ByteBuffer.wrap(byteSeq)
      .order(ByteOrder.BIG_ENDIAN)
      .asDoubleBuffer()
    val s = new Array[Double](buffer.remaining())
    buffer.get(s)
    s.toSeq
  }
    //---------------------------------------------------------------------------
  def doubleSeqToByteSeq(doubles: Seq[Double]): Array[Byte] = {
      val buffer = ByteBuffer.allocate(doubles.length * 8) // 8 bytes per double
      doubles.foreach(buffer.putDouble)
      buffer.flip() // Prepare the buffer for reading
      val bytes = new Array[Byte](buffer.remaining())
      buffer.get(bytes)
      bytes
    }
  //---------------------------------------------------------------------------
}
//=============================================================================
import DataType_Spec._
class DataType_Spec extends UnitSpec {
  //---------------------------------------------------------------------------
  MyConf.c = MyConf(verbose = false)
  //---------------------------------------------------------------------------
  "an data type conversion" must "be ast fast as possible" in {

    val k = 100
    val elapsedTimeJava = ArrayBuffer[Double]()
    val elapsedTimeC = ArrayBuffer[Double]()

    for(i<-0 until k) {
      val dataDouble = generateData()

      println(s"-------Iteration $i-------")
      var startTime = System.nanoTime()
      val byteSeq = doubleSeqToByteSeq(dataDouble)
      var newDataDouble = byteSeqToDoubleSeq(byteSeq)
      var endTime = System.nanoTime()
      var elapsedTime = (endTime - startTime) / 1e6
      elapsedTimeJava += elapsedTime
      println(s"JAVA Execution time: $elapsedTime milliseconds")

      startTime = System.nanoTime()
      val _byteSeq = DataTypeConversion.doubleSeqToByteSeq(dataDouble)
      newDataDouble = DataTypeConversion.byteSeqToDoubleSeq(_byteSeq)
      endTime = System.nanoTime()
      elapsedTime = (endTime - startTime) / 1e6
      elapsedTimeC += elapsedTime
      println(s"C Execution time: $elapsedTime milliseconds")
      println(s"------------------------")
    }
    println("..........")
    println("Median java:" + StatDescriptive.getWithDouble(elapsedTimeJava.toArray).median)
    println("Max    java:" + StatDescriptive.getWithDouble(elapsedTimeJava.toArray).max)
    println("..........")
    println("Median C   :" + StatDescriptive.getWithDouble(elapsedTimeC.toArray).median)
    println("Max    C   :" + StatDescriptive.getWithDouble(elapsedTimeC.toArray).max)
    println("..........")

    assert(true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file DataType_Spec.scala
//=============================================================================

/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.image
//=============================================================================
import com.common.configuration.MyConf
import com.common.geometry.point.{Point2D_Double}
import com.common.timeSeries.LombScargle.LombScargle
import com.test.UnitSpec
//=============================================================================
import org.apache.commons.csv.{CSVFormat, CSVParser}
import java.io.{BufferedReader, FileReader}
import scala.collection.JavaConverters.asScalaBufferConverter
//=============================================================================
//=============================================================================
class Lomb_Scargle_Spec extends UnitSpec {
  //---------------------------------------------------------------------------
  MyConf.c = MyConf(verbose = false)
  //---------------------------------------------------------------------------
  "an csv with freq and magnitude" must "calculate it best harmonic" in {

    //---------------------------------------------------------------------------
    val COLUMN_NAME = Seq("julian_date","R_filter_diff_mag_corrected(r,delta,alfa)")
    val csvInputFormat = CSVFormat.DEFAULT
      .withDelimiter('\t')
      .withHeader(COLUMN_NAME:_*)
      .withSkipHeaderRecord()
      .withIgnoreEmptyLines(true)

    val csvFile = "/home/rafa/proyecto/m2/deploy/output/photometry/differential/merged_seasons/136472_makemake/all/time_diff_magnitude.csv"

    val minPeriodHours:Double = 5
    val maxPeriodHours:Double = 30
    val frequencySamplesPerPeak = 64

    val xySeq =
      new CSVParser(new BufferedReader(new FileReader(csvFile)), csvInputFormat).getRecords.asScala.zipWithIndex.map { case (r, _) =>
      Point2D_Double(r.get(COLUMN_NAME.head).toDouble
                    , r.get(COLUMN_NAME.last).toDouble)
    }.toArray.sortWith(_.x < _.x)


    val r = LombScargle.calculate(
        xySeq map (_.x)
      , xySeq map (_.y)
      , freq_samples_per_peak = frequencySamplesPerPeak
      , freq_minimum_frequency = Some(24d / maxPeriodHours)  //in days. min bestFreqInDays correspond to the max period
      , freq_maximum_frequency = Some(24d / minPeriodHours)  //in days. max bestFreqInDays correspond to the min period
    )

    val frequencySeq = r.get._1
    val spectralPowerSeq = r.get._2
    val maxPowerIndex = spectralPowerSeq.indices.maxBy(spectralPowerSeq)
    val bestPeriodDays = 1d / frequencySeq(maxPowerIndex)
    val bestPeriodHours = bestPeriodDays * 24

    println(s"Best (freq_cycle_days,spectral_power): (${frequencySeq(maxPowerIndex)},${spectralPowerSeq(maxPowerIndex)})")
    println(s"Best period                          : $bestPeriodDays (days) single peak")
    println(s"Best period                          : $bestPeriodHours (hours) single peak")
    println(s"Best period                          : ${bestPeriodHours * 2} (hours) double peak")

    assert(true)
  }
  //---------------------------------------------------------------------------
  "an image" must "commandPhotometryDifferential its background " in {

    val imgeName =  "/home/rafa/Downloads/work_on_users/flavia/group_by_telescope/vlt/science_FORS2_2013-06-02T05_28_44_715_CHIP1_VLT-U1_SCIENCE_VLT_2013_06_02T05_28_44_715_JOHNSON_V_70s_2048x1034_fors2_chip_1_2002MS4.fits"


    assert(true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Lomb_Scargle_Spec.scala
//=============================================================================

/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.image
//=============================================================================
import com.common.configuration.MyConf
import com.common.image.myImage.MyImage
import com.common.image.occultation.OccConfiguration
import com.common.image.occultation.occImage.OccImageAperturePhotometry
import com.test.UnitSpec
//=============================================================================
//=============================================================================
class Aperture_Spec extends UnitSpec {
  //---------------------------------------------------------------------------
  MyConf.c = MyConf(verbose = false)
  //---------------------------------------------------------------------------
  "an image" must "calcualte properly its aperture" in {

    val img = MyImage("/home/rafa/Downloads/work_with_users/nico/oc/2014HE200/2014HE200-ash/no_dark/2014HE200-occ-001.fit")
    OccImageAperturePhotometry.calculateAperturePhotometry(
       img
       , outputDir = "/home/rafa/Downloads/deleteme/bin"
       , OccConfiguration(Some("input/occultation/objects/2014_he200/ash/ash.conf"))
       , verbose = true)

    assert(true)
  }
  //---------------------------------------------------------------------------
}
//=============================================================================
//End of file Aperture_Spec.scala
//=============================================================================

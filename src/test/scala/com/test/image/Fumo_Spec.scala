/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.image

//=============================================================================
import com.common.configuration.MyConf
import com.common.image.focusType.ImageFocusMPO
import com.common.image.fumo.Fumo
import com.m2.M2
import com.test.UnitSpec
//=============================================================================
//=============================================================================
class Fumo_Spec extends UnitSpec {

  //-------------------------------------------------------------------------

  "a directory with FITS images " must "detect the source movement" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //---------------------------------------------------------------------------

    //val inputDir = "/home/rafa/proyecto/m2/deploy/images/mpo_20050000_quaoar/"
    val inputDir = "/home/rafa/Downloads/deleteme/small_set_3/"
    val saveDiscardedTrajectories = true
    //val inputDir = "/home/rafa/Downloads/deleteme/small_set_4/"
//    val inputDir = "/home/rafa/Downloads/deleteme/small_set_5/"

    val minElapsedMinutesBetweenImages = 5

    Fumo.calculate(inputDir
                   , outputDir = "output/fumo"
                   , minElapsedMinutesBetweenImages
                   , stepSeq = Array(1)
    //               , stepSeq = Array(1,2,3)
    )

    assert(true)
  }

   /*
  //-------------------------------------------------------------------------
  "a directory with fumo results" must "recalculate the results for an specif triplet" in {
    //-------------------------------------------------------------------------
    MyConf.c = MyConf()
    //---------------------------------------------------------------------------

    val rootPath = "/home/rafa/proyecto/m2/output/fumo/"
    val tripletID  = 271
    val mpo = "quaoar"

    val mpoToIgnore = Some(ImageFocusMPO.build(mpo, null).get)


    Fumo.recalculateTripletResults(rootPath, tripletID, mpoToIgnore)

    assert(true)
  }*/
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file Fumo_Spec.scala
//=============================================================================

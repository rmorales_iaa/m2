/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.binatyTree
//=============================================================================
import com.common.dataType.tree.kdTree.{K2d_Tree, K2d_TreeFloat}
import com.common.geometry.point.{Point2D, Point2D_Float}
import com.test.UnitSpec
//=============================================================================
class Kd_Tree_Spec extends UnitSpec {
  //---------------------------------------------------------------------------
  "a kdtree float " must "must find its neighbours"  in {

    val seq = Array(
      (Point2D_Float(66.59628,218.85681).toSequence(), 1)
      , (Point2D_Float(131.24283,117.18356).toSequence(), 2)
      , (Point2D_Float(144.28996,84.218925).toSequence(), 3)
      , (Point2D_Float(138.63417,178.826).toSequence(), 4)
      , (Point2D_Float(97.43573,13.768242).toSequence(), 5)
    )

    val kd_tree = K2d_TreeFloat()
    kd_tree.addSeq(seq)
    kd_tree.prettyPrint()

    //kd_tree.debug("output/tree_plot")

    val r = kd_tree.getKNN( Point2D_Float(66.59628,218.85681).toSequence, 5)
    val s = (r map(_.data)).mkString("\n")
    println(s)

    assert(true)
  }
  //---------------------------------------------------------------------------
  "a kdtree int " must "must find its neighbours"  in {
    val seq = Array((Point2D(0,1).toSequence,1), (Point2D(1,0).toSequence,2) , (Point2D(1,1).toSequence,3))
    val kd_tree = K2d_Tree()
    kd_tree.addSeq(seq)
    kd_tree.prettyPrint()

    val r = kd_tree.getKNN( Point2D(0,0).toSequence, 3, keepAllWithMinDistanceIgnoringK = true)
   assert(true)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file TreeMap_Spec.scala
//=============================================================================

/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.binatyTree
//=============================================================================
import com.test.UnitSpec
import scala.collection.immutable.TreeMap
//=============================================================================
class TreeMap_Spec extends UnitSpec {
  //---------------------------------------------------------------------------
  "Immutable treeMap with 4 elements {5,7,8,1,0,3,2} " must "report properly a search by range"  in {

    val map = TreeMap(
      5 -> "5",
      7 -> "7",
      8 -> "8",
      1 -> "1",
      0 -> "0",
      3 -> "3",
      2 -> "2"
    )
    val r = map.range(-1,1)
    r.foreach( p=> println(p._2))
    assert(r.size==1)
    assert(r(0) == "0")
  }
  //---------------------------------------------------------------------------
  "Mutable treeMap with 4 elements {5,7,8,1,0,3,2} " must "report properly a search by range and delete"  in {

    val map =  scala.collection.mutable.TreeMap(
      5 -> "5",
      7 -> "7",
      8 -> "8",
      1 -> "1",
      0 -> "0",
      3 -> "3",
      2 -> "2"
    )
    val r = map.range(-1,1)
    r.foreach( p=> println(p._2))
    assert(r.size==1)
    assert(r(0) == "0")

    map.remove(0)

    val r2 = map.range(-1,1)
    r2.foreach( p=> println(p._2))
    assert(r2.isEmpty)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file TreeMap_Spec.scala
//=============================================================================

/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.binatyTree
//=============================================================================
import com.common.dataType.tree.binary.search.BinaryIntSearchTree
import com.test.UnitSpec
//=============================================================================
class BinaryAugmentedTree_Spec extends UnitSpec {
  //---------------------------------------------------------------------------
  "BST with none element" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with none element")
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file BinaryAugmentedTree_Spec.scala
//=============================================================================

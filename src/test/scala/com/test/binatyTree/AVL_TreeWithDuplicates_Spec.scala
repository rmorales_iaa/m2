/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.binatyTree

//=============================================================================
import com.common.dataType.tree.binary.avl.{AVL_TreeWithDuplicates}
import com.test.UnitSpec

//=============================================================================
class AVL_TreeWithDuplicates_Spec extends UnitSpec {

  //---------------------------------------------------------------------------
  " AVLITree " must "work when remove root" in {

    val tree = AVL_TreeWithDuplicates[Int]()
    tree += (-3,-31)
    tree += (-3,-33)
    tree.prettyPrint()

    tree -= -3
    tree.prettyPrint()

    tree -= -3
    tree.prettyPrint()

    tree -= -3
    tree.prettyPrint()

    tree -= 0
    tree.prettyPrint()

    tree -= 0
    tree.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  " AVLITree " must "traverse propely in  postorder"   in {

    val tree = AVL_TreeWithDuplicates[Int]()
    tree += (-3,-32)
    tree += (0,1)
    tree += (1,11)
    tree += (2,2)
    tree += (-3,-31)
    tree += (0,2)
    tree += (1,12)
    tree += (-2,-21)
    tree += (-2,-22)
    tree += (-3,-33)
    tree.prettyPrint()
    val r = tree.postOrder()

    val kk = tree.getFirstLeftLeaf

    assert(true)
  }

  //---------------------------------------------------------------------------
  " AVLITree " must "traverse propely in  inorder"   in {

    val tree = AVL_TreeWithDuplicates[Int]()
    tree += (-3,-32)
    tree += (0,1)
    tree += (1,11)
    tree += (2,2)
    tree += (-3,-31)
    tree += (0,2)
    tree += (1,12)
    tree += (-2,-21)
    tree += (-2,-22)
    tree += (-3,-33)
    tree.prettyPrint()
    val r = tree.inorOrder()
    val rl = tree.getFirstLeftLeaf
    val rr = tree.getFirstRightLeaf

    assert(true)
  }
  //---------------------------------------------------------------------------
  " AVLITree " must "work with dupliated values"  in {

    val tree = AVL_TreeWithDuplicates[Int]()
    tree += (-3,-32)
    tree += (0,1)
    tree += (1,11)
    tree += (2,2)
    tree += (-3,-31)
    tree += (0,2)
    tree += (1,12)
    tree += (-2,-21)
    tree += (-2,-22)
    tree += (-3,-33)
    tree.prettyPrint()

    val r1 = tree.find(-3)
    val r2 = tree.find(3)

    tree.prettyPrint()

    tree -= -3
    tree.prettyPrint()

    tree -= -3
    tree.prettyPrint()

    tree -= -3
    tree.prettyPrint()

    tree -= 0
    tree.prettyPrint()

    tree -= 0
    tree.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file AVL_TreeWithDuplicated_Spec.scala
//=============================================================================

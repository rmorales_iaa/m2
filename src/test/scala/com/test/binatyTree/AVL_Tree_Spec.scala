/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.binatyTree
//=============================================================================
import com.common.dataType.tree.binary.avl.AVL_Tree
import com.test.UnitSpec
//=============================================================================
class AVL_Tree_Spec extends UnitSpec {

  //---------------------------------------------------------------------------
  "A AVLITree " must "be fid its alues"  in {

    val tree = AVL_Tree()
    val iSeq = Seq(100,90,70,55,50,28,24,20,25,49,51,14)

    iSeq foreach {t => tree += t}
    tree.prettyPrint()

    val r = tree.find(521)


    assert(true)
  }
  //---------------------------------------------------------------------------
  "A AVLITree " must "work with affine_transformation values"  in {

    val tree = AVL_Tree()
    val iSeq = Seq(0,1,2,-2,-3)

    iSeq foreach {t => tree += t}
    tree.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "A AVLITree " must "not work with dupliated values"  in {

    val tree = AVL_Tree()
    val iSeq = Seq(100,50,100,100)

    iSeq foreach {t => tree += t}
    tree.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "A AVLITree " must "be keep properly balanced after insert"  in {

    val tree = AVL_Tree()
    //val iSeq = Seq(50, 70, 90, 25, 0, -1, 100, -75, -50, 51, 24, -24, -510)
    val iSeq = Seq(100,90,70,55,50,28,24,20,25,49,51,100)

    iSeq foreach {t => tree += t}
    tree.prettyPrint()

    tree += 14
    tree.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file AVLTree_Spec.scala
//=============================================================================

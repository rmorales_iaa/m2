/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.binatyTree
//=============================================================================
import com.common.geometry.segment.Segment1D
import com.common.dataType.tree.binary.interval.AVLI_Tree
import com.common.util.util.Util
import com.test.UnitSpec
//=============================================================================
class AVLI_Tree_Spec extends UnitSpec {
  //---------------------------------------------------------------------------
  "A AVLITree " must "be keep properly the (min,max) with base values"  in {

    val tree = AVLI_Tree()

    val iSeq = Seq(
      Segment1D(70, 70)
      , Segment1D(90, 90)
      , Segment1D(25, 25)
      , Segment1D(0, 0)
      , Segment1D(-1, -1)
      , Segment1D(100,100)
      , Segment1D(-75,-75)
      , Segment1D(-50, -50)
      , Segment1D(50, 50)
      , Segment1D(25, 25)
      , Segment1D(0, 0)
    )

    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()

    tree += Segment1D(55, 55)
    assert(tree.verifyMinMax)
    tree.prettyPrint()

    assert(true)
  }
  //---------------------------------------------------------------------------
  "A AVLITree " must "be keep properly the (min,max) with duplicated values"  in {

    val tree = AVLI_Tree()

    val iSeq = Seq(
      Segment1D(5, 10)       //0
      , Segment1D(15, 25)    //1
      , Segment1D(1, 12)     //2
      , Segment1D(8, 16)     //3
      , Segment1D(14, 20)    //4
      , Segment1D(18, 21)    //5
      , Segment1D(5, 10))     //6
    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }

    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "A AVLITree " must "be keep properly the (min,max)"  in {

    val tree = AVLI_Tree()

    val iSeq = Seq(
      Segment1D(5, 10)       //0
      , Segment1D(15, 25)    //1
      , Segment1D(1, 12)     //2
      , Segment1D(8, 16)     //3
      , Segment1D(14, 20)    //4
      , Segment1D(18, 21)    //5
      , Segment1D(2, 8))     //6
    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }

    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "A AVLITree case a" must "be keep properly the (min,max)"  in {

    val tree = AVLI_Tree()

    val iSeq = Seq(
      Segment1D(5, 10)       //0
      , Segment1D(15, 25)    //1
      , Segment1D(1, 12)     //2
      , Segment1D(8, 16)     //3
      , Segment1D(14, 20)    //4
      , Segment1D(18, 21)    //5
      , Segment1D(2, 8))     //6

     Util.getAllSubsets(iSeq.toSet) foreach { s=>
       s foreach {t =>
         tree += t
         assert(tree.verifyMinMax)
       }
     }

    assert(true)
  }
  //---------------------------------------------------------------------------
  "A AVLITree with 6 elements case b" must "be created and balanced in the same way with different order"  in {

    val tree = AVLI_Tree()

    val iSeq = Seq (
        Segment1D(5, 10)     //0
      , Segment1D(15, 25)    //1
      , Segment1D(1, 12)     //2
      , Segment1D(8, 16)     //3
      , Segment1D(14, 20)    //4
      , Segment1D(18, 21)    //5
      , Segment1D(2, 8))     //6
    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()
    assert(tree.verifyMinMax)


    val tree2 = AVLI_Tree()
    tree2 += iSeq(2)
    tree2 += iSeq(6)
    tree2 += iSeq(0)
    tree2 += iSeq(3)
    tree2 += iSeq(4)
    tree2 += iSeq(1)
    tree2 += iSeq(5)
    assert(tree2.verifyMinMax)

    assert(true)
  }

  //---------------------------------------------------------------------------
  "A AVLITree with 6 elements case c " must "be created and balanced in the same way with different order"  in {

    val iSeq = Seq (
      Segment1D(5, 10)     //0
      , Segment1D(15, 25)    //1
      , Segment1D(1, 12)     //2
      , Segment1D(8, 16)     //3
      , Segment1D(14, 20)    //4
      , Segment1D(18, 21)    //5
      , Segment1D(2, 8))     //6

    val tree = AVLI_Tree()
    tree += iSeq(2)
    tree += iSeq(6)
    tree += iSeq(0)
    tree += iSeq(3)
    tree += iSeq(4)
    tree += iSeq(1)
    tree += iSeq(5)
    assert(tree.verifyMinMax)
    tree.prettyPrint()

    assert(tree.verifyMinMax)

    assert(true)
  }

  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_1 " must "be created and balanced"  in {

    //https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(20)
      , Segment1D(4)
      , Segment1D(26)
      , Segment1D(3)
      , Segment1D(9)
      , Segment1D(2)
      , Segment1D(7)
      , Segment1D(11)
      , Segment1D(21)
      , Segment1D(30))

    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()
    assert(tree.verifyMinMax)
    assert(true)
  }

  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_1a " must "be created and balanced"  in {

    //https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(20)
      , Segment1D(4)
      , Segment1D(15)
    )
    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()
    assert(tree.verifyMinMax)
    assert(true)
  }

  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_2a " must "be created and balanced"  in {

    //https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(20)
      , Segment1D(4)
      , Segment1D(26)
      , Segment1D(3)
      , Segment1D(9)
      , Segment1D(15)
    )
    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()
    assert(tree.verifyMinMax)
    assert(true)
  }

  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_3a " must "be created and balanced"  in {

    //https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(20)
      , Segment1D(4)
      , Segment1D(26)
      , Segment1D(3)
      , Segment1D(9)
      , Segment1D(21)
      , Segment1D(30)
      , Segment1D(2)
      , Segment1D(7)
      , Segment1D(11)
      , Segment1D(15)
    )
    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()
    assert(tree.verifyMinMax)
    assert(true)
  }

  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_1b " must "be created and balanced"  in {

    //https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(20)
      , Segment1D(4)
      , Segment1D(8)
    )
    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()
    assert(tree.verifyMinMax)
    assert(true)
  }

  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_2b " must "be created and balanced"  in {

    //https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(20)
      , Segment1D(4)
      , Segment1D(26)
      , Segment1D(3)
      , Segment1D(9)
      , Segment1D(8)
    )
    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()
    assert(tree.verifyMinMax)
    assert(true)
  }

  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_3b " must "be created and balanced"  in {

    //https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(20)
      , Segment1D(4)
      , Segment1D(26)
      , Segment1D(3)
      , Segment1D(9)
      , Segment1D(21)
      , Segment1D(30)
      , Segment1D(2)
      , Segment1D(7)
      , Segment1D(11)
      , Segment1D(8)
    )
    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()
    assert(tree.verifyMinMax)
    assert(true)
  }

  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_d_0_0 " must "be created and balanced"  in {

    //https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
    val tree = AVLI_Tree()
    val iSeq = Seq(
        Segment1D(2)   //0
      , Segment1D(1)   //1
      , Segment1D(4)   //2
      , Segment1D(3)   //3
      , Segment1D(3)   //4
      , Segment1D(5)   //5
    )
    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()
    assert(tree.verifyMinMax)

    tree.delete(iSeq(1))
    tree.prettyPrint()
    assert(tree.verifyMinMax)
  }

  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_d_0_1 " must "be created and balanced"  in {

    //https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(6)     //0
      , Segment1D(2)   //1
      , Segment1D(9)   //2
      , Segment1D(1)   //3
      , Segment1D(4)   //4
      , Segment1D(8)   //5
      , Segment1D(20)   //6
      , Segment1D(3)   //7
      , Segment1D(5)   //8
      , Segment1D(7)   //9
      , Segment1D(21)   //10
      , Segment1D(22)   //11
    )

    iSeq foreach {t =>
      tree += t
      assert(tree.verifyMinMax)
    }
    tree.prettyPrint()
    assert(tree.verifyMinMax)

    tree.delete(iSeq(2))
    tree.prettyPrint()
    assert(tree.verifyMinMax)

  }

  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_d_0_2 " must "be created and balanced"  in {

    //https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(5)     //0
      , Segment1D(2)   //1
      , Segment1D(1)   //2
      , Segment1D(3)   //3
      , Segment1D(4)   //4
      , Segment1D(8)   //5
      , Segment1D(7)   //6
      , Segment1D(6)   //7
      , Segment1D(20)   //8
      , Segment1D(9)   //9
      , Segment1D(21)   //10
      , Segment1D(22)   //11
    )
    iSeq foreach (t => tree += t)
    tree.prettyPrint()
    assert(tree.verifyMinMax)

    tree.delete(iSeq(2))
    tree.prettyPrint()
    assert(tree.verifyMinMax)

  }
  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_d1 " must "be created and balanced"  in {

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(9)     //0
      , Segment1D(5)   //1
      , Segment1D(10)  //2
      , Segment1D(0)   //3
      , Segment1D(6)   //4
      , Segment1D(11)  //5
      , Segment1D(-1)  //6
      , Segment1D(1)   //7
      , Segment1D(2)   //8
    )
    iSeq foreach (t => tree += t)
    tree.prettyPrint()
    assert(tree.verifyMinMax)

    tree.delete(Segment1D(99))
    tree.prettyPrint()
    assert(tree.verifyMinMax)

    tree.delete(iSeq(5))
    tree.prettyPrint()
    assert(tree.verifyMinMax)
  }
  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_d2 " must "be created and balanced"  in {

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(9)     //0
      , Segment1D(5)   //1
      , Segment1D(10)  //2
      , Segment1D(0)   //3
      , Segment1D(6)   //4
      , Segment1D(11)  //5
      , Segment1D(-1)  //6
      , Segment1D(1)   //7
      , Segment1D(2)   //8
    )
    iSeq foreach (t => tree += t)
    tree.prettyPrint()
    assert(tree.verifyMinMax)

    tree.delete(iSeq(1))
    tree.prettyPrint()
    assert(tree.verifyMinMax)

  }
  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_d3 " must "be created and balanced"  in {

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(9)     //0
      , Segment1D(5)   //1
      , Segment1D(10)  //2
      , Segment1D(0)   //3
      , Segment1D(6)   //4
      , Segment1D(11)  //5
      , Segment1D(-1)  //6
      , Segment1D(1)   //7
      , Segment1D(2)   //8
    )
    iSeq foreach (t => tree += t)
    tree.prettyPrint()
    assert(tree.verifyMinMax)

    tree.delete(iSeq(7))
    tree.prettyPrint()
    assert(tree.verifyMinMax)

  }
  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_d4 " must "be created and balanced"  in {

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(9)     //0
      , Segment1D(5)   //1
      , Segment1D(10)  //2
      , Segment1D(0)   //3
      , Segment1D(6)   //4
      , Segment1D(11)  //5
      , Segment1D(-1)  //6
      , Segment1D(1)   //7
      , Segment1D(2)   //8
    )
    iSeq foreach (t => tree += t)
    tree.prettyPrint()
    assert(tree.verifyMinMax)

    tree.delete(iSeq(0))
    tree.prettyPrint()
    assert(tree.verifyMinMax)
  }
  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_d5 " must "be created and balanced"  in {

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(9)     //0
      , Segment1D(5)   //1
      , Segment1D(10)  //2
      , Segment1D(0)   //3
      , Segment1D(6)   //4
      , Segment1D(11)  //5
      , Segment1D(-1)  //6
      , Segment1D(1)   //7
      , Segment1D(2)   //8
    )
    iSeq foreach (t => tree += t)
    tree.prettyPrint()
    assert(tree.verifyMinMax)

    tree.delete(iSeq(6))
    tree.prettyPrint()
    assert(tree.verifyMinMax)
  }
  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_d6 " must "be created and balanced"  in {

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(9)     //0
      , Segment1D(5)   //1
      , Segment1D(10)  //2
      , Segment1D(0)   //3
      , Segment1D(6)   //4
      , Segment1D(11)  //5
      , Segment1D(-1)  //6
      , Segment1D(1)   //7
      , Segment1D(2)   //8
    )
    iSeq foreach (t => tree += t)
    tree.prettyPrint()
    assert(tree.verifyMinMax)

    tree.delete(iSeq(2))
    tree.prettyPrint()
    assert(tree.verifyMinMax)
  }
  //---------------------------------------------------------------------------
  "A AVLITree with a complex case_d7 " must "be created and balanced"  in {

    val tree = AVLI_Tree()
    val iSeq = Seq(
      Segment1D(9)     //0
      , Segment1D(5)   //1
      , Segment1D(10)  //2
      , Segment1D(0)   //3
      , Segment1D(6)   //4
      , Segment1D(11)  //5
      , Segment1D(-1)  //6
      , Segment1D(1)   //7
      , Segment1D(2)   //8
    )
    Util.getAllSubsets(iSeq.toSet) foreach { s=>

      val sa = s.toArray
      for(i<- 0 to s.size -1) {
        s foreach { t=>
         tree += t
         assert(tree.verifyMinMax)
        }
        assert(tree.verifyMinMax)
        tree.delete(sa(i))
        assert(tree.verifyMinMax)
      }
    }
    assert(true)
  }
  //---------------------------------------------------------------------------
  "A treeMap with 4 elements {5,7,8,1,0,3,2} " must "report properly a search by range"  in {

    val tree = new AVLI_Tree()

    val iSeq = Seq (
      Segment1D(5, 10)
      , Segment1D(15, 25)
      , Segment1D(1, 12)
      , Segment1D(8, 16)
      , Segment1D(14, 20)
      , Segment1D(18, 21)
      , Segment1D(2, 8))
    iSeq foreach (t=> tree += t)

    tree.prettyPrint()
    assert(tree.verifyMinMax)

    val seg_1 = Segment1D(8, 10)
    val r_1 = tree.getIntersection(seg_1)
    assert(r_1(0).equalAsInterval(iSeq(0)))
    assert(r_1(1).equalAsInterval(iSeq(2)))
    assert(r_1(2).equalAsInterval(iSeq(6)))
    assert(r_1(3).equalAsInterval(iSeq(3)))

    val seg_2 = Segment1D(20, 22)
    val r_2 = tree.getIntersection(seg_2)
    assert(r_2(0).equalAsInterval(iSeq(4)))
    assert(r_2(1).equalAsInterval(iSeq(1)))
    assert(r_2(2).equalAsInterval(iSeq(5)))
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file AVLITree_Spec.scala
//=============================================================================

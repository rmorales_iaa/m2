/*
 * Rafael Morales (rmorales@iaa.es)
 * Instituto de Astrofísisca de Andalucía - CSIC
 * This code is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)
 */
//=============================================================================
package com.test.binatyTree
//=============================================================================
import com.common.dataType.tree.binary.search.{BinaryIntSearchTree, EmptyNode}
import com.test.UnitSpec
//=============================================================================
class BinarySearchTree_Spec extends UnitSpec {
  //---------------------------------------------------------------------------
  "BST with none element" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with none element")
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with one element: {4}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with one element: {4}")
    tree += 4
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 2 elements: {4,5}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 2 elements: {4,5}")
    tree += 4
    tree += 5
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 2 elements with duplicated values: {4,4}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 2 elements with duplicated values: {4,4}")
    tree += 4
    tree += 4
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 2 elements {4,3}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 2 elements {4,3}")
    tree += 4
    tree += 3
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 3 elements {4,3,5}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 2 elements {4,3,5}")
    tree += 4
    tree += 3
    tree += 5
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 3 elements {4,3,2}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 3 elements {4,3,2}")
    tree += 4
    tree += 3
    tree += 2
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 3 elements {4,6,7}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 3 elements {4,6,7}")
    tree += 4
    tree += 6
    tree += 7
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 3 elements {4,6,5}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 3 elements {4,6,5}")
    tree += 4
    tree += 6
    tree += 5
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 3 elements {4,3,1}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 3 elements {4,3,1}")
    tree += 4
    tree += 3
    tree += 1
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,3,2,1}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,3,2,1}")
    tree += 4
    tree += 3
    tree += 2
    tree += 1
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,7,8}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,7,8}")
    tree += 4
    tree += 6
    tree += 7
    tree += 8
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7}" must "be created and printed" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7}")
    tree += 4
    tree += 6
    tree += 5
    tree += 7
    tree.prettyPrint()
    assert(true)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} and range {4}" must "be created, printed and range found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} and range {4}" )
    tree += 4
    tree += 6
    tree += 5
    tree += 7
    tree.prettyPrint()
    val r = tree.filterByRange(4,4)
    r foreach (t=> println(t))
    assert(r.length == 1)
    assert(r(0) == 4)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} and range {8}" must "be created, printed and range not found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} and range {8}" )
    tree += 4
    tree += 6
    tree += 5
    tree += 7
    tree.prettyPrint()
    val r = tree.filterByRange(8,8)
    r foreach (t=> println(t))
    assert(r.isEmpty)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} and range {5,7}" must "be created, printed and range found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} and range {5,7}" )
    tree += 4
    tree += 6
    tree += 5
    tree += 7
    tree.prettyPrint()
    val r = tree.filterByRange(5,7)
    r foreach (t=> println(t))
    assert(r.length == 3)
    assert(r(0) == 6)
    assert(r(1) == 5)
    assert(r(2) == 7)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} and range {4,7}" must "be created, printed and range found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} and range {4,7}" )
    tree += 4
    tree += 6
    tree += 5
    tree += 7
    tree.prettyPrint()
    val r = tree.filterByRange(4,7)
    r foreach (t=> println(t))
    assert(r.length == 4)
    assert(r(0) == 4)
    assert(r(1) == 6)
    assert(r(2) == 5)
    assert(r(3) == 7)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} but sorted  and range {4,7}" must "be created, printed and range found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} but sorted  and range {4,7}" )
    tree += Seq(4,6,5,7)
    tree.prettyPrint()
    val r = tree.filterByRange(4,7)
    r foreach (t=> println(t))
    assert(r.length == 4)
    assert(r(0) == 6)
    assert(r(1) == 5)
    assert(r(2) == 4)
    assert(r(3) == 7)
  }

  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} find 4" must "be created, printed and element found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} find 4" )
    tree += Seq(4,6,5,7)
    tree.prettyPrint()
    val r = tree.exist(4)
    assert(r)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} find 5" must "be created, printed and element found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} find 5" )
    tree += Seq(4,6,5,7)
    tree.prettyPrint()
    val r = tree.exist(5)
    assert(r)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} find 8" must "be created, printed and element not found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} find 8" )
    tree += Seq(4,6,5,7)
    tree.prettyPrint()
    val r = tree.exist(8)
    assert(!r)
  }
  //---------------------------------------------------------------------------
  "BST with none element" must "have an empty node as max node" in {
    val tree = new BinaryIntSearchTree(name = "BST with none element find min node")
    tree.prettyPrint()
    val n = tree.findMaxNode()
    assert(n._1 == EmptyNode)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} find 8" must "be created, printed and max element was found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} find min node" )
    tree += Seq(4,6,5,7)
    tree.prettyPrint()
    val r = tree.findMaxNode()
    assert(r._1.value.get == 7)
  }
  //---------------------------------------------------------------------------
  "BST with none element" must "have an empty node as min node" in {
    val tree = new BinaryIntSearchTree(name = "BST with none element find min node")
    tree.prettyPrint()
    val n = tree.findMinNode()
    assert(n._1 == EmptyNode)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} find 8" must "be created, printed and min element was found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} find min node" )
    tree += Seq(4,6,5,7)
    tree.prettyPrint()
    val r = tree.findMinNode()
    assert(r._1.value.get == 4)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} find node 6" must "be created, printed and node was found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} find node 6"  )
    tree += Seq(4,6,5,7)
    tree.prettyPrint()
    val r = tree.findNode(6)
    assert(r._1.get.value.get == 6)
    assert(r._2 == EmptyNode)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} find node 5" must "be created, printed and node was found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} find node 5"  )
    tree += Seq(4,6,5,7)
    tree.prettyPrint()
    val r = tree.findNode(5)
    assert(r._1.get.value.get == 5)
    assert(r._2.value.get == 6)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} find node 7" must "be created, printed and node was found" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} find node 7"  )
    tree += Seq(4,6,5,7)
    tree.prettyPrint()
    val r = tree.findNode(7)
    assert(r._1.get.value.get == 7)
    assert(r._2.value.get == 6)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {4,6,5,7} delete node 6" must "be created, printed and node was deleted" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {4,6,5,7} delete node 6"  )
    tree += Seq(4,6,5,7)
    tree.prettyPrint()
    tree.delete(6)
    tree.prettyPrint()
    val r = tree.getRoot.toSeq
    assert(r(0) == 4)
    assert(r(1) == 5)
    assert(r(2) == 7)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {5,7,8,1,0,3,2} delete node 6" must "be created, printed and node was deleted" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {5,7,8,1,0,3,2} delete node 5"  )
    tree += 5
    tree += 7
    tree += 8
    tree += 1
    tree += 0
    tree += 3
    tree += 2
    tree.prettyPrint()
    tree.delete(5)
    tree.prettyPrint()
    val r = tree.getRoot.toSeq
    assert(r(0) == 0)
    assert(r(1) == 1)
    assert(r(2) == 2)
    assert(r(3) == 3)
    assert(r(4) == 7)
    assert(r(5) == 8)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {5,7,8,1,0,3,2} delete node 1" must "be created, printed and node was deleted" in {
    val tree = new BinaryIntSearchTree(name = "BST with 4 elements {5,7,8,1,0,3,2} delete node 1"  )
    tree += 5
    tree += 7
    tree += 8
    tree += 1
    tree += 0
    tree += 3
    tree += 2
    tree.prettyPrint()
    tree.delete(1)
    tree.prettyPrint()
    val r = tree.getRoot.toSeq
    assert(r(0) == 0)
    assert(r(1) == 2)
    assert(r(2) == 3)
    assert(r(3) == 5)
    assert(r(4) == 7)
    assert(r(5) == 8)
  }
  //---------------------------------------------------------------------------
  "BST with 1 element {5} delete node 5" must "be created, printed and node was deleted" in {
    val tree = new BinaryIntSearchTree(name = "BST with 1 element {5} delete node 5"  )
    tree += 5
    tree.prettyPrint()
    tree.delete(5)
    tree.prettyPrint()
    val r = tree.getRoot.toSeq
    assert(r.isEmpty)
  }
  //---------------------------------------------------------------------------
  "BST with 1 element {5} delete node 8" must "be created, printed and node was not deleted" in {
    val tree = new BinaryIntSearchTree(name = "BST with 1 element {5} delete node 8"  )
    tree += 5
    tree.prettyPrint()
    val b = tree.delete(9)
    tree.prettyPrint()
    val r = tree.getRoot.toSeq
    assert(b == false)
    assert(r(0) == 5)
  }
  //---------------------------------------------------------------------------
  "BST with 4 elements {5,7,8,1,0,3,2} not balanced delete all nodes" must "be created, printed and all nodes were deleted"  in {
    val tree = new BinaryIntSearchTree(name ="BST with 4 elements {5,7,8,1,0,3,2} not balanced delete all nodes")
    tree += 5
    tree += 7
    tree += 8
    tree += 1
    tree += 0
    tree += 3
    tree += 2
    tree.prettyPrint()
    tree.delete(1)
    tree.prettyPrint()
    tree.delete(7)
    tree.prettyPrint()
    tree.delete(8)
    tree.prettyPrint()
    tree.delete(0)
    tree.prettyPrint()
    tree.delete(3)
    tree.prettyPrint()
    tree.delete(2)
    tree.prettyPrint()
    tree.delete(5)
    tree.prettyPrint()
    val r = tree.getRoot.toSeq
    assert(r.isEmpty)
  }
  //---------------------------------------------------------------------------
}

//=============================================================================
//End of file BinarySearchTree_Spec.scala
//=============================================================================

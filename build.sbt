//-----------------------------------------------------------------------------
lazy val myScalaVersion     = "2.13.14"
lazy val programName        = "m2"
lazy val programVersion     = "2.8.0"
lazy val programDescription = "Massive prOcessing Of aStronical imagEs (Moose) version 2"
lazy val authorList         = "Rafael Morales Muñoz (rmorales@iaa.es)"
lazy val authorOrganization = "IAA-CSIC"
lazy val license            = "This project is licensed under the Apache License, version 2.0 (http://www.apache.org/licenses/LICENSE-2.0)"
lazy val _homepage          = "http://www.iaa.csic.es"
//-----------------------------------------------------------------------------
//common settings
//version will be calculated using git repository
lazy val commonSettings = Seq(
  name                 := programName
  , version            := programVersion
  , scalaVersion       := myScalaVersion
  , description        := programDescription
  , organization       := authorOrganization
  , homepage           := Some(url(_homepage))
)
//-----------------------------------------------------------------------------
//Main class
lazy val mainClassName = "com.m2.Main"
//-----------------------------------------------------------------------------
//https://github.com/benblack86/finest
//To help improve code quality the following compiler flags will provide errors for deprecated and lint issues
scalacOptions ++= Seq (
  "-Xlint",
  "-deprecation",
  "-feature",
  "-unchecked"
  //  "-Xfatal-warnings"  
)
//-----------------------------------------------------------------------------
//dependencies versions
val apacheCommonsCsv         = "1.11.0"
val apacheCommonsIO          = "2.16.1"
val apacheCommonsMath        = "3.6.1"
val apacheLogCoreVersion     = "2.19.0"
val apacheLogScalaVersion    = "13.1.0"
val fastParseVersion         = "3.1.1"
val guavaVersion             = "33.3.0-jre"
val hdf_5_Version            = "0.8.2"
val httpVersion              = "2.4.2"
val jamaVersion              = "1.0.3"
val javaFitsVersion          = "1.20.1"
val javaxMailVersion         = "1.6.2"
val jniLibraryVersion        = "1.7.1"
val jSofaVersion             = "20210512"
val jsonParsingUpickleVersion  = "4.0.1"
val jsonParsingJsoniterVersion = "2.30.9"
val mongoScalaDriverVersion  = "5.2.0"
val rTree2DVersion           = "0.11.13"
val oshiCoreVersion          = "6.6.3"
val qosLogbackVersion        = "1.5.8"
val postgreSQL_Version       = "42.7.4"
val scallopVersion           = "5.1.0"
val scalaReflectVersion      = myScalaVersion
val scalaSqlVersion          = "0.1.9"
val scalaTestVersion         = "3.2.0-M2"
val typeSafeVersion          = "1.4.3"
val xzCompressionVersion     = "1.10"
//-----------------------------------------------------------------------------
//compile
Compile / mainClass := Some(mainClassName)
//-----------------------------------------------------------------------------
//assembly
assembly / mainClass := Some(mainClassName)
assembly / logLevel  := Level.Warn

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}
//-----------------------------------------------------------------------------
//tests
test / parallelExecution := false

//-----------------------------------------------------------------------------
//resolvers
//sbt-plugin
resolvers ++= Resolver.sonatypeOssRepos("public")
//-----------------------------------------------------------------------------
//output path for JNI C headers. Generate it with sbt javah
javah / target :=  sourceDirectory.value / "../native/header"

//-----------------------------------------------------------------------------
//git versioning: https://github.com/sbt/sbt-git
//The auto generated scala code with ths info is located at: target/src

git.useGitDescribe := true
git.uncommittedSignifier := None         //do not append suffix is some changes are not pushed to git repo
git.gitDescribePatterns := Seq("v*.*")   //format of the tag version

//https://stackoverflow.com/questions/56588228/how-to-get-git-gittagtoversionnumber-value
git.gitTagToVersionNumber := { tag: String =>
  if (tag.isEmpty)  None
  else Some(tag.split("-").toList.head)
}

//See configuration options in: https://github.com/sbt/sbt-buildinfo

//package and name of the scala code auto-generated
buildInfoPackage := "BuildInfo"
buildInfoObject := "BuildInfo"

//append build time
buildInfoOptions += BuildInfoOption.BuildTime

//append extra options to be generated
buildInfoKeys ++= Seq[BuildInfoKey](
  buildInfoBuildNumber
  , scalaVersion
  , sbtVersion
  , name
  , version
  , "authorList" -> authorList
  , "license" -> license
  , description
  , organization
  , homepage
  , BuildInfoKey.action("myGitCurrentBranch") {git.gitCurrentBranch.value}
  , BuildInfoKey.action("myGitHeadCommit")    {git.gitHeadCommit.value.get}
)
//-----------------------------------------------------------------------------
//exclude directories form source path: github.com/sbt/sbt-jshint/issues/14
Compile / unmanagedSources / excludeFilter := {
  new SimpleFileFilter(f=> {
    val p = f.getCanonicalPath
      p.contains("myJava/nom/tam") ||
      p.contains("stream/remote") ||
      p.contains("image/nsaImage/spark")
  })}
//-----------------------------------------------------------------------------
//dependencies 
lazy val dependenceList = Seq(

  //manage configuration files: https://github.com/typesafehub/config
  "com.typesafe" % "config" % typeSafeVersion

  //logging: https://logging.apache.org/log4j/2.x/manual/scala-api.html
  , "org.apache.logging.log4j" %% "log4j-api-scala" % apacheLogScalaVersion
  , "org.apache.logging.log4j" % "log4j-core" % apacheLogCoreVersion % Runtime
  , "ch.qos.logback" % "logback-classic" % qosLogbackVersion //required by mongoDB

  //jni: https://mvnrepository.com/artifact/com.github.sbt/sbt-jni-core
  // remember to add add: 'addSbtPlugin("com.github.sbt" % "sbt-jni" % "jniLibraryVersion")' in plugin.sbt
  , "com.github.sbt" %% "sbt-jni-core" % jniLibraryVersion

  //hardware info: https://mvnrepository.com/artifact/com.github.oshi/oshi-core
  , "com.github.oshi" % "oshi-core" % oshiCoreVersion

  // https://mvnrepository.com/artifact/commons-io/commons-io
  , "commons-io" % "commons-io" % apacheCommonsIO

  //java FITS: https://mvnrepository.com/artifact/gov.nasa.gsfc.heasarc/nom-tam-fits
  , "gov.nasa.gsfc.heasarc" % "nom-tam-fits" % javaFitsVersion

  //scala reflect
  , "org.scala-lang" % "scala-reflect" % scalaReflectVersion
  , "com.google.guava" % "guava" % guavaVersion

  //https://mvnrepository.com/artifact/org.apache.commons/commons-math3 https://mvnrepository.com/artifact/org.apache.commons/commons-math3c
  , "org.apache.commons" % "commons-math3" % apacheCommonsMath

  // basic linear algebra package for Java.  https://mvnrepository.com/artifact/gov.nist.math/jama
  , "gov.nist.math" % "jama" % jamaVersion

  //scala wrapper for HttpURLConnection. OAuth included. https://github.com/scalaj/scalaj-http
  , "org.scalaj" %% "scalaj-http" % httpVersion

  //mongoDB
  , "org.mongodb.scala" %% "mongo-scala-driver" % mongoScalaDriverVersion

  // xz compression: https://tukaani.org/xz/
  , "org.tukaani" % "xz" % xzCompressionVersion

  //command line parser : https://github.com/scallop/scallop
  , "org.rogach" %% "scallop" % scallopVersion

  //csv management: https://mvnrepository.com/artifact/org.apache.commons/commons-csv
  , "org.apache.commons" % "commons-csv" % apacheCommonsCsv

  //R-tree on 2d: https://github.com/plokhotnyuk/rtree2d
  , "com.github.plokhotnyuk.rtree2d" %% "rtree2d-core" % rTree2DVersion

  //Java translation of the International Astronomical Union's C SOFA software library : http://javastro.github.io/jsofa/
  , "org.javastro" % "jsofa" % jSofaVersion

  //json parsing: // https://mvnrepository.com/artifact/com.lihaoyi/upickle
  , "com.lihaoyi" %% "upickle" % jsonParsingUpickleVersion

  //json parsing: https://github.com/plokhotnyuk/jsoniter-scala
  , "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-core" % jsonParsingJsoniterVersion
  , "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-macros" % jsonParsingJsoniterVersion % "provided"

  //postgreSQL: http://www.lihaoyi.com/post/ScalaSqlaNewSQLDatabaseQueryLibraryforthecomlihaoyiScalaEcosystem.html#the-missing-database-library-in-the-com-lihaoyi-ecosystem
  , "com.lihaoyi" %% "scalasql" % scalaSqlVersion
  , "com.lihaoyi" %% "scalasql-core" % scalaSqlVersion
  , "com.lihaoyi" %% "scalasql-operations" % scalaSqlVersion
  , "com.lihaoyi" %% "scalasql-query"  % scalaSqlVersion
  , "org.postgresql" % "postgresql" % postgreSQL_Version

  //mail agent: https://javaee.github.io/javamail/
  , "com.sun.mail" % "javax.mail" % javaxMailVersion

  //hdf5 version: https://github.com/jamesmudd/jhdf
  , "io.jhdf" % "jhdf" % hdf_5_Version

  //grammar parser: https://github.com/com-lihaoyi/fastparse
  , "com.lihaoyi" %% "fastparse" % fastParseVersion

  //scala test
  , "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)
//=============================================================================
//root project
lazy val root = (project in file("."))
  .settings(commonSettings: _*)
  .enablePlugins(AssemblyPlugin, BuildInfoPlugin, GitVersioning)
  .settings(libraryDependencies ++= dependenceList)
//-----------------------------------------------------------------------------
//End of file 'process.sbt'
//-----------------------------------------------------------------------------

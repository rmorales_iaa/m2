//Start of file 'plugins.sbt'

//Buld a fat jar with all dependences included. https://github.com/sbt/sbt-assembly
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "1.2.0")

// Build, package and load native libraries
addSbtPlugin("com.github.sbt" % "sbt-jni" % "1.5.4")

//generates Scala source from your build definitions in sbt file.  https://github.com/sbt/sbt-buildinfo
addSbtPlugin("com.eed3si9n" % "sbt-buildinfo" %  "0.11.0")

//The sbt-git plugin offers git command line features directly inside sbt as well as allowing other plugins to make use of git.
//https://github.com/sbt/sbt-git
addSbtPlugin("com.github.sbt" % "sbt-git" % "2.0.0")

//dependence tree
addDependencyTreePlugin

//End of file 'plugins.sbt'
